Low.CBF Tango Devices for Monitoring and Control
================================================


.. toctree::
    :maxdepth: 2
    :caption: Contents:

.. toctree::
    :maxdepth: 2
    :caption: Readme

    README.md

.. toctree::
    :maxdepth: 2
    :caption: Low.CBF Tango Devices

    tango

.. toctree::
    :maxdepth: 2
    :caption: Subarray Resource Allocation

    resource_alloc

.. toctree::
    :maxdepth: 2
    :caption: Configuring Subarray Capabilities for Scan

    scan_config

.. toctree::
    :maxdepth: 2
    :caption: Health Monitoring

    health

.. toctree::
    :maxdepth: 2
    :caption: Alarms

    alarms

.. toctree::
    :maxdepth: 2
    :caption: Delay polynomial simulation for test & development

    delaypoly_sim

.. toctree::
    :maxdepth: 1
    :caption: Version History

    CHANGELOG.md

More information available on
`SKA Confluence - Low.CBF Tango Architecture <https://confluence.skatelescope.org/display/SE/Low.CBF+Tango+Architecture>`_
