Allocator device Tango attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.allocationVersionCounter
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.alveo_firmware_image_names
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.connectorIDs
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.connectorUpdate
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.internalAlveoLimits
   :noindex:

    INTERNAL use only

.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.internal_alveo
   :noindex:

    INTERNAL use only

.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.internal_subarray
   :noindex:

    INTERNAL use only

.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.p4_stn_routes
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.procDevFqdn
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.processorIDs
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.processorUpdate
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.pst_beams
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.resourceTableP4
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.sdp_routes
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.station_beams
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.stats_alveo
   :noindex:

Allocator device Tango commands
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.InternalRegisterAlveo 
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.InternalRegisterFsp 
   :noindex:

    INTERNAL use only

.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.ReleaseAllCapabilities 
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.ReserveCapabilities 
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.RunScan 
   :noindex:
.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.SubscribeToConnector 
   :noindex:

    INTERNAL use only

.. automethod:: ska_low_cbf.allocator.allocator_device::LowCbfAllocator.UnsubscribeFromConnector 
   :noindex:

    INTERNAL use only

