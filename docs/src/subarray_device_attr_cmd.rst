Subarray device Tango attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automethod:: ska_low_cbf.subarray.subarray_device::LowCbfSubarray.adminMode
   :noindex:                
.. automethod:: ska_low_cbf.subarray.subarray_device::LowCbfSubarray.assigned_processors
   :noindex:                
.. automethod:: ska_low_cbf.subarray.subarray_device::LowCbfSubarray.delaysValid
   :noindex:                
.. automethod:: ska_low_cbf.subarray.subarray_device::LowCbfSubarray.jonesSource
   :noindex:                
.. automethod:: ska_low_cbf.subarray.subarray_device::LowCbfSubarray.pssBeams
   :noindex:                
.. automethod:: ska_low_cbf.subarray.subarray_device::LowCbfSubarray.pstBeams
   :noindex:                
.. automethod:: ska_low_cbf.subarray.subarray_device::LowCbfSubarray.stationBeams
   :noindex:                
.. automethod:: ska_low_cbf.subarray.subarray_device::LowCbfSubarray.stations
   :noindex:                
