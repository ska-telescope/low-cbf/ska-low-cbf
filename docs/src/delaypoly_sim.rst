Delay Polynomial Simulation
===========================

Low.CBF includes a Tango delay-polynomial simulator device that is disabled
by default but can be enabled in helm chart when needed

The device has dynamic attributes that contain a JSON string representing
the delay polynomials for each station in a subarray station-beam. The data
format is identical to MID delay polynomial, but with an added 'sub-receptor'
entry that is provision for LOW sub-stations

The device monitors the Allocator's internal_subarray attribute to determine
the active subarrays, their station-beams, and the stations used. For each
station-beam, the device creates a dynamic atribute 'delay_sNN_bMM', where NN is the
subarray_id and MM is the beam_id.
Devices such as the Processor can subscribe to the attribute to obtain
delay polynomials required for correlation and beamforming.

Usage
=====

Steps for using the delay-polynomial simulator:

  * enable the delay-polynomial device in the helm chart
  * deploy ska-low-cbf
  * use the Tango URI of the delay-polynomial attribute in the JSON argument for a subarray's Configure command

Setting beam and source directions
==================================

Pointing direction for subarray beams are configured via one of three commands:

  * BeamRaDec - Specifies a Right Ascention and Declination, delays track with sidereal movement of the sky
  * BeamAzel  - Specifies Azimuth and Elevation pointing direction, delays are calculated for station geometry but do not move with sky
  * BeamDelay - Specifies delay values to be used for individual statations (stations present in subarray but omitted in command are assumed to have zero delay)

If no direction has been specified, the delay polynomial will default to a constant zero-delay for all stations in the subarray

Directions for the four sources that CNIC can produce for a particular beam are configured via three analogous commands:

  * SourceRaDec, SourceAzEl, SourceDelay

Pointing direction and Source directions can be updated at any time via the commands. Delay values for the new direction will be present
in the next delay polynomial that is calculated. (Currently delay polynomials are updated every 10 seconds)

Delays for for Pulsars that are offset from a particular station beam can be specified with three similar commands:

  * PstOffsetRaDec, PstOffsetAzEl, PstOffsetDelay

These commands result in attributes named for the subarray and beam, plus the pulsar (index starting at unity): "pst_sNN_bMM_X"

