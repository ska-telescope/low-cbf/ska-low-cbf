Controller device Tango attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automethod:: controller.controller_device::LowCbfController.all_alveos
   :noindex:
.. automethod:: controller.controller_device::LowCbfController.available_alveos
   :noindex:
.. automethod:: controller.controller_device::LowCbfController.health_connectors
   :noindex:
.. automethod:: controller.controller_device::LowCbfController.health_processors
   :noindex:
.. automethod:: controller.controller_device::LowCbfController.health_table
   :noindex:
.. automethod:: controller.controller_device::LowCbfController.subelementSubarrays
   :noindex:
