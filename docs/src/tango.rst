.. vim: syntax=rst

***********************
External-Facing Devices
***********************

These will interact with higher levels of the SKA control system (i.e. Low.CSP)
All require the SKA LMC base classes, which will be installed into the Docker image as part of the build process.

LowCbfController
################
The sub-element controller is the primary point of control and coordination for general operations from CSP_Low.LMC.

In the future (not yet implemented):
* Controller will subscribe to attributes of the internal Allocator device, and split them out into simpler attributes that can be consumed by Taranta Dashboards.
* Controller will compare the state of Processors published by the Allocator to their actual state to give a measure of their "health"

LowCbfSubarray
##############
A Low.CBF subarray represents grouping of input signal data streams (from SPS),
and controls Low.CBF's processing of those signals to generate desired visibility and beam outputs.

The subarray device implements the SKA *obsstate* state machine but delegates the remainder of most requests it receives
to the Allocator device which coordinates access to shared hardware.

There will be 16 instances of the subarray Tango device, inclusive of any engineering subarrays.


Attribute/Command list
######################
.. include:: subarray_device_attr_cmd.rst
.. include:: controller_device_attr_cmd.rst

****************
Internal Devices
****************
These devices are not expected to directly interface with the wider control system.

LowCbfAllocator
################
The Allocator coordinates how hardware processing resources are shared between subarrays.
Subarrays delegate most of the processing involved in their ConfigureScan requests to the Allocator.
Allocator publishes two key attributes that other devices subscribe to:
* **internal_subarray** provides information about every configured subarray. This info is needed by all processors to e.g determine destination addresses for beams or visibilties
* **internal_alveo** is a JSON dictionary with Alveo serial numbers as keys, and values describing the desired internal state of every in-use Alveo
All processor devices subscribe to the attributes and make best effort to conform themselves to the published state

Attribute/Command list
######################
.. include:: allocator_device_attr_cmd.rst

Related Tango devices
#####################
There are other Low.CBF devices not contained within this repository.
See `SKA Confluence - Low.CBF Tango Architecture <https://confluence.skatelescope.org/display/SE/Low.CBF+Tango+Architecture>`_.
