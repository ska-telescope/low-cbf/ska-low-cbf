Alarms vs Health
****************

Alarms and Health are different things.

An **alarm** is defined in IEC 62682 as:

  An audible and/or visible means of indicating to the operator of an equipment
  malfunction, process deviation, or abnormal condition requiring a response.

We define **health** as:

  A device's ability to perform its function.

.. image:: images/alarms_vs_health.svg

One is not a subset of the other, although there is some overlap. Some parameters that
trigger alarms may lead to a change in health state, others may not. Some parameters
that are used to calculate health state will have alarms associated with them, some may
not. A failed health state in some devices may be alarmed, others may not.

Low CBF devices will provide many diagnostic attributes which may or may not be used
as alarms in the operating telescope. Alarm annunciation and configuration will be via
an instance of the Elettra ``AlarmHandler`` Tango device. Its configuration includes a
"formula" that is evaluated to trigger an alarm, alarm priority, group, etc.

.. include:: attrquality_alarm_note.rst
