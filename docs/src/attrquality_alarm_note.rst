.. note:: An attribute that has an AttrQuality status of ALARM does not necessarily
  result in an alarm being announced to the operator! All operator alarms will be
  configured and announced via an ``AlarmHandler`` Tango device.
