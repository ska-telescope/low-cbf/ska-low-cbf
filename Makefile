# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST = artefact.skao.int and overwrites
# PROJECT to give a final Docker tag
PROJECT = ska-low-cbf

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-low-cbf

# RELEASE_NAME is the release that all Kubernetes resources will be labelled
# with
RELEASE_NAME ?= test

# UMBRELLA_CHART_PATH Path of the umbrella chart to work with
HELM_CHART ?= test-parent
UMBRELLA_CHART_PATH ?= charts/$(HELM_CHART)/
# Chart for testing
K8S_CHART ?= test-parent
K8S_CHARTS = $(K8S_CHART)

# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400
# Helm version
HELM_VERSION = v3.3.1
# kubectl version
KUBERNETES_VERSION = v1.19.2

CI_PROJECT_DIR ?= .

KUBE_CONFIG_BASE64 ?=  ## base64 encoded kubectl credentials for KUBECONFIG
KUBECONFIG ?= /etc/deploy/config ## KUBECONFIG location

XAUTHORITY ?= $(HOME)/.Xauthority
THIS_HOST := $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY ?= $(THIS_HOST):0
JIVE ?= false# Enable jive
TARANTA ?= false# Enable Taranta
TARANTA_AUTH ?= $(TARANTA) # Enable taranta authentication pod?
TARANTA_DASH ?= $(TARANTA) # Enable taranta dashboard pod & PVC?
MINIKUBE ?= true ## Minikube or not
EXPOSE_All_DS ?= true ## Expose All Tango Services to the external network (enable Loadbalancer service)

CI_PROJECT_PATH_SLUG ?= ska-low-cbf
CI_ENVIRONMENT_SLUG ?= ska-low-cbf

PROXY_VALUES = --env=http_proxy=${http_proxy} --env=https_proxy=${https_proxy}

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/base.mk
include .make/k8s.mk
include .make/python.mk
include .make/helm.mk
include .make/oci.mk


CI_JOB_ID ?= local##pipeline job id
TANGO_HOST ?= tango-databaseds:10000## TANGO_HOST connection to the Tango DS
TANGO_SERVER_PORT ?= 45450## TANGO_SERVER_PORT - fixed listening port for local server
CLUSTER_DOMAIN ?= cluster.local## Domain used for naming Tango Device Servers
K8S_TEST_RUNNER = test-runner-$(CI_JOB_ID)##name of the pod running the k8s-test

# define private overrides for above variables in here
-include PrivateRules.mak

# Single image in root of project
OCI_IMAGES = ska-low-cbf

ITANGO_ENABLED ?= false## ITango enabled in ska-tango-base

COUNT ?= 1


### LOW PSI LOGIC

# option used to enable settings for use at PSI Low site
# if user specifies a PSI_SERVER, then we infer they want to use Low PSI!
ifeq ($(origin PSI_SERVER),undefined)
	PSI_LOW ?=
else
	# PSI_SERVER value was supplied
	PSI_LOW ?= true
endif

# default to perentie1 for running CI jobs
PSI_SERVER ?= perentie1
# remove "psi-" prefix (in case user specifies actual hostname, e.g. 'psi-perentie1')
# this will give us a path like 'charts/psi-low-perentie1.values.yaml'
PSI_VALUES_FILE ?= charts/psi-low-$(PSI_SERVER:psi-%=%).values.yaml

ifneq ($(PSI_LOW),)
# PSI_LOW is active
# settings for psi-low-test CI job
	PYTHON_VARS_BEFORE_PYTEST := export TANGO_HOST=$(TANGO_HOST); \
		export CNIC_FW_VERSION=$(CNIC_FW_VERSION); export CNIC_FW_SOURCE=$(CNIC_FW_SOURCE);
	PYTHON_VARS_AFTER_PYTEST := -m 'hardware_present' --disable-pytest-warnings \
		--count=1 --timeout=300 --forked --true-context --verbose
	K8S_TEST_RUNNER_ADD_ARGS := --override-type="strategic" \
		--overrides="$$(cat test-runner-overrides.json | sed 's/test-runner-local/$(K8S_TEST_RUNNER)/g')"
	VALUES_FILE ?= $(PSI_VALUES_FILE)
# Settings to use PSI Low shared Taranta (user to set TARANTA=true as well)
	TARANTA_AUTH=false
	TARANTA_DASH=false
	MINIKUBE=false
else ifeq ($(strip $(firstword $(MAKECMDGOALS))),k8s-test)
# PSI_LOW is not active, we are running k8s-test (presumably in Minikube)
	PYTHON_VARS_BEFORE_PYTEST := export TANGO_HOST=$(TANGO_HOST);
	PYTHON_VARS_AFTER_PYTEST := -m 'post_deployment and not hardware_present' --disable-pytest-warnings \
		--count=1 --timeout=300 --forked --true-context
else
# PSI_LOW is not active
# settings for python-test CI job
	PYTHON_VARS_AFTER_PYTEST := -m 'not hardware_present and not post_deployment'
endif


HELM_CHARTS_TO_PUBLISH = ska-low-cbf
HELM_CHARTS ?= $(HELM_CHARTS_TO_PUBLISH)

PYTHON_BUILD_TYPE = non_tag_setup

PYTHON_SWITCHES_FOR_PYLINT=--fail-under=7

# Add the ability to specify a VALUES_FILE at runtime
ifneq ($(VALUES_FILE),)
	CUSTOM_VALUES := --values $(VALUES_FILE)
else
endif

ifneq ($(CI_REGISTRY),)
	K8S_TEST_TANGO_IMAGE = --set ska-low-cbf.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
		--set ska-low-cbf.image.registry=$(CI_REGISTRY)/ska-telescope/low-cbf/ska-low-cbf
	K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/low-cbf/ska-low-cbf/ska-low-cbf:$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
else
	K8S_TEST_TANGO_IMAGE = --set ska-low-cbf.image.tag=$(VERSION)
	K8S_TEST_IMAGE_TO_TEST = artefact.skao.int/ska-low-cbf:$(VERSION)
endif

TARANTA_PARAMS = --set ska-taranta.enabled=$(TARANTA) \
				 --set ska-taranta-auth.enabled=$(TARANTA_AUTH) \
				 --set ska-dashboard-repo.enabled=$(TARANTA_DASH)

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.exposeAllDS=$(EXPOSE_All_DS) \
	--set global.tango_host=$(TANGO_HOST) \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	--set global.device_server_port=$(TANGO_SERVER_PORT) \
	--set ska-tango-base.display=$(DISPLAY) \
	--set ska-tango-base.xauthority=$(XAUTHORITY) \
	--set ska-tango-base.jive.enabled=$(JIVE) \
	--set ska-tango-base.itango.enabled=$(ITANGO_ENABLED) \
	$(TARANTA_PARAMS) \
	${K8S_TEST_TANGO_IMAGE} \
	${CUSTOM_VALUES}

# override python.mk python-pre-test target
# poetry install can go away once upstream removes "--no-root"
python-pre-test:
	@echo "python-pre-test: running with: $(PYTHON_VARS_BEFORE_PYTEST) $(PYTHON_RUNNER) pytest $(PYTHON_VARS_AFTER_PYTEST) \
	 --cov=src --cov-report=term-missing --cov-report xml:build/reports/code-coverage.xml --junitxml=build/reports/unit-tests.xml $(PYTHON_TEST_FILE)"
	poetry install

k8s-pre-template-chart: k8s-pre-install-chart

k8s-pre-test:
ifneq ($(PSI_LOW),)
##	Enable PVC creation for psi-low-test
	kubectl apply -n $(KUBE_NAMESPACE) -f perentie-test-data-pvc.yaml
	@echo -e "Using \033[1;33mCNIC firmware\033[0m version \033[1;36m${CNIC_FW_VERSION}\033[0m from \033[1;36m${CNIC_FW_SOURCE}\033[0m"
else
endif

requirements: ## Install Dependencies
	poetry install

pipeline_unit_test: ## Run simulation mode unit tests in a docker container as in the gitlab pipeline
	@docker run -it --rm --volume="$$(pwd):/app:rw" \
		artefact.skao.int/ska-tango-images-pytango-builder:9.3.32 \
		sh -c "poetry config virtualenvs.create false && poetry install --no-root && make python-test"

start_pogo: ## start the pogo application in a docker container; be sure to have the DISPLAY and XAUTHORITY variable not empty.
	docker run --network host --user $(shell id -u):$(shell id -g) --volume="$(PWD):/home/tango/ska-low-cbf" --volume="$(HOME)/.Xauthority:/home/tango/.Xauthority:rw" --env="DISPLAY=$(DISPLAY)" $(CAR_OCI_REGISTRY_HOST)/ska-tango-images-tango-pogo:9.6.35

# When incrementing version
# (via make bump-patch-release or bump-minor-release or bump-major-release)
# update SKA_LOW_CBF_VERSION variable used for integration test CI job
post-set-release:
	@echo "Replacing SKA_LOW_CBF_VERSION variable in .gitlab-ci.yml for v$(VERSION)"
	@sed -i 's/SKA_LOW_CBF_VERSION:.*/SKA_LOW_CBF_VERSION: "$(VERSION)-dev.c$${CI_COMMIT_SHORT_SHA}"/' .gitlab-ci.yml

.PHONY: pipeline_unit_test requirements
