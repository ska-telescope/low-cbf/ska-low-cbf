SKA Low CBF
===============

TANGO device servers that perform monitor and control functions for the SKA-Low telescope's Correlator and Beam-Former.

## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-cbf/badge/?version=latest)](https://developer.skao.int/projects/ska-low-cbf/en/latest/?badge=latest)

Documentation for this project can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-low-cbf documentation](https://developer.skatelescope.org/projects/ska-low-cbf/en/latest/index.html "SKA Developer Portal: ska-low-cbf documentation")

## Project Avatar (Repository Icon)
[Control panel icons created by Eucalyp - Flaticon](https://www.flaticon.com/free-icons/control-panel "control panel icons")

# Developer's Guide

* The Makefiles here are inherited from
  [ska-cicd-makefile](https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile).
  * Refer to docs at that repo, or use `make help` for details.
  * This link is via a git submodule, so use the `--recursive` flag when cloning
this repository, or run `git submodule update --init --recursive` afterwards.
* A pre-commit config is provided to try and format your code before it gets
rejected by the CI pipeline lint checkers.
  * Install [pre-commit](https://pre-commit.com/) - `pip3 install pre-commit`
  * Activate the pre-commit checks for the repo - `pre-commit install`
  * From now on your commits will be formatted automatically. Note that you
have to add & commit again any files changed by pre-commit.
* A git hook is provided that may help comply with SKA commit message rules.
You can install the hook with `cp -s "$(pwd)/resources/git-hooks"/* .git/hooks`.
Once installed, the hook will insert commit messages to match the JIRA ticket
from the branch name.
e.g. On branch `perentie-1350-new-base-classes`:
```console
ska-low-cbf$ git commit -m "Add git hook note to README"
Branch perentie-1350-new-base-classes
Inserting PERENTIE-1350 prefix
[perentie-1350-new-base-classes 3886657] PERENTIE-1350 Add git hook note to README
 1 file changed, 7 insertions(+)
```
You can see the modified message above, and confirming via the git log:
```console
ska-low-cbf$ git log -n 1 --oneline
3886657 (HEAD -> perentie-1350-new-base-classes) PERENTIE-1350 Add git hook note to README
```

## Deploying to Low PSI

Makefile variables `PSI_SERVER` and `PSI_LOW` can be used to activate one of the Low
PSI values files (there is one per FPGA host server).

e.g. to use `psi-perentie2`:
```console
$ PSI_SERVER=perentie2 make k8s-install-chart
```

The server name can be specified either with or without the `psi-` prefix - i.e.
`psi-perentie2` and `perentie2` are equivalent.

Setting a value for `PSI_SERVER` implies that `PSI_LOW` should be active. If you
set `PSI_LOW` without specifying a server, it will default to `perentie1`.

## Tests
* `tests/unit` contains tests for our 'business logic' (the non-Tango code),
as well as basic Tango device tests that don't need to interact with other
devices. Allocation algorithm tests print some debug files:
  * Allocation visualisations in files dbg-alloc ...
  * Switch routing table entries for LFAA-\>Alveo routes in files rules- ...
  * Switch routing table entries for Alveo-\>PST server in files rules-pst ...
  * VCT table entries for each used alveo in files `vct_` ...
* `tests/integration` contains tests that involve interaction between
multiple Tango devices.

# Health Monitoring
Alveos not allocated to a Subarray won't be reporting FPGA register based health conditions (current as of 12-Jul-2023).
