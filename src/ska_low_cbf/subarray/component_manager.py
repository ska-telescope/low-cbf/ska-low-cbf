# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2022 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
SKA Low CBF
Subarray Component Manager
"""
import json
import logging
import math
import os
import time

from ska_tango_base.base import check_communicating
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import CommunicationStatus, PowerState
from ska_tango_base.executor import TaskExecutorComponentManager, TaskStatus
from ska_tango_base.subarray import SubarrayComponentManager
from tango import DevFailed, DeviceProxy

from ska_low_cbf.subarray.subarray import Subarray

# from typing import Callable  # these type hints were giving errors?


class LowCbfSubarrayComponentManager(
    SubarrayComponentManager, TaskExecutorComponentManager
):
    """
    Component manager for Low.CBF subarray
    Implements on/off opState functionality, involved in obsState
    Implements commands inherited from ska-tango-base subarray and
    csp/subarray
    """

    def __init__(
        self,
        logger: logging.Logger,
        subarray_id,
        communication_state_callback,  #: Callable[[CommunicationStatus]],
        component_state_callback,  #: Callable,
        allocator,
        admin_mode_cbk,
        connection_builder=None,
    ):
        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            power=PowerState.UNKNOWN,
            # Following are args that can be passed to _update_component_state
            # Must be all possibilites or _update_component_state() will crash
            fault=None,
            resourced=False,
            configured=False,
            scanning=False,
            obsfault=False,
        )
        self._admin_mode_cbk = admin_mode_cbk
        self.subarray = Subarray(
            subarray_id, allocator, connection_builder, logger
        )

    @property
    def configured_capabilities(self):
        """Return list of configured capabilities"""
        # TODO
        return []

    @property
    def config_id(self):
        """Return the configuration id."""
        return "config_id not yet implemented"

    @property
    def scan_id(self):
        """Return the scan id."""
        return "scan_id not yet implemented"

    # assignedResources attribute (from ska-tango-base/subarray_device.py)
    # calls component_manager.assigned_resources implemented here
    @property
    def assigned_resources(self):
        """Get list of assigned resource IDs"""
        return []

    @check_communicating
    def assign(self, task_callback, **resources) -> tuple[TaskStatus, str]:
        """
        Function called from SKA baseclasses in response to the
        AssignResources command. For Low.CBF there are no resources
        that can be assigned (argument should be empty)

        :param task_callback: called to inform baseclass about progress
        :param resources: dictionary of resources (should be empty)
        """
        if len(resources) != 0:
            self.logger.warn("Unexpected resources in assignment")
            for key, val in resources.items():
                self.logger.warn(f"ignoring {key}: {val}")

        return self.submit_task(
            self._assign,
            (resources,),
            task_callback=task_callback,
        )

    def _assign(self, resources, task_callback, task_abort_event) -> None:
        """
        Perform the actual resource assigment
        Note: Resources argument is not used
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        self._update_component_state(resourced=True)

        if task_callback is not None:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, "Assignment completed - noop"),
            )

    # Subarray releaseResources command calls here
    # overriding stub implementation inherited from SubarrayComponentManager
    @check_communicating
    def release(self, task_callback, **resources) -> tuple[TaskStatus, str]:
        """
        Function called from SKA baseclasses in response to
        ReleaseResources command

        Release resources is a no-op for Low.CBF (command takes no args)

        :param resources: resources to be released (should be empty)
        :type resources: dict
        """
        return self.submit_task(
            self._release, (resources,), task_callback=task_callback
        )

    def _release(self, resources, task_callback, task_abort_event) -> None:
        """
        Release resources from the component.

        :param resources: resources to be released
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        self._update_component_state(resourced=False)

        if task_callback is not None:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, "Release complete (no-op)"),
            )

    # Subarray releaseAllResources calls here
    # overriding stub implementation inherited from SubarrayComponentManager
    @check_communicating
    def release_all(self, task_callback):
        """
        Release all resources is a no-op for Low.CBF
        """
        return self.submit_task(self._release_all, task_callback=task_callback)

    def _release_all(self, task_callback, task_abort_event) -> None:
        """
        Release resources from the component.
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        self._update_component_state(resourced=False)

        if task_callback is not None:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, "Release all complete (no-op)"),
            )

    # Subarray ConfigureScan command calls here
    # overriding stub implementation inherited from SubarrayComponentManager
    # and called from CspSubElementSubarray
    @check_communicating
    def configure(
        self,
        task_callback,
        **configuration,
    ) -> tuple[TaskStatus, str]:
        """
        Function called from SKA baseclasses in response to Configure command

        Configure the component in preparation for scanning

        :param task_callback: called to inform baseclass of progress
        :param configuration: the configuration to be configured
        :type configuration: dict
        """
        return self.submit_task(
            self._configure, (configuration,), task_callback=task_callback
        )

    def _configure(
        self, configuration, task_callback, task_abort_event
    ) -> None:
        """
        Perform the actual configuring, acquire capabilities for subarray.

        This method provides the synchronous implementation of
        configuring. The public :py:class:`.configure` method achieves
        configuring by executing this method in an asynchronous context.
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        admin_mode = self._admin_mode_cbk()
        rslt_str = self.subarray.configure(admin_mode, configuration)
        self.logger.info(f"ConfigureScan result from allocator: {rslt_str}")
        rslt = json.loads(rslt_str)

        if rslt["success"] is False or rslt["has_config"] is False:
            # Allocation did not succeed
            self._update_component_state(obsfault=True)
            if task_callback is not None:
                task_callback(
                    status=TaskStatus.FAILED,
                    result=(ResultCode.FAILED, rslt["msg"]),
                )
            return

        # Wait for Processor devices to be actually ready for scan
        processor_devs = rslt["processor_fqdns"]
        success = self.wait_for_processors(processor_devs)
        if not success:
            msg = "Timeout waiting for processors"
            self._deconfigure(None, None)
            self._update_component_state(obsfault=True)
            if task_callback is not None:
                task_callback(
                    status=TaskStatus.COMPLETED,
                    result=(ResultCode.FAILED, msg),
                )
            return

        self._update_component_state(configured=True)
        if task_callback is not None:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, "Configure completed"),
            )

    def wait_for_processors(self, processor_devs: list[str]) -> bool:
        """
        Wait for processor devices to have loaded firmware

        :param processor_devs: list of Processor FQDNs to wait for
        :return: False if timeout occurred, True otherwise
        """
        if len(processor_devs) == 0:
            self.logger.info("No processor devices to wait on")
            return True
        n_devs = len(processor_devs)

        # how many devices to wait for
        qty_txt = os.getenv("PROCESSOR_WAIT_QTY", "all").lower()
        if qty_txt == "all":
            n_wait = n_devs
        elif qty_txt == "none":
            self.logger.info("Number of processors to wait for is zero")
            return True
        elif qty_txt == "one":
            n_wait = 1
        else:
            try:
                percent = int(qty_txt)
            except ValueError:
                self.logger.error("PROCESSOR_WAIT_QTY assumed 'none'")
                percent = 0
            n_wait = math.ceil(percent / 100 * n_devs)
            if n_wait < 0:
                return True
            if n_wait > n_devs:
                n_wait = n_devs

        # How long to wait for
        wait_time_txt = os.getenv("PROCESSOR_WAIT_SECS", "60")
        try:
            wait_secs = int(wait_time_txt)
        except ValueError:
            self.logger.error("PROCESSOR_WAIT_SECS assumed 60")
            wait_secs = 60

        dev_proxies = {
            devname: DeviceProxy(devname) for devname in processor_devs
        }
        devs_ready = []
        t_timeout = time.time() + wait_secs
        while True:
            for devname, devproxy in dev_proxies.items():
                if devname in devs_ready:
                    continue

                try:
                    tango_attr = devproxy.read_attribute("stats_mode")
                except DevFailed:
                    # DevFailed expected while downloading due to Xilinx driver
                    continue

                value = json.loads(tango_attr.value)
                if value["ready"] is True:
                    devs_ready.append(devname)

                if len(devs_ready) >= n_wait:
                    self.logger.info("%d processors ready", len(devs_ready))
                    return True

            if time.time() > t_timeout:
                self.logger.error(
                    "Timeout. Only %d/%d processors ready. Needed %d",
                    len(devs_ready),
                    n_devs,
                    n_wait,
                )
                return False
            # TODO could improve by deleting ready devices from dev_proxies
            # Sleep to prevent 100% CPU use while waiting
            time.sleep(1.0)

    # Subarray End and GoToIdle commands call here
    def deconfigure(self, task_callback):
        """Deconfigure this component ie release all its capabilities"""
        return self.submit_task(self._deconfigure, task_callback=task_callback)

    def _deconfigure(self, task_callback, task_abort_event) -> None:
        """
        Perform the actual deconfiguration, release capabilities.

        This method provides the synchronous implementation of
        configuring. The public :py:class:`.configure` method achieves
        configuring by executing this method in an asynchronous context.
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        self.subarray.deconfigure()

        # TODO fix when deconfigure returns useful things
        self._update_component_state(configured=False)

        if task_callback is not None:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, "Deconfigure completed"),
            )

    # Subarray Scan command calls here
    @check_communicating
    def scan(self, task_callback, **scan_args) -> tuple[TaskStatus, str]:
        """
        Function called from SKA baseclasses in response to Scan command

        Start scanning.

        :param task_callback: called to inform baseclass of progress
        :param scan_args: the scan parameters (eg ID)
        :type scan_args: dict
        """
        return self.submit_task(
            self._scan, (scan_args,), task_callback=task_callback
        )

    def _scan(self, args, task_callback, task_abort_event) -> None:
        """

        Perform the actual scan.
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        self.subarray.scan(args)
        self._update_component_state(scanning=True)

        if task_callback is not None:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, "Scan started"),
            )

    # Subarray EndScan command calls here
    @check_communicating
    def end_scan(self, task_callback):
        """End scanning."""
        return self.submit_task(self._end_scan, task_callback=task_callback)

    def _end_scan(self, task_callback, task_abort_event) -> None:
        """Perform the actual scan."""
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        self.subarray.end_scan()
        self._update_component_state(scanning=False)

        if task_callback is not None:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, "Scan ended"),
            )

    # Subarray Abort command calls here
    @check_communicating
    def abort(self, task_callback) -> tuple[TaskStatus, str]:
        """
        Function called from SKA baseclasses in response to Abort command

        Tell the component to:
            stop scanning (if scanning) and
            deconfigure (if configured)
        """
        self.subarray.end_scan()
        self._update_component_state(scanning=False)
        self._deconfigure(None, None)  # TODO handle result
        # abort commands in baseclass queue (should not be any)
        # return (TaskStatus.COMPLETED, "Abort completed")
        return self.abort_commands(task_callback=task_callback)

    # Subarray ObsReset command calls here
    def obsreset(self, task_callback) -> tuple[TaskStatus, str]:
        """
        Function called from SKA baseclasses in response to ObsReset command

        Reset the component to unconfigured but do not release resources.
        """
        return self.submit_task(self._obsreset, task_callback=task_callback)

    def _obsreset(self, task_callback, task_abort_event) -> None:
        """Perform subarray obsreset"""
        self._update_component_state(obsfault=False, configured=False)
        # TODO release all configured capabilities first
        message = "TODO obsreset: implement release capabilities"
        self.logger.warning(message)
        task_callback(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, message),
        )

    # Subarray Restart command calls here
    def restart(self, task_callback):
        """Deconfigure and release all resources."""
        return self.submit_task(self._restart, task_callback=task_callback)

    def _restart(self, task_callback, task_abort_event) -> None:
        """Perform subarray restart"""
        # Restart command (aborted state only) causes resources to be released
        self._deconfigure(None, None)  # deconfigure first as if end() issued

        self._update_component_state(
            resourced=False, obsfault=False, configured=False
        )
        message = "Restart OK"
        self.logger.info(message)
        task_callback(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, message),
        )
        self._update_component_state(obsfault=False)

    # Methods associated with starting comms to any hardware associated with
    # the tango device. No hardware present for this device.

    def start_communicating(self):
        """start_communicating is called when adminMode is changed to online"""
        # These calls move opState (Tango State) to "ON"
        self._update_communication_state(CommunicationStatus.ESTABLISHED)
        self._update_component_state(power=PowerState.ON, fault=False)

    def stop_communicating(self):
        """stop_communicating is called when adminMode is changed to offline"""
        # These calls move opState (Tango State) to "DISABLED"
        self._update_component_state(power=PowerState.OFF, fault=False)
        self._update_communication_state(CommunicationStatus.DISABLED)

    @check_communicating
    def off(self, task_callback) -> tuple[TaskStatus, str]:
        """OFF not used, subarray is always ON when adminMode is ONLINE"""
        return self.submit_task(self._off, task_callback=task_callback)

    def _off(self, task_callback, task_abort_event) -> None:
        """Method called by thread to perform 'off''"""
        message = "Ignored 'off', subarray always ON"
        self.logger.warning(message)
        task_callback(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.REJECTED, message),
        )

    @check_communicating
    def standby(self, task_callback) -> tuple[TaskStatus, str]:
        """STANDBY not used, subarray is always ON when adminMode is ONLINE"""
        return self.submit_task(self._standby, task_callback=task_callback)

    def _standby(self, task_callback, task_abort_event) -> None:
        """Method called by thread to perform 'standby'"""
        message = "Ignored 'standby', subarray always ON"
        self.logger.warning(message)
        task_callback(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.REJECTED, message),
        )

    @check_communicating
    def on(self, task_callback) -> tuple[TaskStatus, str]:
        """ON not used, subarray is always ON when adminMode is ONLINE"""
        return self.submit_task(self._on, task_callback=task_callback)

    def _on(self, task_callback, task_abort_event) -> None:
        """Method called by thread to perform 'on'"""
        message = "Ignored 'on', subarray always ON"
        self.logger.warning(message)
        task_callback(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, message),
        )

    @check_communicating
    def reset(self, task_callback):
        """RESET not used, subarray is always ON when adminMode is ONLINE"""
        return self.submit_task(self._reset, task_callback=task_callback)

    def _reset(self, task_callback, task_abort_event) -> None:
        """Method called by thread to perform 'reset'"""
        message = "Ignored 'reset', subarray always ON"
        self.logger.warning(message)
        task_callback(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.REJECTED, message),
        )
