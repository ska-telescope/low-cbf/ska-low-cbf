# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

""" Low CBf Subarray

A Low.CBF subarray represents a conceptual grouping of input signal data
streams (from MCCS), and controls Low.CBF's processing of those signals.

The subarray device will control the processing hardware (FPGA &
Network devices) to produce visibilities and beams,
if that is possible with the resources that have been assigned and configured.
It will also subscribe to delay polynomial updates from a given source address
and pass the data on to the relevant FPGAs.
"""
# Ignore the unused arguments in the scan commands for now
# pylint: disable=unused-argument

import ipaddress
import json
import logging
from collections import defaultdict

# Schema experiment
# TODO - this will move to telescope model later
from schema import Optional, Schema  # ,  Use, Or, And, Forbidden
from ska_tango_base.control_model import HealthState

configure_scan_schema = Schema(
    {
        "id": int,  # Configuration ID required by ska-tango-base classes
        Optional("common"): {Optional("subarrayID"): int},
        "lowcbf": {
            # Input to subarray from LFAA: stations, station_beams, frequencies
            "stations": {
                "stns": [
                    [int, int],  # stationID, substationID pairs
                ],
                "stn_beams": [
                    {
                        "beam_id": int,
                        "freq_ids": [
                            int,
                        ],
                        "delay_poly": str,
                    },
                ],
            },
            # Output to be calculated: visibilities
            Optional("vis"): {
                Optional("fsp"): {  # Deprecated
                    "firmware": str,
                    "fsp_ids": [
                        int,
                    ],
                    Optional("image_name"): str,
                },
                Optional("firmware"): str,
                "stn_beams": [
                    {
                        "stn_beam_id": int,
                        "host": [
                            [int, str],
                        ],
                        Optional("mac"): [
                            [int, str],
                        ],
                        "port": [
                            [int, int, int],
                        ],
                        "integration_ms": int,
                    },
                ],
            },  # standard Correlation parameter list TBD
            # Output to be calculated: PST beams
            Optional("timing_beams"): {
                Optional("fsp"): {  # Deprecated
                    "firmware": str,
                    "fsp_ids": [
                        int,
                    ],
                    Optional("image_name"): str,
                },
                Optional("firmware"): str,
                "beams": [
                    {
                        "pst_beam_id": int,
                        "stn_beam_id": int,
                        "delay_poly": str,
                        "jones": str,
                        "destinations": [
                            {
                                "data_host": str,
                                "data_port": int,
                                "start_channel": int,
                                "num_channels": int,
                            },
                        ],
                        "stn_weights": [
                            float,
                        ],
                        Optional("rfi_enable"): [
                            bool,
                        ],
                        Optional("rfi_static_chans"): [
                            int,
                        ],
                        Optional("rfi_dynamic_chans"): [
                            int,
                        ],
                        Optional("rfi_weighted"): float,
                    }
                ],
            },
            Optional("search_beams"): {
                Optional("fsp"): {  # Deprecated
                    "firmware": str,
                    "fsp_ids": [
                        int,
                    ],
                    Optional("image_name"): str,
                },
                Optional("firmware"): str,
                "beams": [
                    {
                        "pss_beam_id": int,
                        "stn_beam_id": int,
                        "delay_poly": str,
                        "jones": str,
                        "destinations": [
                            {
                                "data_host": str,
                                "data_port": int,
                                "start_channel": int,
                                "num_channels": int,
                            },
                        ],
                        "stn_weights": [
                            float,
                        ],
                        Optional("rfi_enable"): [
                            bool,
                        ],
                        Optional("rfi_static_chans"): [
                            int,
                        ],
                        Optional("rfi_dynamic_chans"): [
                            int,
                        ],
                        Optional("rfi_weighted"): float,
                    }
                ],
            },
            Optional("zooms"): str,  # zoom correlation parameter list TBD
        },
    },
)
"""ConfigureScan command schema"""


def sort_stns_in_subarray_cfg(cfg_allocator: dict) -> None:
    """
    Ensure stations in LowCBF subarray config are in ascending order
    This modifies a subarray configuration dictionary in place

    :param cfg_allocator: dictionary with lowCBF subarray configuration
    """
    stns = cfg_allocator["stations"]["stns"]  # list of (stn, substn)
    order = [i[0] for i in sorted(enumerate(stns), key=lambda x: x[1])]
    sorted_stns = [stns[idx] for idx in order]
    cfg_allocator["stations"]["stns"] = sorted_stns
    if "timing_beams" in cfg_allocator:
        # sort station weights in all timing beams
        for beam in cfg_allocator["timing_beams"]["beams"]:
            wts = beam["stn_weights"]
            if len(wts) < len(stns):
                wts.extend([wts[-1]] * (len(stns) - len(wts)))
            sorted_wts = [wts[idx] for idx in order]
            beam["stn_weights"] = sorted_wts
    if "search_beams" in cfg_allocator:
        # sort station weights in all search beams
        for beam in cfg_allocator["search_beams"]["beams"]:
            wts = beam["stn_weights"]
            if len(wts) < len(stns):
                wts.extend([wts[-1]] * (len(stns) - len(wts)))
            sorted_wts = [wts[idx] for idx in order]
            beam["stn_weights"] = sorted_wts


class Subarray:
    """Low.CBF Subarray core functionality"""

    def __init__(
        self, subarray_id, allocator, connection_builder, logger=None
    ):
        """
        Subarray object initialisation

        :param allocator: an object to be used for allocation requests
        :param connection_builder: a function to be called to create a
        connection to another device
        :param logger: Optional logger, one will be created if not supplied
        """
        self.logger = logger or logging.getLogger(__name__)
        self._allocator = allocator
        self._connect = connection_builder
        self._subarray_id = subarray_id

        self._jones_source = None
        self._pss_beams = defaultdict(set)
        self._pst_beams = defaultdict(set)
        """key: PST beam ID, value: Station Beam ID"""
        self._station_beam_channels = defaultdict(set)
        """key: station beam id, values: set of channel ids"""
        self._station_beam_zooms = defaultdict(set)
        """key: station beam id, values: set of zoom ids"""
        self._stations = defaultdict(set)
        """key: station, value: set of sub-stations"""

    # Methods for used to assign/release resources from a subarray
    # called from component manager
    # ============================================================

    def _supported_by_low(self, cfg_request) -> bool:
        """
        Some combinations of valid schema options are not supported by
        Low.CBF FPGA correlator implementation.

        This code currently checks visibility destination parameters:
        - SDP addresses must be dotted numeric ("1.2.3.4" not "www.abc.com")
        - start port be same for all hosts receiving the same station-beam
        - port increment can only be unity
        - number of channels sent to each SDP host must be same


        :param cfg_request: full JSON string for ConfigureScan request
        :return: bool, True indicates that Low.CBF can support requested args
        """
        is_ok = True
        if "vis" in cfg_request["lowcbf"]:
            for stn_beam in cfg_request["lowcbf"]["vis"]["stn_beams"]:
                start_frqs_1 = []
                host_ips = []
                for itm in stn_beam["host"]:
                    # items in host list are [start_frq, dotted_ip]
                    start_frqs_1.append(itm[0])
                    host_ips.append(itm[1])

                start_frqs_2 = []
                first_ports = []
                port_increments = []
                for itm in stn_beam["port"]:
                    # items in port list are [start_frq, first_udp, port_incr]
                    start_frqs_2.append(itm[0])
                    first_ports.append(itm[1])
                    port_increments.append(itm[2])

                # Host and Port first channel numbers must match
                if start_frqs_1 != start_frqs_2:
                    msg = "SDP destination 'host' and 'port' first channels"
                    msg += " must match (host=%s ports= %s)"
                    self.logger.error(msg, start_frqs_1, start_frqs_2)
                    is_ok = False

                # Each SDP IP address must be dotted-ip format
                for dotted_ip in host_ips:
                    # each IP dest must be dotted IP form
                    try:
                        _ = ipaddress.ip_address(dotted_ip)
                    except ValueError:
                        self.logger.error("Bad SDP IP address '%s'", dotted_ip)
                        is_ok = False

                # First port numbers must be same for all hosts
                first_ports = set(first_ports)
                if len(first_ports) != 1:
                    msg = "SDP first port numbers must be same for all hosts,"
                    msg += " but there are %d values: %s"
                    self.logger.error(msg, len(first_ports), first_ports)
                    is_ok = False

                # only unity for port increments
                port_increments = set(port_increments)
                if len(port_increments) != 1 or 1 not in port_increments:
                    msg = "SDP port increments must be unity, values used"
                    msg += " were: %s"
                    self.logger.error(msg, port_increments)
                    is_ok = False

                # number of channels must be same for all hosts
                if len(start_frqs_1) > 2:
                    n_chans = []
                    sorted_start_frqs = sorted(start_frqs_1)
                    for i in range(0, len(start_frqs_1) - 1):
                        n_frqs = (
                            sorted_start_frqs[i + 1] - sorted_start_frqs[i]
                        )
                        n_chans.append(n_frqs)
                    n_chans = set(n_chans)
                    if len(n_chans) > 1:
                        msg = "No. of channels must be same for each SDP host"
                        msg += ", found %d values: %s"
                        self.logger.error(msg, len(n_chans), n_chans)
                        is_ok = False

        return is_ok

    # Methods associated with configuring Capabilities to prepare for a scan
    # called from the component manager
    # ======================================================================

    def configure(self, admin_mode: int, cfg_request=None):
        """Configure and  ConfigureScan command implementation call here"""
        self.logger.info(
            f"configure: reserve capabilities for subarray {self._subarray_id}"
        )

        # TODO validate JSON via Telescope Model rather than python schema
        if not configure_scan_schema.is_valid(cfg_request):
            self.logger.warning(f"Bad ConfigureScan request {cfg_request}")
            return json.dumps(
                {
                    "success": False,
                    "has_config": False,
                    "msg": "Request invalid",
                }
            )
        if not self._supported_by_low(cfg_request):
            self.logger.warning("Unsupported configureScan %s", cfg_request)
            return json.dumps(
                {
                    "success": False,
                    "has_config": False,
                    "msg": "Unsupported parameter combination",
                }
            )

        # peel off the enclosing "lowcbf" before passing to allocator
        cfg_allocator = cfg_request["lowcbf"]
        cfg_allocator["subarray_id"] = self._subarray_id
        cfg_allocator["status"] = admin_mode

        # ensure stations are in ascending order, per ICD
        sort_stns_in_subarray_cfg(cfg_allocator)

        # Call to allocator Tango device to reserve Capbilities for subarray
        rslt = self._allocator.ReserveCapabilities(json.dumps(cfg_allocator))
        return rslt

    def deconfigure(self, deconf_request=None):
        """End and GoToIdle command implementation call here"""
        self.logger.info(
            f"deconfig: release capabilities for subarray {self._subarray_id}"
        )
        # TODO validate JSON? But End and GoToIdle don't take arguments
        deconf_request = {"subarray_id": self._subarray_id}
        self._allocator.ReleaseAllCapabilities(json.dumps(deconf_request))

    # Methods associated with initiating and ending a scan,
    # called from the component manager
    # ====================================================

    def scan(self, *args, **kwargs):
        """
        Start a Scan.
        :param args: single argument, dict with "id" key for scan_id
        :param kwargs: TDB
        :return: TBD
        """
        scan_id = args[0]["id"]
        self.logger.info(
            f"Starting Scan with {len(self._pst_beams)} PST Beams, "
            f"{len(self._pss_beams)} PSS Beams, {len(self._stations)} Stations"
        )
        scan_params = {
            "subarray_id": self._subarray_id,
            "scan": True,
            "scan_id": scan_id,
        }
        self._allocator.RunScan(json.dumps(scan_params))

    def end_scan(self, *args, **kwargs):
        """
        End the active Scan.
        :param args: TBD
        :param kwargs: TDB
        :return: TBD
        """
        self.logger.info(
            f"Ending Scan with {len(self._pst_beams)} PST Beams, "
            f"{len(self._pss_beams)} PSS Beams, {len(self._stations)} Stations"
        )
        scan_params = {
            "subarray_id": self._subarray_id,
            "scan": False,
            "scan_id": 0,
        }
        self._allocator.RunScan(json.dumps(scan_params))

    # Getter methods used to access info about the state of this module
    # =================================================================

    @property
    def health_state(self):
        """
        :return: Subarray Overall Health State
        :rtype: ska_tango_base.HealthState
        """
        # TODO
        return HealthState.OK

    @property
    def jones_source(self):
        """Get jones source"""
        return self._jones_source

    @jones_source.setter
    def jones_source(self, value):
        """Set jones source"""
        self._jones_source = value

    @property
    def pss_beams(self):
        """Get assigned PSS beams"""
        return self._pss_beams

    @property
    def pst_beams(self):
        """Get assigned PST beams"""
        return self._pst_beams

    @property
    def station_beams(self):
        """Get assigned station beams"""
        # TODO - figure out a format for this data.
        retval = {}
        if self._station_beam_channels:
            retval["channels"] = self._station_beam_channels
        if self._station_beam_zooms:
            retval["zooms"] = self._station_beam_zooms
        return retval

    @property
    def stations(self):
        """Get assigned stations"""
        return self._stations
