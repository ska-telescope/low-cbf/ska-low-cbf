# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=invalid-name,protected-access,too-few-public-methods

""" SKA Low CBF

Subarray device for Low.CBF
"""

import json
import os
import time
from threading import Lock, Thread

from ska_tango_base import SKABaseDevice, SKASubarray
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import AdminMode, HealthState, ObsState
from tango import AttrWriteType, Database, DeviceProxy, DevUShort
from tango.server import attribute, device_property, run

from ska_low_cbf import release
from ska_low_cbf.device_proxy import MccsDeviceProxy
from ska_low_cbf.events import EventManager
from ska_low_cbf.subarray.component_manager import (
    LowCbfSubarrayComponentManager,
)

__all__ = ["LowCbfSubarray", "main"]

UNUSED = 2  # a kind of 3-rd state boolean indicating subarray polynomial is not used


class LowCbfSubarray(SKASubarray):
    """
    Subarray device for Low.CBF

    **Properties:**

    - Device Property
        ControllerAddress
            - Tango address of Low.CBF Controller
            - Type:'DevString'
        AllocatorAddress
            - Tango address of allocator device that will handle assignment of
            FPGA resources
            - Type:'DevString'
    """

    _health_states = (HealthState.FAILED, HealthState.DEGRADED, HealthState.OK)
    """HealthStates order by severity"""

    # Device Properties
    ControllerAddress = device_property(
        dtype="DevString", default_value="low-cbf/control/0"
    )

    AllocatorAddress = device_property(
        dtype="DevString", default_value="low-cbf/allocator/0"
    )

    ConnectorDbHost = device_property(
        dtype="str", default_value="tango-databaseds.ska-low-cbf-conn"
    )
    ConnectorDbPort = device_property(dtype="int", default_value=10000)

    @attribute(
        dtype="DevString",
        access=AttrWriteType.READ_WRITE,
        label="Jones Source",
        doc=(
            "The subarray will subscribe to this source for updates to Jones "
            "Matrices"
        ),
    )
    def jonesSource(self):
        """Return the jonesSource attribute."""
        # self.logger.info("read_jonesSource")
        return self.component_manager.subarray.jones_source

    @attribute(
        dtype="DevString",
        label="Stations",
        doc="Report station & substation membership in subarray",
    )
    def stations(self) -> str:
        """
        Report station & substation membership in subarray
        """
        return str(self.component_manager.subarray.stations)

    @attribute(dtype=DevUShort)
    def delaysValid(self) -> DevUShort:
        """Get per-subarray delay polynomial validity value.

        Legal values are:

        * 0 - INVALID
        * 1 - VALID
        * 2 - UNUSED

        :return: delay validity status as integer
        :rtype: DevUShort
        """
        return self._delays_valid

    def _update_delays_valid(self, fqdn: str, new_delays: list[int]) -> None:
        "Update this subarray specific delay polynomial validity status"
        assert len(new_delays) >= self._subarray_id
        self.logger.info(f"{new_delays}")
        sub_id = self._subarray_id - 1  # subarrays are 1-based
        # update this processor's contribution
        self._delay_contributors[fqdn] = new_delays[sub_id]
        for device, value in self._delay_contributors.items():
            self.logger.info(f"{device}: {value}")
        new_value = min(self._delay_contributors.values())
        if self._delays_valid != new_value:
            self._delays_valid = new_value
            self.push_change_event("delaysValid", new_value)
            self.push_archive_event("delaysValid", new_value)

    @attribute(
        dtype="DevString",
        label="Station Beams",
        doc="Report configuration of all Station Beams",
    )
    def stationBeams(self) -> str:
        """Return the stationBeams attribute."""
        return str(self.component_manager.subarray.station_beams)

    @attribute(
        dtype="DevString",
        label="Pulsar Search Beams",
        doc=(
            "Each Pulsar Search Beam is associated with one Station Beam, and "
            "has additional configuration parameters including a delay "
            "polynomial source (supplied via Configure)"
        ),
    )
    def pssBeams(self) -> str:
        """Return the pssBeams attribute."""
        return str(self.component_manager.subarray.pss_beams)

    @attribute(
        dtype="DevString",
        label="Pulsar Timing Beams",
        doc=(
            "Each Pulsar Timing Beam is associated with one Station Beam, and "
            "has additional configuration parameters including a delay "
            "polynomial source (supplied via Configure)"
        ),
    )
    def pstBeams(self) -> str:
        """Return the pstBeams attribute."""
        return str(self.component_manager.subarray.pst_beams)

    # We override SKA-Tango-BASE adminMode attribute to prevent it being
    # set to OFFLINE while subarray is resourced. This change means that
    # a subarray can have resources only in adminMode ONLINE or ENGINEERING
    @attribute(dtype=AdminMode, memorized=True, hw_memorized=True)
    def adminMode(self: SKABaseDevice) -> AdminMode:
        """
        Read the Admin Mode of the device.

        It may interpret the current device condition and condition of all
        managed devices to set this. Most possibly an aggregate attribute.

        :return: Admin Mode of the device
        """
        return self._admin_mode

    @adminMode.write  # type: ignore[no-redef]
    def adminMode(self: SKABaseDevice, value: AdminMode) -> None:
        """
        Set the Admin Mode of the device. Overide of ska-tango-base
        to prevent being set offline while resourced

        :param value: Admin Mode of the device.

        :raises ValueError: for unknown adminMode
        """
        # Block adminMode change if subarray active but obsState is not EMPTY
        # or IDLE. IDLE also allowed - subarrays have empty assignresources.
        # Allowing IDLE allows PTC1 to pass. PTC1 changes AdminMode in IDLE
        blocked = (
            (
                (self._admin_mode == AdminMode.ONLINE)
                or (self._admin_mode == AdminMode.ENGINEERING)
            )
            and (self._obs_state != ObsState.EMPTY)
            and (self._obs_state != ObsState.IDLE)
        )

        assert not blocked, "Can't change adminMode when obsState not EMPTY"

        if value == AdminMode.NOT_FITTED:
            self.admin_mode_model.perform_action("to_notfitted")
        elif value == AdminMode.OFFLINE:
            # should health monitoring stop here?
            self.admin_mode_model.perform_action("to_offline")
            self.component_manager.stop_communicating()
        elif value == AdminMode.ENGINEERING:
            self.admin_mode_model.perform_action("to_engineering")
            self.component_manager.start_communicating()
            # Should health monitoring also start here?
        elif value == AdminMode.ONLINE:
            self.admin_mode_model.perform_action("to_online")
            self.component_manager.start_communicating()
            # monitoring of external device health should only run when the
            # NO_HEALTH_ROLLUP environment variable is NOT defined
            if (
                os.getenv("NO_HEALTH_ROLLUP") is None
                and not self._subscribed_to_alloc
            ):
                self._thrd = Thread(target=self._subscribe_allocator)
                self._thrd.start()
                self._subscribe_to_connector()
            # propagate healthState in case it was updated while not ONLINE
            self._recalculate_health()
        elif value == AdminMode.RESERVED:
            self.admin_mode_model.perform_action("to_reserved")
        else:
            raise ValueError(f"Unknown adminMode {value}")

    # TBD: Taranta didn't like the name 'assignedProcessors' (:shrug:)
    @attribute(dtype="str", doc=("List of processors assigned to subarray"))
    def assigned_processors(self: SKABaseDevice) -> str:
        """
        Get a list of processors assigned to the subarray.

        :return: JSON string, a list of processor serial numbers
        """
        return json.dumps(self._assigned_proc_serials)

    # override of CspSubElementObsDevice _component_state_changed
    def _component_state_changed_low(
        self,
        fault=None,
        power=None,
        resourced=None,
        configured=None,
        scanning=None,
        obsfault=None,  # new here
    ):
        """Enhanced obsState callback to allow obsState=FAULT"""
        # Most state is handled by calling ska-tango-base BaseComponentManager
        self._component_state_changed(
            fault=fault,
            power=power,
            resourced=resourced,
            configured=configured,
            scanning=scanning,
        )
        # But our new state variable is handled here
        if obsfault is not None:
            if obsfault:
                self.obs_state_model.perform_action("component_obsfault")
            else:
                pass  # (no state change needed when obsfault is cleared)

    # General methods
    def create_component_manager(self):
        subarray_name = self.get_name()
        subarray_name_parts = subarray_name.split("/")
        subarray_id = int(subarray_name_parts[2])
        allocator = MccsDeviceProxy(
            self.AllocatorAddress, self.logger, connect=True
        )

        return LowCbfSubarrayComponentManager(
            logger=self.logger,
            subarray_id=subarray_id,
            communication_state_callback=self._communication_state_changed,
            component_state_callback=self._component_state_changed_low,
            allocator=allocator,
            admin_mode_cbk=self.get_admin_mode,
        )

    def get_admin_mode(self) -> int:
        """
        callback providing access to subarray "adminMode"
        :return: Which adminMode subarray is in
        """
        return self._admin_mode

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """

    @jonesSource.write
    def jonesSource(self, value):
        """Set the jonesSource attribute."""
        self.logger.info("write_jonesSource")
        self.component_manager.subarray.jones_source = value

    # Commands
    def init_command_objects(self):
        """
        Initialises the command handlers for commands supported by this device.
        """
        # pylint: disable=useless-super-delegation
        super().init_command_objects()

    class InitCommand(SKASubarray.InitCommand):
        """Init Command object"""

        def do(self):
            """
            Initialises the attributes and properties of the LowCbfSubarray.
            """
            super().do()
            self._device._version_id = release.version
            self._device._build_state = (
                f"{release.name}, {release.version}, {release.description}"
            )

            self._device._processor_event_manager = EventManager(
                self._device.logger
            )

            # Events to manually be pushed
            # self._device.set_change_event("assignedResources", True, False)
            self._device.set_change_event("healthState", True, False)

            self._device._proc_sn_to_fqdn = {}
            """All known Processors { serial number: Processor FQDN }"""
            self._device._assigned_proc_serials = []
            """Serial numbers of Processors assigned to this subarray"""
            self._device._subscribed_sn = []
            """Serial numbers of Processors we are subscribed to"""
            self._device._subscribed_fqdn = []
            """FQDNs of Processors we are subscribed to"""
            self._device._proc_thread = None
            """Thread used to subscribe to processors healthState as they get
            assigned to this subarray"""
            # TBD expose this as an attribute:
            self._device._component_health = {}
            """A dictionary of health for constituent components; e.g.
            {
                "low-cbf/processor/0" : OK,
                "low-cbf/processor/1" : DEGRADED,
                "low-cbf/connector/0" : OK
            }
            """
            self._device._callback_lock = Lock()
            """Serialise callbacks from different devices"""
            self._device._delays_valid = UNUSED
            """Overall indicator if all delays in subarray are valid"""
            self._device._delay_contributors = {}
            """FQDN: delay_valid map - how much each processor contributes to
            overall subarray's 'delaysValid' state"""

            # A few more attributes that could be pushed on change however
            # at present they come from the component_manager so needs more work
            #   stationBeams    pssBeams
            #   pstBeams        jonesSource
            #   stations
            for attribute_name in (
                "state",
                "status",
                "adminMode",
                "healthState",
                "controlMode",
                "simulationMode",
                "testMode",
                "obsState",
                "obsMode",
                "configurationProgress",
                "configurationDelayExpected",
                "delaysValid",
                "assigned_processors",
            ):
                self._device.set_change_event(attribute_name, True, False)
                self._device.set_archive_event(attribute_name, True, False)

            my_name = self._device.get_name()
            # is there a better way to find my_id ?
            self._device._subarray_id = int(my_name.split("/")[-1])
            self._device.logger.info(
                f"{my_name} ID {self._device._subarray_id}"
            )
            self._device._subscribed_to_alloc = False
            self._device._admin_modes_using_health = [AdminMode.ONLINE]
            value = os.getenv(
                "ENGINEERING_MODE_IGNORE_HEALTH", default="false"
            )
            if value.lower() != "true":
                self._device._admin_modes_using_health.append(
                    AdminMode.ENGINEERING
                )

            message = "LowCbfSubarray init complete"
            self._device.logger.info(message)
            self._completed()
            return ResultCode.OK, message

            # this will break in v11 of ska-tango-base
            # (and we will get a whole new way of handling resources...)
            # device.resource_manager._key = "lowcbf"
            #
            # try:
            #     allocator = DeviceProxy(device.AllocatorAddress)
            #     device.subarray = Subarray(allocator, device._connect)
            #     # dirty hack to make resource manager have a link to subarray
            #     # object (for use in assign command)
            #     device.resource_manager.subarray = device.subarray
            #
            #     message = (
            #         f"LowCbfSubarray init complete"
            #         f", using allocator {device.AllocatorAddress}"
            #     )
            #     return ResultCode.OK, message
            # except Exception as e:
            #     message = "Exception connecting to Allocator"
            #     device.logger.error(f"{message}\n{e}")
            #     return ResultCode.FAILED, message

    # Inherited partially-implemented commands from ska-tango-base.subarray
    # ==== Command ====     ==CommandObject==       == method ==
    # AssignResources       "AssignResources"       "assign"
    # ReleaseResources      "ReleaseResources"      "release"
    # Configure             - replaced in ska-tango-base.csp.subarray -
    # ReleaseAllResources   "ReleaseAllResources"   "release"
    # Scan                  "Scan"                  "scan"
    # EndScan               "EndScan"               "end_scan"
    # End                   - replaced in ska-tango-base.csp.subarray -
    # Abort                 "Abort"                 abort (not submitted)
    # ObsReset              "ObsReset"              "obsreset"
    # Restart               "Restart"               "restart"

    # Inherited partially-implemented command from ska-tango-base:csp.subarray
    # ConfigureScan         "ConfigureScan"
    # Configure             - calls ConfigureScan
    # GoToIdle              "GoToIdle"
    # End                   - calls GoToIdle

    def _subscribe_to_connector(self):
        """Subscribe to healthState attribute change on all connectors"""
        try:
            db = Database(self.ConnectorDbHost, self.ConnectorDbPort)
        except Exception as e:
            # CI/CD k8s-test won't have the connector deployed so bail out
            self.logger.warning(f"DB EXCEPTION {e}")
            return
        em = self._processor_event_manager
        prefix = f"{self.ConnectorDbHost}:{self.ConnectorDbPort}/"
        dev_names = db.get_device_exported_for_class("LowCbfConnector")
        for fqdn in dev_names:
            full_path = prefix + fqdn
            self.logger.info(f"SUBSCRIBING to {full_path}")
            em.register_callback(
                self._health_callback, full_path, "healthState"
            )

    def _flush_alveos(self):
        "Purge Alveo details on ReleaseResources event"
        for sn in self._subscribed_sn:
            if sn not in self._sn_to_fqdn:
                continue
            fqdn = self._sn_to_fqdn[sn]
            if fqdn in self._component_health:
                self._component_health.pop(fqdn)
        self._assigned_proc_serials = []
        self.push_change_event("assigned_processors", "[]")
        self.push_archive_event("assigned_processors", "[]")
        self._subscribed_sn = []
        self._subscribed_fqdn = []

    # ----------
    # Callbacks
    # ----------
    def _fqdn_callback(self, name: str, jsonstr: str, quality) -> None:
        """Gets called when processor fqdn list gets updated in Allocator"""
        self.logger.info(f"from ALLOCATOR {name} {jsonstr}")
        procs = json.loads(jsonstr)
        self._sn_to_fqdn = procs

    def _internal_alveo_callback(self, name: str, jsonstr: str, quality):
        """Allocator calls this with information of mapping between Alveo
        serial number and a subarray it is allocated to"""
        self._delay_contributors = {}
        log = self.logger
        log.info(f"ALLOCATOR CFG {name}: {jsonstr}")
        cfg_dict = json.loads(jsonstr)
        if not cfg_dict:
            # resources are released: flush all references to Alveos
            log.info("flush all Alveos")
            self._flush_alveos()
            self._recalculate_health()
            return
        # handle ONLY configuration relevant to THIS subarray
        # probably the bit: "sa_id": 1  in cfg
        assigned_procs = set()
        for k, v in cfg_dict.items():
            if "regs" not in v:
                log.warning("UNEXPECTED format")
                break
            # NOTE PST and CORR have different key names
            key_names = ("sa_id", "subarray_id")
            for subarray in v["regs"]:
                if not subarray:
                    # skip empty register dicts representing unused subarray capacity
                    continue
                if all([_ not in subarray for _ in key_names]):
                    log.warning("MISSING subarray ID")
                    continue
                # get the subarray number using one of possible dict keys
                for key in key_names:
                    if key in subarray:
                        subarray_id = subarray[key]
                        break
                if self._subarray_id == subarray_id:
                    log.info(f"APPENDING {k} to subarray {subarray_id}")
                    assigned_procs.add(k)
            # 15-Jan-2024: formats are messy:
            # -------------------------------
            # for CORR:
            # {"XFL1TJCHM3ON": {
            #    "fw": "vis:0.0.2-main.e4b5ad79",
            #    "regs": [
            #        {
            #           "sa_id": 4,
            #           "stns": [[345, 1], [350, 1], [352, 1], [355, 1], [431, 1], [434, 1]],
            #           "sa_bm": [[1, 140, 0, 27648, 192, 24, 0]]
            #        }
            #      ]}}
            # for PST:
            # {"XFL1XCRTUC22  ": {
            #    "fw": "pst:0.0.20-dev.7c13dd33",
            #    "regs": [
            #        {
            #           "subarray_id": 4,
            #           "stn_bm_id": 1,
            #           "stns": [[2, 1], [3, 1], [345, 1], [350, 1], [352, 1], [355, 1], [431, 1], [434, 1 ]], "freq_ids": [140, 141, 142, 143, 144, 145, 146, 147], "pst_bm_ids": [15]}, {}, {}]}}

        assigned_procs = sorted(assigned_procs)
        # publish change in assigned processors (if any)
        if assigned_procs != self._assigned_proc_serials:
            report_str = json.dumps(assigned_procs)
            self.push_change_event("assigned_processors", report_str)
            self.push_archive_event("assigned_processors", report_str)
            self._assigned_proc_serials = assigned_procs

        if len(self._assigned_proc_serials) > 0 and not self._proc_thread:
            self._proc_thread = Thread(target=self._subscribe_processors)
            self._proc_thread.start()

    def _health_callback(self, fqdn: str, name: str, value, quality) -> None:
        """Called by LowCbfProcessor or LowCbfConnector when their health state
        changes. Recalculate overall LowCbfSubarray healthState and publish it
        to subscribers if changed."""

        def is_proc(fqdn):
            "Determine if the specified device name is a processor"
            return fqdn.find("processor") != -1

        with self._callback_lock:
            if is_proc(fqdn) and fqdn not in self._subscribed_fqdn:
                self.logger.info(f"{fqdn} not used in {self.get_name()}")
                return
            self.logger.info(f"DEV {fqdn} {name} val: {value} Q: {quality}")
            self._component_health[fqdn] = value
            # update/propagate healthState only when subarray is ONLINE
            if self._admin_mode in self._admin_modes_using_health:
                self._recalculate_health()

    def _recalculate_health(self):
        """Recalculate overall health state (across all devices) and
        propagate change upwards (Controller) if needed"""
        current_health = HealthState.UNKNOWN
        for hs in self._health_states:
            if hs in self._component_health.values():
                current_health = hs
                break
        if current_health != self._health_state:
            self._update_health_state(current_health)

    def _poly_delay_callback(
        self, fqdn: str, name: str, value, quality
    ) -> None:
        # serialise access: multiple processors can fire at the same time
        with self._callback_lock:
            self._update_delays_valid(fqdn, value)

    # ----------
    # Threads
    # ----------
    def _subscribe_allocator(self):
        """A short lived thread to subscribe to Allocator attributes informing
        us which Alveo cards were assigned to this subarray
        """
        alloc_proxy = MccsDeviceProxy(
            self.AllocatorAddress,
            self.logger,
            connect=False,
        )
        attribute_callbacks = (
            ("procDevFqdn", self._fqdn_callback),
            ("internal_alveo", self._internal_alveo_callback),
        )
        for att, cb in attribute_callbacks:
            alloc_proxy.evt_sub_on_connect(att, cb)
        try:
            alloc_proxy.connect()
            self._subscribed_to_alloc = True
        except Exception as e:
            self.logger.error(f"_subscribe_allocator failed: {e}")

    def _subscribe_processors(self):
        """As allocator assignes processors to this subarray, subscribe to
        their healthState attribute change."""
        assigned_procs = self._assigned_proc_serials  # short alias
        while True:
            time.sleep(1)
            for sn in [
                p for p in assigned_procs if p not in self._subscribed_sn
            ]:
                if sn not in self._sn_to_fqdn:
                    self.logger.info(f"CAN'T FIND {sn}")
                    continue
                # subscribe to new processors healthStatus(JSON) changes
                fqdn = self._sn_to_fqdn[sn]
                self.logger.info(f"SUBSCRIBING TO {fqdn} healthState")
                self._subscribed_sn.append(sn)
                self._subscribed_fqdn.append(fqdn)
                self._processor_event_manager.register_callback(
                    self._health_callback, fqdn, event_spec="healthState"
                )
                self.logger.info(f"SUBSCRIBING TO {fqdn} delays VALID")
                self._processor_event_manager.register_callback(
                    self._poly_delay_callback,
                    fqdn,
                    event_spec="subarrayDelaysSummary",
                )


# Run server
def main(args=None, **kwargs):
    """Main function of the LowCbfSubarray module."""
    return run((LowCbfSubarray,), args=args, **kwargs)


if __name__ == "__main__":
    main()
