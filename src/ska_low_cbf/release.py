# -*- coding: utf-8 -*-
"""Release information for ska_low_cbf"""
# pylint: disable=invalid-name,redefined-builtin

import importlib.metadata

name = __package__
version = importlib.metadata.version(__package__)
version_info = version.split(".")
description = """SKA Low CBF Monitoring & Control System"""
author = "CSIRO"
author_email = "andrew dot bolin at csiro dot au"
license = """CSIRO Open Source Licence"""
url = """https://www.skatelescope.org/"""
copyright = """CSIRO"""
