# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=invalid-name,protected-access,too-few-public-methods

""" SKA Low CBF

Delay simulator Tango device for Low.CBF
"""

# import tango
import json
import queue
import threading
import time
from json.decoder import JSONDecodeError

import tango
from schema import Schema  # ,  Use, Optional, Or, And, Forbidden
from ska_control_model import AdminMode
from ska_tango_base import SKABaseDevice
from ska_tango_base.commands import ResultCode  # FastCommand
from tango import DebugIt
from tango.server import attribute, command, device_property, run

from ska_low_cbf.delay_poly.delay_calcs import (
    VALIDITY_SECS,
    const_delays,
    katpoint_delays,
)
from ska_low_cbf.delay_poly.delay_component_manager import (
    DelayComponentManager,
)
from ska_low_cbf.delay_poly.stations import StationInfo
from ska_low_cbf.device_proxy import MccsDeviceProxy

__all__ = ["DelayDevice", "main"]

MAX_SOURCE_DIRECTIONS = 4
DEFAULT_UPDATE_SECS = 10.0  # floating point
ADVANCE_PUBLICATION_SECONDS = 0
ALLOCATOR_DEVICE = "low-cbf/allocator/0"
RA_DEC_DEFAULT = {"ra": "00h00m00.0s", "dec": "00d00m00.0s"}


class DelayDevice(SKABaseDevice):
    """
    DelayDevice simulates the SKA delay source that generates
    delay polynomials that subarrays use.
    """

    TelmodelSourceURI = device_property(dtype=str)

    def __init__(self, *args, **kwargs):
        # declared here (not InitCommand.do) to keep linting happy
        self._allocator_proxy = None
        self._cmd_q = queue.Queue()
        self._subarray_beams = {}  # from Allocator subscription

        self._src_directions = {}  # from SourceDirection commannds
        self._src_poly_cache = {}  # key="source_sNN_bMM_x"
        self._src_active = []  # names of actively updated sources
        self._src_katpoint_state = {}  # key="source_sNN_bMM_x"

        self._beam_directions = {}  # from BeamDirection commands
        self._beam_poly_cache = {}  # key="delay_sNN_bMM"
        self._beam_active = []  # names of actively updated beams
        self._beam_katpoint_state = {}  # key="delay_sNN_bMM"

        self._pst_directions = {}
        self._pst_poly_cache = {}
        self._pst_active = []  # names of actively updated PST directions
        self._pst_katpoint_state = {}

        self._update_period_secs = DEFAULT_UPDATE_SECS
        self._locations = None
        self._epoch_lock = threading.Lock()
        self._next_epoch = 0  # Seconds after SKA epoch used for next poly
        self._ypol_offset_ns = 0.0
        self._validity_secs = VALIDITY_SECS  # how long polynomials are valid
        self._poly_update_enable = True  # whether polys update or are held
        # SKA base class can now run InitCommand.do
        super().__init__(*args, **kwargs)

    # Inherited methods ==========================
    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """

    def create_component_manager(self):
        """Create a (mostly unused) component manager for this Tango device.
        (Overrides an abstract inherited method.)"""
        self.logger.info("run create_component_manager")
        return DelayComponentManager(
            logger=self.logger,
            communication_state_callback=self._communication_state_changed,
            component_state_callback=self._component_state_changed,
        )

    def init_command_objects(self):
        """
        Initialises the command handlers for commands supported by this device.
        """
        super().init_command_objects()
        self.logger.info("init_command_objects completed!")

    class InitCommand(SKABaseDevice.InitCommand):
        """Extends the SKABaseDevice class used for device initialisation
        to initialise this sub-classed device"""

        def do(self, *args, **kwargs) -> tuple[ResultCode, str]:
            """
            A stateless hook for device initialisation
            Initialises the attributes and properties this device subclass
            """
            super().do(*args, **kwargs)

            # Initialisation for this delay device starts here...

            # get station location info
            self._device._locations = StationInfo(
                self._device.TelmodelSourceURI, self.logger
            )
            # subscribe to allocator.internal_subarray for subarray info
            self._device._allocator_proxy = None
            self._device._subscribe_to_allocator(ALLOCATOR_DEVICE)

            # spawn thread to regularly push poly values
            self._device._update_period_secs = DEFAULT_UPDATE_SECS
            self._device._updater_thread = threading.Thread(
                target=self._device._epoch_thread, args=()
            )
            self._device._updater_thread.start()

            # End of delay device initialisation
            message = "LowCBF Delay Simulator InitCommand complete"
            self._device.logger.info(message)
            return ResultCode.OK, message

    # Start of Tango Device Attributes ==========================

    # Allow the delay polynomial update period to be read/written
    @attribute(dtype=float, memorized=False, hw_memorized=False)
    def update_seconds(self):
        """
        Get current delay polynomial update period (seconds)
        """
        return self._update_period_secs

    @update_seconds.write  # type: ignore[no-redef]
    def update_seconds(self, value: float):
        """Set period for update of polynomial"""
        self._update_period_secs = value

    @attribute(dtype=str, memorized=False, hw_memorized=False)
    def subarray_beams(self):
        """Get subarray beams"""
        return json.dumps(self._subarray_beams)

    @attribute(dtype=str, memorized=False, hw_memorized=False)
    def beam_directions(self):
        """Get current beam directions"""
        return json.dumps(self._beam_directions)

    @attribute(dtype=str, memorized=False, hw_memorized=False)
    def source_directions(self):
        """Get current source directions"""
        return json.dumps(self._src_directions)

    # override disables ska-tango-base adminMode state machine: always ONLINE
    @attribute(dtype=AdminMode, memorized=False, hw_memorized=False)
    def adminMode(self: SKABaseDevice) -> AdminMode:
        return AdminMode.ONLINE

    @adminMode.write  # type: ignore[no-redef]
    def adminMode(self: SKABaseDevice, value: AdminMode) -> None:
        self.logger.warning(
            "admin_mode is always ONLINE, cannot set %s", value
        )

    @attribute(dtype=float, memorized=False, hw_memorized=False)
    def ypol_offset_ns(self):
        """
        Get current static offset between X and Y for all stations.
        """
        return self._ypol_offset_ns

    @ypol_offset_ns.write  # type: ignore[no-redef]
    def ypol_offset_ns(self, value: float):
        """Set current static offset between X and Y for all stations"""
        self._ypol_offset_ns = value

    @attribute(dtype=bool, memorized=False, hw_memorized=False)
    def enable_poly_updates(self: SKABaseDevice) -> bool:
        """Get current update state"""
        return self._poly_update_enable

    @enable_poly_updates.write  # type: ignore[no=redef]
    def enable_poly_updates(self: SKABaseDevice, enable: bool) -> None:
        """Enable/disable polynomial updates"""
        self._poly_update_enable = enable

    @attribute(dtype=float, memorized=False, hw_memorized=False)
    def validity_seconds(self: SKABaseDevice) -> float:
        """Get current polynomial validity in seconds"""
        return self._validity_secs

    @validity_seconds.write  # type: ignore[no=redef]
    def validity_seconds(self: SKABaseDevice, valid_secs: float) -> None:
        """Set future polynomial validity interval (seconds)"""
        self._validity_secs = valid_secs

    # Start of Tango Device Commands ==========================

    @command(
        dtype_in="DevString",
        doc_in="Subarray configuration, JSON string",
    )
    @DebugIt()
    def ConfigureSubarray(self, argin):
        """
        Called to configure a dynamic attribute for a subarray

        :param argin: JSON string representing dictionary of
        subarray parameters for delays: stations, frequencies, sources
        source locations

        :return: None
        """
        try:
            params = json.loads(argin)
        except JSONDecodeError as err:
            self.logger.error("Error decoding JSON parameter: %s", err)
            return

        assert isinstance(params, dict), "Dictionary required"
        # TODO
        # self._poly.add_subarray_delay(params)
        return

    @command(
        dtype_in="DevLong",
        doc_in="Station number (1..512)",
        dtype_out="DevString",
        doc_out="Capabilities Available, JSON string",
    )
    @DebugIt()
    def GetStationInfo(self, argin):
        """Retrieve data about station location"""
        info = self._locations.get_info(argin)
        return json.dumps(info)

    @command(dtype_in=str, dtype_out=None)
    def subscribetoallocator(self, argin):
        """
        Temp method called to cause Processor to subscribe to events from
        a Tango device that has attributes providing allocation info

        :param argin: A string containing a Tango device name. Device must have
        the three "internal_XXX" attributes eg "low-cbf/allocator/0"
        """
        device_name = argin
        self._subscribe_to_allocator(device_name)

    _beam_radec_schema = Schema(
        {
            "subarray_id": int,
            "beam_id": int,
            "direction": {"ra": str, "dec": str},
        }
    )

    @command(
        dtype_in="DevString",
        doc_in="subarray_id, beam_id, ra_hms, dec",
    )
    @DebugIt()
    def BeamRaDec(self, argin):
        """
        Set station-beam pointing direction as RightAscention/Declination coord
        Delay values will update to track RA/DEC as the sky moves
        """
        params = json.loads(argin)
        valid_params = self._beam_radec_schema.validate(params)
        self.logger.info("beam directions=%s", valid_params)
        self._cmd_q.put({"beam_dirn": valid_params})

    _beam_azel_schema = Schema(
        {
            "subarray_id": int,
            "beam_id": int,
            "direction": {"az": str, "el": str},
        }
    )

    @command(
        dtype_in="DevString",
        doc_in="subarray_id, beam_id, az_degrees, el_degrees",
    )
    @DebugIt()
    def BeamAzEl(self, argin):
        """
        Set station-beam pointing direction as an Azimuth/Elevation
        Delays will be calculated from AZ/EL, but will not vary with sky motion
        """
        params = json.loads(argin)
        valid_params = self._beam_azel_schema.validate(params)
        self.logger.info("beam directions=%s", valid_params)
        self._cmd_q.put({"beam_dirn": valid_params})

    _beam_stn_delay_schema = Schema(
        {
            "subarray_id": int,
            "beam_id": int,
            "delay": [
                {"stn": int, "nsec": float},
            ],
        }
    )

    @command(
        dtype_in="DevString",
        doc_in="subarray_id, beam_id, az_degrees, el_degrees",
    )
    @DebugIt()
    def BeamDelay(self, argin):
        """
        Set individual delays for stations contributing to a station-beam
        Individual station delays will be as specified by command args
        """
        params = json.loads(argin)
        valid_params = self._beam_stn_delay_schema.validate(params)
        self.logger.info("beam directions=%s", valid_params)
        self._cmd_q.put({"beam_dirn": valid_params})

    _source_radec_schema = Schema(
        {
            "subarray_id": int,
            "beam_id": int,
            "direction": [
                {"ra": str, "dec": str},
            ],
        }
    )

    @command(
        dtype_in="DevString",
        doc_in="subarray_id, beam_id, [{ra_deg, dec_hms}]",
    )
    @DebugIt()
    def SourceRaDec(self, argin):
        """
        Set directions for the four possible CNIC sources using RA/DEC coords
        Source delay polynomials will be updated with sky movement
        """
        params = json.loads(argin)
        valid_params = self._source_radec_schema.validate(params)
        self.logger.info("source directions=%s", valid_params)
        self._cmd_q.put({"source_dirn": valid_params})

    _source_azel_schema = Schema(
        {
            "subarray_id": int,
            "beam_id": int,
            "direction": [
                {"az": str, "el": str},
            ],
        }
    )

    @command(
        dtype_in="DevString",
        doc_in="subarray_id, beam_id, [{az_degrees, el_degrees}]",
    )
    @DebugIt()
    def SourceAzEl(self, argin):
        """
        Set directions for the four possible CNIC sources using AZ/EL coords
        Source delay polynomials will be calculated from the AZ/EL but will
        not change with time as AZ/EL is independent of sky motion
        """
        params = json.loads(argin)
        valid_params = self._source_azel_schema.validate(params)
        self.logger.info("source directions=%s", valid_params)
        self._cmd_q.put({"source_dirn": valid_params})

    _source_stn_delay_schema = Schema(
        {
            "subarray_id": int,
            "beam_id": int,
            "delay": [
                [
                    {"stn": int, "nsec": float},
                ],
            ],
        }
    )

    @command(
        dtype_in="DevString",
        doc_in="subarray_id, beam_id, [[{stn_id, nsec}, ], ]",
    )
    @DebugIt()
    def SourceDelay(self, argin):
        """
        Set individual delays for stations generating data for the four sources
        that CNICs can generate
        Individual station delays will be constant, specified by command args
        """
        params = json.loads(argin)
        valid_params = self._source_stn_delay_schema.validate(params)
        self.logger.info("source station_delays=%s", valid_params)
        self._cmd_q.put({"source_dirn": valid_params})

    _pst_radec_schema = Schema(
        {
            "subarray_id": int,
            "beam_id": int,
            "direction": [
                {"ra": str, "dec": str},
            ],
        }
    )

    @command(
        dtype_in="DevString",
        doc_in="JSON: {subarray_id, beam_id, dirn [{ra (deg), dec (hms)}]}",
    )
    @DebugIt()
    def PstRaDec(self, argin):
        """
        Set directions for PST beams as RA/DEC values
        Source delay polynomials will be updated with sky movement
        """
        params = json.loads(argin)
        valid_params = self._pst_radec_schema.validate(params)
        self.logger.info("PST RADEC directions=%s", valid_params)
        self._cmd_q.put({"pst_dirn": valid_params})

    _pst_azel_schema = Schema(
        {
            "subarray_id": int,
            "beam_id": int,
            "direction": [
                {"az": str, "el": str},
            ],
        }
    )

    @command(
        dtype_in="DevString",
        doc_in="JSON: {subarray_id, beam_id, dirn [{az (deg), el (deg)},]}",
    )
    @DebugIt()
    def PstAzEl(self, argin):
        """
        Set directions for PST beams as AZ/EL
        Delay polynomials will be calculated from the AZ/EL but will
        not change with time as AZ/EL is independent of sky motion
        """
        params = json.loads(argin)
        valid_params = self._pst_azel_schema.validate(params)
        self.logger.info("PST AZ_EL directions=%s", valid_params)
        self._cmd_q.put({"pst_dirn": valid_params})

    _pst_stn_delay_schema = Schema(
        {
            "subarray_id": int,
            "beam_id": int,
            "delay": [
                [
                    {"stn": int, "nsec": float},
                ],
            ],
        }
    )

    @command(
        dtype_in="DevString",
        doc_in="JSON: {subarray_id, beam_id, delay [[{stn (id), nsec}, ], ]}",
    )
    @DebugIt()
    def PstDelay(self, argin):
        """
        Set individual delays for PST beams as delays
        Individual station delays will be constant, specified by command args
        """
        params = json.loads(argin)
        valid_params = self._pst_stn_delay_schema.validate(params)
        self.logger.info("PST station_delays=%s", valid_params)
        self._cmd_q.put({"pst_dirn": valid_params})

    @command(
        dtype_in="DevLong",
        doc_in="Seconds after SKA Epoch for next delay poly",
    )
    @DebugIt()
    def SetSecondsAfterEpoch(self, argin):
        """
        Command to allow the simulation time to be set to any desired
        number of seconds after SKA epoch (midnight, 1 January 2000 TAI)
        (Currently must be within the 32-bit capability of the
        V1 SPS packet counter ie 2^32 packets = 9.5M seconds = 109.9 days)
        """
        with self._epoch_lock:
            self._next_epoch = argin

    # End of Tango Device commands ==================

    def _epoch_thread(self) -> None:
        """
        Thread that processes subarray-beam updates and also
        calculates delay polynomials at each update period
        """

        # Start at 0 seconds after TAI epoch
        secs_after_epoch = 0

        self._next_epoch = secs_after_epoch + self._update_period_secs
        next_update_time = time.time() + self._update_period_secs
        while True:
            # delay until the next polynomial updates are due
            wait_time = max(0, next_update_time - time.time())
            try:
                arg = self._cmd_q.get(timeout=wait_time)

                # is the queued parameter a new subarray definition
                new_subarray_beams = arg.get("subarray", None)
                if new_subarray_beams is not None:
                    # queue not empty: subarray beam update (from allocator)
                    self._update_beams(new_subarray_beams)
                    continue

                # is the new queued parameter a beam direction
                beam_direction = arg.get("beam_dirn", None)
                if beam_direction is not None:
                    self._set_beam_dir(beam_direction)
                    continue

                # is the new queued parameter a source direction
                src_direction = arg.get("source_dirn", None)
                if src_direction is not None:
                    self._set_src_dir(src_direction)
                    continue

                # is the new queued parameter a PST beam
                pst_direction = arg.get("pst_dirn", None)
                if pst_direction is not None:
                    self._set_pst_dir(pst_direction)
                    continue

            except queue.Empty:
                # Timeout: next epoch calculation due now
                self._on_epoch(self._next_epoch)
                with self._epoch_lock:
                    self._next_epoch += self._update_period_secs
                next_update_time += self._update_period_secs
                # if compute too slow (already at later epoch) skip epochs
                while time.time() > next_update_time:
                    with self._epoch_lock:
                        self._next_epoch += self._update_period_secs
                    next_update_time += self._update_period_secs

    def _on_epoch(self, epoch):
        """
        Re-calculate all delay polynomials and push attribute updates
        """
        if len(self._src_active) == 0 and len(self._beam_active) == 0:
            return

        if not self._poly_update_enable:
            self.logger.warning("Polynomial updates are disabled")
            return

        self.logger.info("Seconds after SKA Epoch: %d", epoch)
        # calculate updated beam delays
        for beam_name in self._beam_active:
            subarray = int(beam_name[7:9])
            beam = int(beam_name[11:])
            subarray_info = self._subarray_beams.get(subarray)
            stations = subarray_info.get("stns")
            direction = self._beam_directions[subarray][beam]
            katpoint_state = self._beam_katpoint_state[beam_name]
            if isinstance(direction, list):
                # Direction is list of delays for each stn
                self._beam_poly_cache[beam_name] = const_delays(
                    katpoint_state,
                    stations,
                    direction,
                    epoch + ADVANCE_PUBLICATION_SECONDS,
                    self._update_period_secs,
                    self.logger,
                    ypol_offset_ns=self._ypol_offset_ns,
                    validity_secs=self._validity_secs,
                )
            else:
                # Direction is az/el or ra/dec dictionary
                self._beam_poly_cache[beam_name] = katpoint_delays(
                    katpoint_state,
                    stations,
                    direction,
                    epoch + ADVANCE_PUBLICATION_SECONDS,
                    self._locations,
                    self._update_period_secs,
                    self.logger,
                    ypol_offset_ns=self._ypol_offset_ns,
                    validity_secs=self._validity_secs,
                )
        # Note: Publish later, last of all, (for PSS/PST validity flags)

        # update source delays
        for src_name in self._src_active:
            subarray = int(src_name[8:10])
            beam = int(src_name[12:14])
            src_idx = int(src_name[15:]) - 1
            subarray_info = self._subarray_beams.get(subarray)
            stations = subarray_info.get("stns")
            direction = self._src_directions[subarray][beam][src_idx]
            katpoint_state = self._src_katpoint_state[src_name]
            if isinstance(direction, list):
                # Direction is list of delays for each stn
                self._src_poly_cache[src_name] = const_delays(
                    katpoint_state,
                    stations,
                    direction,
                    epoch,
                    self._update_period_secs,
                    self.logger,
                    ypol_offset_ns=self._ypol_offset_ns,
                    validity_secs=self._validity_secs,
                )
            else:
                # Direction is az/el or ra/dec dictionary
                self._src_poly_cache[src_name] = katpoint_delays(
                    katpoint_state,
                    stations,
                    direction,
                    epoch,
                    self._locations,
                    self._update_period_secs,
                    self.logger,
                    ypol_offset_ns=self._ypol_offset_ns,
                    validity_secs=self._validity_secs,
                )
        # publish updated source delay attributes
        for src_name in self._src_active:
            value = self._src_poly_cache[src_name]
            self.push_change_event(src_name, json.dumps(value))

        # update PST delays
        for pst_name in self._pst_active:
            subarray = int(pst_name[5:7])
            beam = int(pst_name[9:11])
            src_idx = int(pst_name[12:]) - 1
            subarray_info = self._subarray_beams.get(subarray)
            stations = subarray_info.get("stns")
            direction = self._pst_directions[subarray][beam][src_idx]
            katpoint_state = self._pst_katpoint_state[pst_name]
            # stn-beam poly to use for differencing of polys
            stn_beam_name = f"delay_{pst_name[4:11]}"
            ref = self._beam_poly_cache[stn_beam_name]
            if isinstance(direction, list):
                # Direction is list of delays for each stn
                self._pst_poly_cache[pst_name] = const_delays(
                    katpoint_state,
                    stations,
                    direction,
                    epoch,
                    self._update_period_secs,
                    self.logger,
                    # ref, No ref - no subtraction for constant delays
                    ypol_offset_ns=self._ypol_offset_ns,
                    validity_secs=self._validity_secs,
                )
            else:
                # Direction is az/el or ra/dec dictionary
                self._pst_poly_cache[pst_name] = katpoint_delays(
                    katpoint_state,
                    stations,
                    direction,
                    epoch,
                    self._locations,
                    self._update_period_secs,
                    self.logger,
                    ref,
                    ypol_offset_ns=self._ypol_offset_ns,
                    validity_secs=self._validity_secs,
                )
        # publish updated pst delay attributes
        for pst_name in self._pst_active:
            value = self._pst_poly_cache[pst_name]
            self.push_change_event(pst_name, json.dumps(value))

        # publish updated beam delay attributes
        for beam_name in self._beam_active:
            value = self._beam_poly_cache[beam_name]
            self.push_change_event(beam_name, json.dumps(value))

    def _subscribe_to_allocator(self, tango_device_name) -> None:
        """
        Implementation of subscription to allocation attributes on allocator

        :param tango_device_name: eg 'low-cbf/allocator/0'
        """
        self.logger.info("Subscribe to %s subarray events", tango_device_name)
        self._allocator_proxy = MccsDeviceProxy(
            tango_device_name, self.logger, connect=False
        )

        alloc_evt_hndlrs = {
            "internal_subarray": self._internal_subarray_evt_handler,
        }
        for attr_name, hndlr in alloc_evt_hndlrs.items():
            self._allocator_proxy.evt_sub_on_connect(attr_name, hndlr)
        # try:
        #     # self.logger.info("Before subscribe")
        self._allocator_proxy.connect(max_time=120.0)

    def _internal_subarray_evt_handler(self, attr_name, evt_json, quality):
        """Receive subarray change events from Allocator"""
        self.logger.info("EVENT from %s (%s)", attr_name, quality)
        subarrays_published = json.loads(evt_json)
        assert isinstance(
            subarrays_published, dict
        ), "Unexpected type for Allocator internal_subarray attribute"

        # extract subarray-beam info
        new_subarray_beams = {}
        for subarray_id, subarray_info in subarrays_published.items():
            subarray_int_id = int(subarray_id)  # undo change by JSON
            new_info = {
                "stns": subarray_info["stns"],
                "stn_beam_ids": [
                    beam["beam_id"] for beam in subarray_info["stn_beams"]
                ],
            }
            new_subarray_beams[subarray_int_id] = new_info

        self._cmd_q.put({"subarray": new_subarray_beams})

    def _update_beams(self, new_subarray_beams: dict):
        """Called to process queued command parameters"""

        # get list of added beams
        new_beams = []
        for sbry, desc in new_subarray_beams.items():
            for beam in desc["stn_beam_ids"]:
                if (sbry not in self._subarray_beams) or (
                    beam not in self._subarray_beams[sbry]["stn_beam_ids"]
                ):
                    new_beams.append([sbry, beam])
        # get list of deleted beams
        deleted_beams = []
        for sbry, desc in self._subarray_beams.items():
            for beam in desc["stn_beam_ids"]:
                if (sbry not in new_subarray_beams) or (
                    beam not in new_subarray_beams[sbry]["stn_beam_ids"]
                ):
                    deleted_beams.append([sbry, beam])

        # remove attributes from subarray-beams that no longer exist
        for sbry, beam in deleted_beams:
            name = f"delay_s{int(sbry):02d}_b{beam:02d}"
            self._deactivate_delay_attr(name)
            self._deactivate_src_attr(sbry, beam)
            self._deactivate_pst_attr(sbry, beam)

        self._subarray_beams = new_subarray_beams

        # Create attributes for each added subarray-beam
        for sbry, beam in new_beams:
            # ensure there is a default direction before creating
            if sbry not in self._beam_directions:
                self._beam_directions[sbry] = {}
            if beam not in self._beam_directions[sbry]:
                self._beam_directions[sbry][beam] = self.FIXED_BEAM_DLY
            self._create_delay_attr(sbry, beam)
            # ensure thre are default src direction before creating
            if sbry not in self._src_directions:
                self._src_directions[sbry] = {}
            if beam not in self._src_directions[sbry]:
                self._src_directions[sbry][beam] = self.FIXED_SRC_DLY
            self._create_src_attrs(sbry, beam)
            # ensure PST delay attrs exist
            self._create_pst_attrs(sbry, beam)

    FIXED_BEAM_DLY = [
        {"stn": 0, "nsec": 0.0},
    ]

    FIXED_SRC_DLY = [
        [
            {"stn": 0, "nsec": 0.0},
        ]
        for _ in range(0, 4)
    ]

    def _create_delay_attr(self, sbry, beam):
        """Add a delay-polynomial attribute for a beam"""
        attr_name = f"delay_s{int(sbry):02d}_b{beam:02d}"
        if attr_name not in self._beam_poly_cache:
            self.logger.info("Create beam delay attribute: %s", attr_name)
            attr = tango.server.attribute(
                name=attr_name,
                dtype=tango.DevString,
                access=tango.AttrWriteType.READ,
                label=attr_name,
                fread=self._read_poly,
                fwrite=None,
                polling_period=0,
            )

            try:
                self.add_attribute(attr)
            except tango.DevFailed as df:
                self.logger.info("Failed to create %s: %s", attr_name, df)
                return
            # set as manually pushed, not auto-detected
            self.set_change_event(attr_name, True, False)
        else:
            self.logger.info("Re-use delay attribute %s", attr_name)

        stns = self._subarray_beams[sbry]["stns"]
        self._beam_poly_cache[attr_name] = self._zero_polys(stns)
        self._beam_active.append(attr_name)
        self._beam_katpoint_state[attr_name] = {}

    def _create_src_attrs(self, subarray_id, beam_id):
        if (subarray_id not in self._src_directions) or (
            beam_id not in self._src_directions[subarray_id]
        ):
            self.logger.info(
                "No sources for subarray %s, beam %s", subarray_id, beam_id
            )
            return
        srcs = self._src_directions[subarray_id][beam_id]
        for count in range(0, len(srcs)):
            attr_name = (
                f"source_s{subarray_id:02d}_b{beam_id:02d}_{count+1:1d}"
            )
            if attr_name not in self._src_poly_cache:
                self.logger.info(
                    "Create source delay attribute: %s", attr_name
                )

                attr = tango.server.attribute(
                    name=attr_name,
                    dtype=tango.DevString,
                    access=tango.AttrWriteType.READ,
                    label=attr_name,
                    fread=self._read_src_poly,
                    fwrite=None,
                    polling_period=0,
                )
                try:
                    self.add_attribute(attr)
                except tango.DevFailed as df:
                    self.logger.info("Failed to create %s: %s", attr_name, df)
                    continue
                # set as manually pushed, not auto-detected
                self.set_change_event(attr_name, True, False)
            else:
                self.logger.info("Re-use src attribte: %s", attr_name)
            stns = self._subarray_beams[subarray_id]["stns"]
            self._src_poly_cache[attr_name] = self._zero_polys(stns)
            self._src_katpoint_state[attr_name] = {}
            self._src_active.append(attr_name)

    def _create_pst_attrs(self, subarray_id, beam_id):
        if (subarray_id not in self._pst_directions) or (
            beam_id not in self._pst_directions[subarray_id]
        ):
            self.logger.info(
                "No pst beams for subarray %s, beam %s", subarray_id, beam_id
            )
            return
        psts = self._pst_directions[subarray_id][beam_id]
        for count in range(0, len(psts)):
            attr_name = f"pst_s{subarray_id:02d}_b{beam_id:02d}_{count+1:1d}"
            if attr_name not in self._pst_poly_cache:
                self.logger.info("Create pst delay attribute: %s", attr_name)

                attr = tango.server.attribute(
                    name=attr_name,
                    dtype=tango.DevString,
                    access=tango.AttrWriteType.READ,
                    label=attr_name,
                    fread=self._read_pst_poly,
                    fwrite=None,
                    polling_period=0,
                )
                try:
                    self.add_attribute(attr)
                except tango.DevFailed as df:
                    self.logger.error("Failed to create %s: %s", attr_name, df)
                    continue
                # set as manually pushed, not auto-detected
                self.set_change_event(attr_name, True, False)
            else:
                self.logger.info("Re-use pst attribte: %s", attr_name)
            stns = self._subarray_beams[subarray_id]["stns"]
            self._pst_poly_cache[attr_name] = self._zero_polys(stns)
            self._pst_katpoint_state[attr_name] = {}
            self._pst_active.append(attr_name)

    def _set_beam_dir(self, beam_dir):
        """Configure a beam direction"""
        # shorten long names
        subarray_id = int(beam_dir["subarray_id"])
        beam_id = int(beam_dir["beam_id"])
        dirn = beam_dir.get("direction", None)
        if dirn is None:
            dirn = beam_dir["delay"]
        if subarray_id not in self._beam_directions:
            self._beam_directions[subarray_id] = {}
        self._beam_directions[subarray_id][beam_id] = dirn
        # if we're updating an in-use beams, force recalculation
        attr_name = f"delay_s{subarray_id:02d}_b{beam_id:02d}"
        if attr_name in self._beam_active:
            self._beam_katpoint_state[attr_name] = {}

    def _set_src_dir(self, src_dir):
        """Configure a source direction"""
        # shorten long names
        subarray_id = src_dir["subarray_id"]
        beam_id = src_dir["beam_id"]
        dir_list = src_dir.get("direction", None)
        if dir_list is None:
            dir_list = src_dir["delay"]
        if len(dir_list) > MAX_SOURCE_DIRECTIONS:
            dir_list = dir_list[:MAX_SOURCE_DIRECTIONS]
        if subarray_id not in self._src_directions:
            self._src_directions[subarray_id] = {}
        self._src_directions[subarray_id][beam_id] = dir_list
        # if we're updating in-use sources, force recalculation
        for i in range(0, len(dir_list)):
            attr_name = f"source_s{subarray_id:02d}_b{beam_id:02d}_{i+1:1d}"
            if attr_name in self._src_active:
                self._src_katpoint_state[attr_name] = {}

    def _set_pst_dir(self, pst_dir):
        """Configure PST directions"""
        # shorten long names
        subarray_id = pst_dir["subarray_id"]
        beam_id = pst_dir["beam_id"]
        # we have one of two types of spec "direction" or "delay"
        dir_list = pst_dir.get("direction", None)
        if dir_list is None:
            dir_list = pst_dir["delay"]

        if subarray_id not in self._pst_directions:
            self._pst_directions[subarray_id] = {}
        self._pst_directions[subarray_id][beam_id] = dir_list

        # if we're updating in-use pst-beams, force recalculation
        for i in range(0, len(dir_list)):
            attr_name = f"pst_s{subarray_id:02d}_b{beam_id:02d}_{i+1:1d}"
            if attr_name in self._pst_active:
                self._pst_katpoint_state[attr_name] = {}

    def _read_poly(self, attr: tango.server.attribute):
        """Called when a delay-polynomial attribute is read by client"""
        try:
            attr_name = attr.get_name()
        except AttributeError as error:
            self.logger.warning("Atrribute error: %s", error)
            return
        value = self._beam_poly_cache.get(attr_name, {})
        attr.set_value(json.dumps(value))

    def _deactivate_delay_attr(self, attr_name):
        """
        Deactivate/stop updating polynomials for a pointing-delay attribute

        We used to delete the Tango attribute but Tango has bugs. Instead
        we mark the beam as unused and leave the attribute in existence
        """
        self.logger.info("Stop attribute updates: %s", attr_name)
        self._beam_active.remove(attr_name)

    def _deactivate_src_attr(self, subarray, beam):
        """
        Deactiveate/stop updating all source attributes for subarray beam

        We used to delete the Tango attribute but Tango has bugs. Instead
        we mark the beam as unused and leave the attribute in existence
        """
        prefix = f"source_s{subarray:02d}_b{beam:02d}_"
        attrs_to_remove = []
        for attr_name in self._src_active:
            if not attr_name.startswith(prefix):
                continue
            attrs_to_remove.append(attr_name)
        for attr_name in attrs_to_remove:
            self.logger.info("Stop attribute updates: %s", attr_name)
            self._src_active.remove(attr_name)

    def _deactivate_pst_attr(self, subarray, beam):
        """
        Deactiveate/stop updating all PST attributes for subarray beam

        We used to delete the Tango attribute but Tango has bugs. Instead
        we mark the beam as unused and leave the attribute in existence
        """
        prefix = f"pst_s{subarray:02d}_b{beam:02d}_"
        attrs_to_remove = []
        for attr_name in self._pst_active:
            if not attr_name.startswith(prefix):
                continue
            attrs_to_remove.append(attr_name)
        for attr_name in attrs_to_remove:
            self.logger.info("Stop attribute updates: %s", attr_name)
            self._pst_active.remove(attr_name)

    def _read_src_poly(self, attr: tango.server.attribute):
        """Called when a source-polynomial attribute is read by client"""
        try:
            attr_name = attr.get_name()
        except AttributeError as error:
            self.logger.warning("Atrribute error: %s", error)
            return
        value = self._src_poly_cache.get(attr_name, {})
        attr.set_value(json.dumps(value))

    def _read_pst_poly(self, attr: tango.server.attribute):
        """Called when a pst-polynomial attribute is read by client"""
        try:
            attr_name = attr.get_name()
        except AttributeError as error:
            self.logger.warning("Atrribute error: %s", error)
            return
        value = self._pst_poly_cache.get(attr_name, {})
        attr.set_value(json.dumps(value))

    def _zero_polys(self, stns):
        """initial all-zero station delay polynomials"""
        detail = []
        for stn_id in stns:
            stn_poly_info = {
                "station_id": stn_id[0],
                "substation_id": stn_id[1],
                "xypol_coeffs_ns": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                "ypol_offset_ns": 0,
            }
            detail.append(stn_poly_info)

        poly = {
            "interface": "https://schema.skao.int/ska-low-csp-delaymodel/1.0",
            "start_validity_sec": 0,
            "cadence_sec": self._update_period_secs,
            "validity_period_sec": VALIDITY_SECS,
            "config_id": "",  # blank like leafNode -> discard in processor
            "station_beam": 0,
            "subarray": 0,
            "station_beam_delays": detail,
        }
        return poly


# Run Tango device server
def main(args=None, **kwargs):
    """Main function of the Low module."""
    return run((DelayDevice,), args=args, **kwargs)


if __name__ == "__main__":
    main()
