# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=invalid-name,protected-access,too-few-public-methods

""" SKA Low CBF

Delay simulator Tango device for Low.CBF

This file contains katpoint-related code to calculate delay polynomials for
a beam or a source
"""

import time
from typing import Optional

import numpy as np
from astropy.coordinates import Angle, SkyCoord
from astropy.time import Time, TimeDelta
from astropy.utils import iers
from katpoint import Target
from katpoint.delay_correction import DelayCorrection
from realtime.receive.core import antenna_utils

from ska_low_cbf.delay_poly.stations import StationInfo

# disable download of latest earth rotation model (starts ~30seconds faster)
iers.conf.auto_download = False

POLY_DEGREE = 5
VALIDITY_SECS = 600.0


def katpoint_delays(
    katpoint_state: dict,
    stations: list,
    direction,
    epoch: int,
    locations: StationInfo,
    cadence_sec: float,
    logger,
    reference_polys: Optional[dict] = None,
    ypol_offset_ns: float = 0,
    validity_secs: float = VALIDITY_SECS,
):
    """
    Calculate delay polynomial for stations, epoch, and direction of
      source or beam

    :param katpoint_state: calculation-state storage for this beam
    :param stations: list of [station, sub-station] tuples
    :param direction: ra/dec of beam or source
    :param epoch: unix epoch at which to calculate
    :param locations: object with station geo locations
    :param cadence_sec: time in seconds btw polynomial updates
    :param reference_polys: Dictionary containing station beam polynomials.
      Only present when delays are to be calculated as offsets from another
      beam's polynomials. Otherwise None.
    :param ypol_offset_ns: Offset between X & Y polarisations, nanoseconds
    """

    if "ants" not in katpoint_state:
        stopwatch_start_time = time.time()
        # select stations (locations) by matching the station's ID field
        stns = []
        # add centre-of-array into the stations list as first station
        array_centre_is_phase_ref = False
        for stn in locations.all_stns:
            if stn["station_id"] == 0:
                stns.append(stn)
                array_centre_is_phase_ref = True
                break
        if array_centre_is_phase_ref:
            logger.info("Using Array Centre as phase reference")
        else:
            logger.info("Using first station as phase reference")
        katpoint_state["ac_phase_ref"] = array_centre_is_phase_ref
        # add all subarray stations into the list
        for stn_substn_id in stations:
            for stn in locations.all_stns:
                if stn["station_id"] == stn_substn_id[0]:
                    stns.append(stn)
                    break
        katpoint_state["stations"] = stns
        # Use first antenna as phase ref (== Array Centre if present)
        ref_antenna_index = 0
        ants = antenna_utils.load_antennas(stns)
        KatPointAnts = []
        for ant in ants:
            # katpointant = ant.as_KatPointAntenna(ref_location=ref_location)
            katpointant = ant.as_KatPointAntenna()
            KatPointAnts.append(katpointant)
        katpoint_state["ants"] = KatPointAnts
        duration = time.time() - stopwatch_start_time
        logger.info("load katpoint antennas: %f secs", duration)
    array_centre_is_phase_ref = katpoint_state["ac_phase_ref"]
    ants = katpoint_state["ants"]

    if "delay_correction" not in katpoint_state:
        stopwatch_start_time = time.time()
        delay_correction = DelayCorrection(
            ants=KatPointAnts, ref_ant=KatPointAnts[ref_antenna_index]
        )
        katpoint_state["delay_correction"] = delay_correction
        duration = time.time() - stopwatch_start_time
        logger.info("katpoint delay correction: %f secs", duration)
    del_cor = katpoint_state["delay_correction"]

    if "tgt" not in katpoint_state:
        stopwatch_start_time = time.time()
        if "ra" in direction:  # RA/DEC direction spec
            try:
                c = SkyCoord(direction["ra"], direction["dec"], frame="icrs")
            except ValueError as val_err:
                logger.warning("Using direction RA=0, DEC=0, [%s]", val_err)
                c = SkyCoord("00h00m00.0s", "00d00m00.0s", frame="icrs")
            logger.info(
                "Target @ ra=%s radian, dec=%s radian",
                c.ra.radian,
                c.dec.radian,
            )
            tgt_katpoint = Target.from_radec(c.ra.radian, c.dec.radian)
        else:  # AZ/EL direction spec
            try:
                az = Angle(direction["az"])
                el = Angle(direction["el"])
            except ValueError as val_err:
                logger.warning("Using direction Az=0, EL=0, [%s]", val_err)
                az = Angle("00d00m00.0s")
                el = Angle("00d00m00.0s")
            logger.info(
                "Target @ az=%s radian, el=%s radian", az.radian, el.radian
            )
            tgt_katpoint = Target.from_azel(az, el)
        katpoint_state["tgt"] = tgt_katpoint
        duration = time.time() - stopwatch_start_time
        logger.info("katpoint target: %f secs", duration)
    tgt = katpoint_state["tgt"]

    # Times at which to find delaypoly ('epoch' means seconds after SKA epoch)
    stopwatch_start_time = time.time()

    # time is relative to SKA epoch
    ska_epoch = Time("2000-01-01T00:00:00", scale="tai")
    t0 = ska_epoch + TimeDelta(epoch, format="sec")

    # Times around epoch where delay is evaluated for poly fitting
    delta_sec = np.linspace(0, validity_secs, POLY_DEGREE + 1)
    timestamp_array = [
        t0 + TimeDelta(delta_sec[i], format="sec")
        for i in range(0, POLY_DEGREE + 1)
    ]

    # Calculate delays
    delays = del_cor.delays(tgt, timestamp_array)

    # get polynomial fitted to delays for each station
    first_stn_ant = 0
    if array_centre_is_phase_ref:
        first_stn_ant = 1  # skip first station which is array centre
    polys = []
    for ant_idx in range(first_stn_ant, len(ants)):
        # we use the same values both polarisations,
        # so we can skip half the entires here
        y = np.array(
            delays[ant_idx * 2].value * 1e9
        )  # 1e9: seconds->nanoseconds
        polynomial = np.polynomial.Polynomial.fit(delta_sec, y, POLY_DEGREE)
        polynomial_coefficients = polynomial.convert().coef
        polys.append(polynomial_coefficients.tolist())
    duration = time.time() - stopwatch_start_time
    logger.info("katpoint delay calc: %f secs", duration)

    delta_polys = _calc_difference_polys(stations, polys, reference_polys)

    return _package_polys(
        stations,
        delta_polys,
        epoch,
        cadence_sec,
        ypol_offset_ns,
        validity_secs,
    )


def _calc_difference_polys(
    stns: list, polys: list, reference_polys: Optional[dict]
) -> list:
    """Difference the polys for each station with the references (PST)"""
    if reference_polys is None:
        return polys

    diff_polys = []
    for count, delay_spec in enumerate(reference_polys["station_beam_delays"]):
        assert delay_spec["station_id"] == stns[count][0], "bad stn order"
        assert delay_spec["substation_id"] == stns[count][1], "bad sstn order"
        ref_stn_poly = delay_spec["xypol_coeffs_ns"]
        diff = [
            polys[count][idx] - ref_stn_poly[idx]
            for idx in range(0, POLY_DEGREE + 1)
        ]
        diff_polys.append(diff)

    return diff_polys


def _package_polys(
    stns: list,
    polys: list,
    epoch: int,
    cadence_sec: float,
    ypol_offset_ns: float = 0,
    validity_secs: float = VALIDITY_SECS,
) -> dict:
    """put polynomial info in expected format"""
    all_polys = []
    for count, stn_id in enumerate(stns):
        stn_poly_info = {
            "station_id": stn_id[0],
            "substation_id": stn_id[1],
            "xypol_coeffs_ns": polys[count],
            "ypol_offset_ns": ypol_offset_ns,
        }
        all_polys.append(stn_poly_info)

    poly = {
        "interface": "https://schema.skao.int/ska-low-csp-delaymodel/1.0",
        "start_validity_sec": epoch,
        "cadence_sec": cadence_sec,
        "validity_period_sec": validity_secs,
        "config_id": "test",  # non-blank value to avoid discard
        "station_beam": 0,  # not checked by processor
        "subarray": 0,  # not checked by processor
        "station_beam_delays": all_polys,
    }
    return poly


def const_delays(
    state: dict,
    stations: list,
    fixed_stn_delay_list,
    epoch: int,
    cadence_sec: float,
    logger,
    ypol_offset_ns: float = 0,
    validity_secs: float = VALIDITY_SECS,
):
    """
    Assemble delay polynomial from constants for stations, epoch,

    :param state: calculation-state storage for this beam
    :param stations: list of [station, sub-station] tuples
    :param fixed_stn_delay_list: constant delays for beam or source
    :param epoch: unix epoch at which to calculate
    :param cadence_sec: time in seconds btw polynomial updates
    :param ypol_offset_ns: Offset between X & Y polarisations, nanoseconds
    """

    if "stn_polys" not in state:
        stopwatch_start_time = time.time()
        polys = []
        for stn_id, substn_id in stations:
            poly = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
            for stn_delay in fixed_stn_delay_list:
                if stn_delay["stn"] == stn_id:
                    poly = [stn_delay["nsec"], 0.0, 0.0, 0.0, 0.0, 0.0]
                    break
            polys.append(poly)
        state["stn_polys"] = polys
        duration = time.time() - stopwatch_start_time
        logger.info("setup fixed delays: %f secs", duration)
    delay_polys = state["stn_polys"]

    return _package_polys(
        stations,
        delay_polys,
        epoch,
        cadence_sec,
        ypol_offset_ns,
        validity_secs,
    )
