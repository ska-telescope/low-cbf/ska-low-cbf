# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=invalid-name,protected-access,too-few-public-methods

""" SKA Low CBF

Delay simulator Tango device for Low.CBF
This file contains code to establish location of stations
"""

import json
import threading

from ska_telmodel.data import TMData

NUM_LOW_STATIONS = 512
IF_GEO = "https://schema.skao.int/ska-telmodel-layout-location-geocentric/0.0"
IF_LOC = "https://schema.skao.int/ska-telmodel-layout-location/0.0"


class StationInfo:
    """Class to hold geographic info about station"""

    def __init__(self, telmodel_source_uri, logger=None):
        self._telmodel_source_uri = telmodel_source_uri
        self._logger = logger
        self._stns = []
        # while waiting for telmodel data, init all stations at same place
        for idx in range(0, NUM_LOW_STATIONS):
            this_stn = {
                "station_name": f"TMP-{idx:03d}",
                "station_id": idx,
                "location": {
                    "geocentric": {
                        "coordinate_frame": "ITRF",
                        "interface": IF_GEO,
                        "x": -2562839.409288656,
                        "y": 5104718.946838843,
                        "z": -2828499.863822968,
                    },
                    "geodetic": {
                        "coordinate_frame": "WGS84",
                        "h": 0.0,
                        "interface": IF_GEO,
                        "lat": -26.49752899999999,
                        "lon": 116.659076,
                    },
                    "interface": "IF_LOC",
                },
            }
            self._stns.append(this_stn)
        # spawn thread to download telmodel data
        self._tm_thread = threading.Thread(target=self._get_telmodel_data)
        self._tm_thread.start()

    def _get_telmodel_data(self):
        """
        Get updated location data from Telmodel data store
        This method is run in a thread from __init__
        """
        if self._logger:
            self._logger.info("Start read telmodel location data")
        t = TMData(source_uris=[self._telmodel_source_uri])
        loc_str = t["instrument/ska1_low/layout/low-layout.json"].get()
        loc = json.loads(loc_str)
        stns = list(loc["receptors"])
        self._stns = stns
        if self._logger:
            self._logger.info(
                "%s Station locations updated from Telmodel", len(stns)
            )

    def get_info(self, stn_num):
        """Retrieve info about one station"""
        assert (
            0 < stn_num <= NUM_LOW_STATIONS
        ), f"Station number {stn_num} out of range (1-{NUM_LOW_STATIONS})"
        idx = stn_num - 1  # convert 1-based station numbering to 0-based array
        return self._stns[idx]

    @property
    def all_stns(self):
        """Get list of all stations"""
        return self._stns
