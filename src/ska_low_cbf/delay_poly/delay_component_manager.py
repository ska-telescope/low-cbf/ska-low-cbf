# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2022 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
SKA Low CBF
Delay-simulator Component Manager
"""

import logging

from ska_tango_base.base import (
    BaseComponentManager,
    TaskCallbackType,
    check_communicating,
)
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import CommunicationStatus, PowerState
from ska_tango_base.executor import TaskStatus


class DelayComponentManager(BaseComponentManager):
    """
    Component Manager for delay simulator

    This is unused boilerplate, because the delay simulator does not have
    hardware to monitor or control
    """

    def __init__(
        self,
        logger: logging.Logger,
        communication_state_callback,  #: Callable[[CommunicationStatus]],
        component_state_callback,  #: Callable,
    ):
        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            power=PowerState.UNKNOWN,
            fault=None,
        )

    # inherited abstract BaseComponentManager methods, needing implementation
    def start_communicating(self):
        self._update_communication_state(CommunicationStatus.ESTABLISHED)
        self._update_component_state(power=PowerState.ON, fault=False)

    def stop_communicating(self):
        self._update_communication_state(CommunicationStatus.DISABLED)

    @check_communicating
    def off(
        self,
        _: TaskCallbackType | None = None,
    ) -> tuple[TaskStatus, str]:
        message = "Ignored OFF, delay-sim does not have hardware"
        self.logger.error(message)
        return (ResultCode.REJECTED, message)

    @check_communicating
    def standby(
        self,
        _: TaskCallbackType | None = None,
    ) -> tuple[TaskStatus, str]:
        message = "Ignored STANDBY, delay-sim not does have hardware"
        self.logger.error(message)
        return (ResultCode.REJECTED, message)

    @check_communicating
    def on(
        self,
        _: TaskCallbackType | None = None,
    ) -> tuple[TaskStatus, str]:
        message = "Ignored ON, delay-sim does not have hardware"
        self.logger.error(message)
        return (ResultCode.REJECTED, message)

    @check_communicating
    def reset(
        self,
        _: TaskCallbackType | None = None,
    ) -> tuple[TaskStatus, str]:
        message = "Ignored RESET, delay-sim does not have hardware"
        self.logger.error(message)
        return (ResultCode.REJECTED, message)

    @check_communicating
    def abort_commands(
        self,
        _: TaskCallbackType | None = None,
    ) -> tuple[TaskStatus, str]:
        message = "Ignored Abort, delay-sim does not queue commands"
        self.logger.error(message)
        return (ResultCode.REJECTED, message)
