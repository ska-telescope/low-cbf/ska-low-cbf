"""
This file defines classes to represent hardware items
- Alveo FPGAs,
- P4 Switches,

P4s have "is_shared" and "subarray_access" attributes that record
whether the item is shared or exclusive-access, and which subarrays have
access to the item
"""

# pylint: disable=too-many-arguments

from typing import List

from ska_low_cbf.allocator.firmware_pss import FirmwarePss
from ska_low_cbf.allocator.firmware_pst import FirmwarePst
from ska_low_cbf.allocator.firmware_vis import FirmwareVis


class Alveo:
    """
    Class representing an alveo, recording how it attaches to switch
    """

    def __init__(
        self,
        alveo_id: str,
        switch_id: str,
        switch_port: int,
        serial_no: str,
        hw: str,
        tango_dev: str | None,
        admin_mode: int,
    ) -> None:
        # Alveo parameters that do not change
        self.hardware = hw
        self._alveo_id = alveo_id
        self._switch_id = switch_id
        self._switch_port = switch_port
        self.serial_no = serial_no
        self.tango_dev = tango_dev
        self.admin_mode = admin_mode
        # Alveo parameters that change when firmware is loaded
        self.fw_image = None
        self.fw_mode = None
        self.fw_descr = {}  # dict of info about firmware
        self.fw_name_override = None  # for testing only (remove??)
        # Internal personality-specific info
        self._fw_regs = None

    @property
    def alveo_id(self):
        """Get the ID of this Alveo"""
        return self._alveo_id

    @property
    def switch_id(self):
        """Get the ID of the switch to which the Alveo is connected"""
        return self._switch_id

    @property
    def switch_port(self):
        """Get the port of the switch to which the Alveo is connected"""
        return self._switch_port

    def assign_firmware(self, fw_type, limits, fw_image):
        """Create structures that Alveo FPGAs use to store
        information about their firmware - needed for register settings"""
        if fw_type == "pst":
            self._fw_regs = FirmwarePst(limits)
        elif fw_type == "vis":
            self._fw_regs = FirmwareVis(limits)
        elif fw_type == "pss":
            self._fw_regs = FirmwarePss(limits)
        else:
            raise ValueError(f"Unknown firmware type: {fw_type}")
        self.fw_image = fw_image
        self.fw_mode = fw_type

    @property
    def has_firmware(self):
        """Whether Alveo has a firmware type"""
        return self._fw_regs is not None

    def is_in_use(self):
        """Whethere Alveo FPGA is in use"""
        if self._fw_regs is None:
            return False
        if self.fw_mode == "vis":
            return self._fw_regs.vchans_in_use() != 0
        if self.fw_mode == "pst":
            return self._fw_regs.pipes_used() != 0
        if self.fw_mode == "pss":
            return self._fw_regs.pipes_used() != 0
        return False

    def delete_firmware(self):
        """Delete Correlator/PST data structs (when FPGA is no longer used)"""
        self._fw_regs = None
        self.fw_image = None

    def pst_reserve(self, subarray_id, beam_id, stns, frq_ids, pst_beam_ids):
        """Reserve a PST signal_processing/vct instance"""
        return self._fw_regs.pst_reserve(
            subarray_id, beam_id, stns, frq_ids, pst_beam_ids
        )

    def pss_reserve(self, subarray_id, beam_id, stns, frq_ids, pst_beam_ids):
        """Reserve a PSS signal_processing/vct instance"""
        return self._fw_regs.pss_reserve(
            subarray_id, beam_id, stns, frq_ids, pst_beam_ids
        )

    def delete_reservation(self) -> None:
        """Remove a subarray reservation from correlator/PST FPGA"""
        if self._fw_regs is None:
            return
        self._fw_regs.delete_reservation()
        # Remove firmware too if reservation was only the user of the FPGA
        if not self.is_in_use:
            self.delete_firmware()

    def has_reservation(self) -> bool:
        """Whether the FPGA has some reservation in it"""
        if self._fw_regs is None:
            return False
        return self._fw_regs.has_reservation()

    def accept_reservation(self) -> None:
        """Place a subarray beam-frequency in Correlator/PST FPGA"""
        if self._fw_regs is None:
            return
        accepted = self._fw_regs.accept_reservation()
        if accepted:
            print(f"{self._alveo_id} accepted {self.fw_mode} reservation")

    def vis_reserve(
        self, subarry_stn_ids, subarray_id, stn_bm_id, freq_id, is_long
    ) -> bool:
        """Make beam-frequency reservation in Correlator FPGA"""
        return self._fw_regs.vis_reserve(
            subarry_stn_ids, subarray_id, stn_bm_id, freq_id, is_long
        )

    def clear_subarray(self, sbarry):
        """Remove everything relating to subarray from VCT"""
        if self._fw_regs is None:
            return
        still_used = self._fw_regs.clear_subarray(sbarry)
        if not still_used:
            self.delete_firmware()

    def get_sps_routing_info(self) -> list:
        """
        :return: (subarray_id, stn_beam_id, freq_id, stn_id, sub-stn_id) part
        of every entry in the VCT to be used for SPS routing, order unimportant
        """
        if self._fw_regs is None:
            return []
        return self._fw_regs.get_sps_routing_info()

    def get_subarray_clients(self) -> list:
        """Get list of subarrays using this FPGA"""
        if self._fw_regs is None:
            return []
        return self._fw_regs.subarray_clients()

    def get_internal_regs(self) -> list:
        """
        Get info that Processor devices require to configure their internal
        registers

        :return: list, one entry per signal processing instance in the FPGA
        """
        if self._fw_regs is None:
            return []
        return self._fw_regs.get_internal_regs()

    def get_usage(self) -> list:
        """Get usage by subarray"""
        if self._fw_regs is None:
            return []
        return self._fw_regs.get_usage()


class P4Switch:
    """
    Class representing a P4 switch and its network connections to
    alveos, switches, IO
    """

    def __init__(self) -> None:

        self.alveos = {}  # dict of alveo class objects, key = alveo_id
        self._links = (
            {}
        )  # index = sw_port -> [{"speed": speed, "to": dest_id}, ]
        self._port_to = {}  # index = dest_id -> (sw_port, speed)

    def alveo_ids(self) -> List[str]:
        """Get list of all alveo_ids attached to the switch"""
        return self.alveos.keys()

    def get_alveo_serial_nums(self) -> List[str]:
        """Get list of all alveo serial numbers attached to switch"""
        serial_nos = []
        for alveo in self.alveos.values():
            serial_nos.append(alveo.serial_no)
        return serial_nos

    def get_alveo(self, alveo_id) -> Alveo:
        """Get access to an Alveo class instance attached to the switch"""
        return self.alveos[alveo_id]

    def add_alveo(self, alveo):
        """Add an Alveo object to this switch"""
        self.alveos[alveo.alveo_id] = alveo

    # TODO is this really needed?
    def add_link(self, switch_port: str, speed: int, dest_id: str):
        """Record what switch port connects to"""
        if switch_port not in self._links:
            self._links[switch_port] = []
        self._links[switch_port].append({"speed": speed, "to": dest_id})
        if dest_id not in self._port_to:
            self._port_to[dest_id] = []
        self._port_to[dest_id].append((switch_port, speed))

    def port_to(self, dest_id):
        """Get (port, speed) where dest_id attaches"""
        if dest_id not in self._port_to:
            return None
        return self._port_to[dest_id]

    def port_to_named_links(self, link_name: str):
        """
        Get list of ports connected to destination name

        :param link_name: (beginning of) destination name
        :return: list of ports connecting to the name
        """
        portlist = []
        for port, link_list in self._links.items():
            for link in link_list:
                if link["to"].startswith(link_name):
                    portlist.append(port)
                    break
        return portlist
