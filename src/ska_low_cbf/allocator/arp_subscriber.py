# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""Classes that subscribe to Connector ARP reply events."""

import logging
import random
from abc import ABC
from enum import IntEnum
from queue import Queue
from threading import Lock, Thread, Timer

import tango
from tango import DevFailed, DeviceProxy, EventData, EventSystemFailed

PROCESS_EVTS_IN_CALLER_CONTEXT = False
READ_INITIAL_ATTR_VALUE = False


class Evt(IntEnum):
    """Events relevant to ARP subscription states."""

    ON_ENTRY = 0
    ON_EXIT = 1
    SUBSCRIBE = 2
    TIMEOUT = 3
    UNSUBSCRIBE = 4
    SUBSCRIBE_FAILURE = 5


class SimpleStateMachine(ABC):
    """State Machine logic for processing events"""

    def __init__(self, logger: logging.Logger):
        """
        Create state machine that handles events received
        via the "add_event" function. Processing runs-to-completion
        for each event before the next event is unqueued & processed

        :param logger: for processing log messages.
        """
        self._cur_state = None
        self._evt_queue = Queue()  # queue of input events to be processed
        self._logger = logger
        self._lock = Lock()  # protects self._is_processing
        self._is_processing = False

    def initial_state(self, initial_state):
        """Set starting state for handler."""
        self.transition(initial_state)
        self._process_evts()

    def transition(self, next_state):
        """Transition from one event handling state to another."""
        if self._cur_state is not None:
            self._cur_state(Evt.ON_EXIT, None)
        self._cur_state = next_state
        self._cur_state(Evt.ON_ENTRY, None)

    def add_evt(self, evt, evt_data):
        """
        Source of new events for the state machine to handle
        Add an event to the queue of events to be processed.
        """
        self._evt_queue.put((evt, evt_data))
        # Ensure only one thread of execution processes events
        # at a time to get run-to-completion in-order behaviour
        with self._lock:
            if self._is_processing:
                return
            self._is_processing = True

        if PROCESS_EVTS_IN_CALLER_CONTEXT:
            # This can lead to deadlocks if caller is Tango event handler
            self._process_evts()
        else:
            # Use separate thread of execution to process queued events
            thr = Thread(
                target=self._process_evts,
                args=(),
            )
            thr.start()

    def _process_evts(self):
        """Process all events in queue until queue is empty."""
        self._lock.acquire()
        while not self._evt_queue.qsize() == 0:
            self._lock.release()

            # get next event & run current state's handling for the event
            evt, evt_data = self._evt_queue.get()
            self._cur_state(evt, evt_data)

            self._lock.acquire()
        self._is_processing = False
        self._lock.release()


class ArpSubscriber(SimpleStateMachine):
    """Class handling subscriptions to connector ARP replies."""

    def __init__(self, logger):
        """
        Create a Delay Polynomial Subscriber.

        :param logger: for processing log messages.
        """
        super().__init__(logger)

        self._dev_name = None  # Name of tango device we subscribe to
        self._device_proxy = None  # proxy for device we subscribe to
        self._attr_name = None  # Name of attribute we subscribe to
        self._subscription_id = None  # id of successful subscription
        self._subscription_cbk = None  # Client callback
        self._timer = None  # for retry of failed subscriptions

        # Start in "idle state"
        self.initial_state(self._state_idle)
        logger.info("ArpSubscriber init complete")

    # Methods to handle events for each state

    def _state_idle(self, evt: Evt, evt_data):
        """Handle events in idle state."""
        if evt == Evt.SUBSCRIBE:
            self._dev_name, self._attr_name, self._subscription_cbk = evt_data
            self._do_try_subscribe()
            return
        # ON_ENTRY - nothing to do
        # ON_EXIT - nothing to do
        # TIMEOUT (impossible) - ignore
        # SUBSCRIBE_FAILURE - not possible in idle, ignore
        # UNSUBSCRIBE - ignore

    def _state_retry_wait(self, evt: Evt, _):
        """Retry event subscription."""
        if evt == Evt.ON_ENTRY:
            # Set time to wait in this state before retrying subacribe
            retry_secs = 10.0 + 2.0 * random.random()
            self._timer = Timer(retry_secs, self._on_timer_timeout)
            self._timer.start()
            return
        if evt == Evt.TIMEOUT:
            del self._timer
            self._timer = None
            self._do_try_subscribe()
            return
        if evt == Evt.UNSUBSCRIBE:
            self._timer.cancel()
            self._timer = None
            self.transition(self._state_idle)
            return
        # ON_EXIT - nothing to do
        # SUBSCRIBE_FAILURE - not possible in retry_wait
        # SUBSCRIBE - ignore

    def _state_subscribed(self, evt: Evt, _):
        """Handle Events received when successfully subscribed."""
        if evt == Evt.SUBSCRIBE_FAILURE:
            self.transition(self._state_retry_wait)
            return
        if evt == Evt.UNSUBSCRIBE:
            self._subscription_cbk = None
            self.transition(self._state_idle)
            return
        if evt == Evt.ON_EXIT:
            self._logger.info(
                "Unsubscribing %s, id=%s",
                self._attr_name,
                self._subscription_id,
            )
            self._device_proxy.unsubscribe_event(self._subscription_id)
            self._subscription_id = None
            del self._device_proxy
            self._device_proxy = None
            return
        if evt == Evt.ON_ENTRY:
            # read initial value of attribute just subscribed
            if READ_INITIAL_ATTR_VALUE:
                attr_full = self._device_proxy.read_attribute(self._attr_name)
                if self._subscription_cbk is not None:
                    self._subscription_cbk(attr_full.value)
        # SUBSCRIBE - ignore, already subscribed
        # TIMEOUT - ignore

    def _do_try_subscribe(self):
        """Attempt to subscribe to Tango attribute events."""
        self._logger.info(
            "Try subscribing to %s/%s ...", self._dev_name, self._attr_name
        )
        try:
            # Attempt to get device proxy for subscription target
            self._device_proxy = DeviceProxy(self._dev_name)
        except DevFailed:
            self._logger.info("Subscription fail: no Tango device)")
            self.transition(self._state_retry_wait)
            return

        try:
            # Attempt to subscribe to attribute
            self._subscription_id = self._device_proxy.subscribe_event(
                self._attr_name,
                tango.EventType.CHANGE_EVENT,
                self._tango_event_receiver,
                stateless=True,
            )
        except EventSystemFailed:
            # Tango docs say this never occurs if stateless=True
            del self._device_proxy
            self._device_proxy = None
            self._logger.info("Subscription fail: Tango evt sys fail")
            self.transition(self._state_retry_wait)
            return

        # Successful subscription
        self._logger.info("Subscription success: id %s", self._subscription_id)
        self._logger.info("Tango proxy %s", self._device_proxy)
        self.transition(self._state_subscribed)

    def _on_timer_timeout(self):
        """
        Called on timer timeout: Queue timeout event for state machine
        """
        self.add_evt(Evt.TIMEOUT, None)

    def _tango_event_receiver(self, tango_data: EventData):
        """
        Called by Tango when subscribed attribute notifies an update
        """
        if tango_data.err:
            for err in tango_data.errors:
                self._logger.warning("%s %s", tango_data.device, err.desc)
            self.add_evt(Evt.SUBSCRIBE_FAILURE, None)
            return

        # Call subscriber's handler with event value
        if self._subscription_cbk is not None:
            self._subscription_cbk(tango_data.attr_value.value)
        else:
            # Apparently Tango events can be received after unsubscribing!
            self._logger.warning("ARP event received while not subscribed")

    # Interface to external callers

    def subscribe(self, tango_dev_name, tango_attr_name, callback):
        """
        Subscribe to notifications from tango device attribute.

        :param tango_dev_name: FQDN of device for subscription
        :param tango_attr_name: name of device attribute for subscription
        """
        self.add_evt(
            Evt.SUBSCRIBE, (tango_dev_name, tango_attr_name, callback)
        )

    def unsubscribe(self):
        """Unsubscribe from tango attribute"""
        self.add_evt(Evt.UNSUBSCRIBE, None)
