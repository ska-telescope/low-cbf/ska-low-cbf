# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2022 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
SKA Low CBF
Allocator Component Manager
"""
import logging

from ska_tango_base.base import BaseComponentManager
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import CommunicationStatus, PowerState

# from typing import Callable


class AllocatorComponentManager(BaseComponentManager):
    """
    Component Manger for Allocator Device
    The Allocator is always-on, and does not manage a device at the
    end of a comms link so this component manager is not realy needed.
    It allows for the Allocator to be turned on in a Tango sense, but
    none of the commands check for on, and will work regardless.

    It is retained in case long-running commands are needed in future
    """

    def __init__(
        self,
        logger: logging.Logger,
        communication_state_callback,  #: Callable[[CommunicationStatus]],
        component_state_callback,  #: Callable,
    ):
        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            power=PowerState.UNKNOWN,
            fault=None,
        )

    def start_communicating(self):
        # Intended to start comms to a hardware device but Allocator hasn't any
        # Called when adminMode is changed to ONLINE
        self._update_communication_state(CommunicationStatus.ESTABLISHED)
        self._update_component_state(power=PowerState.ON, fault=False)

    def stop_communicating(self):
        # Intended to stop comms to a hardware device but Allocator hasn't any
        # Called when adminMode changed to OFFLINE
        self._update_component_state(power=PowerState.OFF, fault=False)
        self._update_communication_state(CommunicationStatus.DISABLED)

    def off(self, _):  # 2nd arg is "task_callback: Callable"
        message = "Ignored OFF, allocator does not have hardware"
        self.logger.error(message)
        return (ResultCode.REJECTED, message)

    def standby(self, _):  # 2nd arg is "task_callback: Callable"
        message = "Ignored STANDBY, allocator not does have hardware"
        self.logger.error(message)
        return (ResultCode.REJECTED, message)

    def on(self, _):  # 2nd arg is "task_callback: Callable"
        message = "Ignored ON, allocator does not have hardware"
        self.logger.error(message)
        return (ResultCode.REJECTED, message)

    def reset(self, _):  # 2nd arg is "task_callback: Callable"
        message = "Ignored RESET, allocator does not have hardware"
        self.logger.error(message)
        return (ResultCode.REJECTED, message)
