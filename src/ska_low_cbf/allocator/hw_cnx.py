"""
This file defines utility functions that extract information from a
Connection List, creating a structure of P4Switch and Alveo
instances representing hardware

The functions rely on specific keyword names that must exist in the
dictionaries contained in the Connection List and a specific format
for the entries. This file exists to confine any code changes that
may result from future format or keyword changes to this one file.
"""
from dataclasses import dataclass
from typing import List


class InterSwLinks:
    """
    Tools for finding Switch Ports that have fiber links to other switches

    (could move into own python file since it doesn't depend on string format)
    """

    def __init__(self):
        self._links = {}

    def _add_unidirection(self, sw1, port1, sw2):
        """Record that sw1 links to sw1 by sw1:port1"""
        if sw1 not in self._links:
            self._links[sw1] = {}
        if sw2 not in self._links[sw1]:
            self._links[sw1][sw2] = []
        self._links[sw1][sw2].append(port1)

    def add(self, sw1, port1, sw2, port2):
        """Record that sw1 links to sw2 bidirectionally"""
        self._add_unidirection(sw1, port1, sw2)
        self._add_unidirection(sw2, port2, sw1)

    def get_ports(self, sw1, sw2) -> list:
        """
        Get list of ports on sw1 that link to sw2
        :return: list (possibly empty)
        """
        if sw1 not in self._links:
            return []
        if sw2 not in self._links[sw1]:
            return []
        return self._links[sw1][sw2]


def _analyse_cnx2(cnx: List[dict]) -> dict:
    """
    Analyse a list of connections between P4 switches, Alveos, and Servers

    returns:
    - Dictionary key=link_type, value=dict of connection points
    """

    io_locn = {"stn": {}, "vis": {}, "pss": {}, "pst": {}}
    links = _get_p4_links(cnx)
    for sw_id, sw_port, _, dest_id in links:  # link speed unused
        # record where each IO item is connected
        if dest_id[0:4] == "stn_":
            io_locn["stn"][int(dest_id[4:])] = (sw_id, sw_port)
        elif dest_id[0:4] == "pst_":
            io_locn["pst"][dest_id[4:]] = (sw_id, sw_port)
        elif dest_id[0:4] == "pss_":
            io_locn["pss"][dest_id[4:]] = (sw_id, sw_port)
        elif dest_id[0:4] == "vis_":
            io_locn["vis"][dest_id[4:]] = (sw_id, sw_port)
    return io_locn


def _get_intersw_links(cnx: list[dict]) -> InterSwLinks:
    """
    Get interswitch link object from hardware_connections list
    """
    iswl = InterSwLinks()
    for item in cnx:
        if "switch2" not in item:
            continue
        sw1 = item["switch"]
        port1 = item["port"]
        sw2 = item["switch2"]
        port2 = item["port2"]
        iswl.add(sw1, port1, sw2, port2)
    return iswl


def _get_switch_ids(cnx: List[dict]) -> list:
    """
    Extract switch IDs from hardware_connections list

    :return: list of switch IDs
    """
    all_switch_ids = set()
    for itm in cnx:
        all_switch_ids.add(itm["switch"])
        if "switch2" in itm:
            all_switch_ids.add(itm["switch2"])
    return list(all_switch_ids)


def _get_alveo_info(cnx: List[dict]) -> dict:
    """
    Extract all alveo connection to switch from hardware_connections list

    :return: dict: key=alveoID, value=(switch, switch_port, alveo_serial)
    """
    all_alveos = {}
    alveo_no = 1  # 1-based numbering
    for itm in cnx:
        if "alveo" in itm:
            alv_id = f"alveo_{alveo_no:03d}"
            alveo_no += 1
            sw_id = itm["switch"]
            sw_port = itm["port"]
            serial = itm["alveo"]
            all_alveos[serial] = (sw_id, sw_port, alv_id)
    return all_alveos


def _get_p4_links(cnx: List[dict]) -> list:
    """
    Get list of all links from P4 switches in the hardware_connections list

    :return: list of switch-connected items
    """
    all_links = []
    alveo_no = 1  # 1-based numbering
    for itm in cnx:
        assert "switch" in itm, "No connection table, or malformed table"
        sw_id = itm["switch"]
        sw_port = itm["port"]
        speed = itm["speed"]
        if "alveo" in itm:
            dest_id = f"alveo_{alveo_no:03d}"
            alveo_no += 1
            all_links.append((sw_id, sw_port, speed, dest_id))
            continue
        if "switch2" in itm:
            sw2_id = itm["switch2"]
            sw2_port = itm["port2"]
            all_links.append((sw_id, sw_port, speed, sw2_id))
            all_links.append((sw2_id, sw2_port, speed, sw_id))
            continue
        if "station" in itm:  # TODO is this used?
            dest_id = itm["station"]
            all_links.append((sw_id, sw_port, speed, dest_id))
            continue
        if "link" in itm:
            io_id = itm["link"]
            all_links.append((sw_id, sw_port, speed, io_id))
    return all_links


@dataclass
class HwLayout:
    """
    Structure describing hardware connections derived from the
    helm chart "hardware_connections" list
    """

    link_recs: List[tuple]
    # Summary list of hardware_connections (sw_id, sw_port, speed, dest_id)
    intersw_links: InterSwLinks
    # Object used to access info about links between switches
    alv_recs: dict  # (sw_id, sw_port, alv_id) key=serial_no
    # switch and switch port where each alveo card is connected
    io_locn: dict  # (switch_id, port, speed, _)=io_locn["stn"][stn_id]
    # switch and port where STN/PST/PSS/SDP endpoints are connected
    sw_recs: list
    # List of all switch IDs in the connections list


def get_hw_layout(cnx_info: List[dict]):
    """
    Get hardware connections layout by parsing the list describing
    the switch port to which each hardware item connects
    """
    link_recs = _get_p4_links(cnx_info)
    io_locn = _analyse_cnx2(cnx_info)
    alv_recs = _get_alveo_info(cnx_info)
    intersw_links = _get_intersw_links(cnx_info)
    sw_recs = _get_switch_ids(cnx_info)

    return HwLayout(link_recs, intersw_links, alv_recs, io_locn, sw_recs)
