"""This module implements a resource manager used by the Allocator
to manage sharing of Alveo & P4 resources used by subarrays"""

import copy
from dataclasses import dataclass

from ska_low_cbf.allocator.arp_replies import ArpReplies
from ska_low_cbf.allocator.hardware import Alveo, P4Switch
from ska_low_cbf.allocator.hw_cnx import HwLayout, get_hw_layout
from ska_low_cbf.allocator.subarray_config import (
    SubarrayConfig,
    get_data_products,
)
from ska_low_cbf.allocator.subarray_dests import SubarrayDests

ALVEO_LIMIT_DEFAULTS = {
    "pst": {
        "vch": 900,  # max VCH due to beamformer clock frequency
        "sps_ch": 128,  # packetiser 2k first chans->2k/16 = 128 chans max
    },
    "pss": {
        "vch": 960,  # Max VCH due to intermittent failures near 1020 vch
        "sps_ch": 1024,  # Max SPS chans per pipeline (VCT limit)
    },
    "vis": {
        "vch": 1024,  # Max Virtual Channels into FPGA
        "hbm": 606,  # Max VChans in one matrix correlator's (MxC) 2Bbyte HBM
        "bli": (512 * 513) // 2,  # Max baselines in one MxC
    },
}


class Resources:
    """Class to represent Manager of P4, Alveo resources"""

    def __init__(self, array_config, cnx_data):
        self._max_stations = array_config["stations"]
        self._max_channels = array_config["channels"]
        # contains description of each FPGA personality
        self._alveo_limits = copy.deepcopy(ALVEO_LIMIT_DEFAULTS)
        # contains switch object instances, each switch has alveo objects
        self._switches = {}  # dict key: switch_id
        # Dict of switch objects (Alveos added to switches on registration)
        self._subarrays = {}  # dict key: subarray_id
        # Subarray SDP destinations for traffic injected at each switch
        self._subarray_traf_dests = {}

        self._hw_layout: HwLayout = get_hw_layout(cnx_data)
        # dataclass holding info on how everything connects to the switches

        # create a switch object for each switch that was found in Connections
        for sw_id in self._hw_layout.sw_recs:
            if sw_id not in self._switches:
                new_sw = P4Switch()
                self._switches[sw_id] = new_sw

        # add IO links to each P4 switch instance
        for sw_id, sw_port, speed, dest_id in self._hw_layout.link_recs:
            p4_sw = self._switches[sw_id]
            p4_sw.add_link(sw_port, speed, dest_id)

        # Note: Alveo cards are added to switches when they register

    @property
    def max_stations(self):
        """Get max stations for array release TODO eventually eliminate"""
        return self._max_stations

    @property
    def max_channels(self):
        """Get max channels for array release TODO eventually eliminate"""
        return self._max_channels

    # This only used in testing
    @property
    def switch_ids(self):
        """Get list of P4 switches that the resource manager knows about"""
        return self._switches.keys()

    # This only used in testing
    @property
    def switches(self):
        """Get access to switch objects"""
        return self._switches

    @property
    def fw_names_with_test_overrides(self):
        """Get names of all running alveo firmware allowing for some names
        to be overridden to specify test firmware"""
        names = {}
        for switch in self._switches.values():
            for alveo_id, alveo in switch.alveos.items():
                if alveo.fw_name_override:
                    # if override exists return its name
                    names[alveo_id] = alveo.fw_name_override
                else:
                    # otherwise just return normal firmware name
                    names[alveo_id] = alveo.fw_image
        return names

    @fw_names_with_test_overrides.setter
    def fw_names_with_test_overrides(self, override_dict: dict):
        """
        Specify alveo firmware names to be overridden for tests

        :param override_dict: key: Alveo_ID, value: firmware_name
        """
        for switch in self._switches.values():
            for alveo_id, alveo in switch.alveos.items():
                if alveo_id in override_dict:  # Ignore unknown alveo names
                    alveo.fw_name_override = override_dict[alveo_id]

    # This only provides access for testing
    def get_alveo_ids(self, sw_id):
        """Get list of Alveos that attached to a switch"""
        if sw_id not in self._switches:
            return []
        return self._switches[sw_id].alveo_ids()

    def get_p4s_as_table(self):
        """Get 'P4_table' by aggregating lines for each P4"""
        table = {}
        # TODO: remove this old method
        return table

    def get_alveo_list(self) -> list:
        """Get list of alveo serial numbers and other status"""
        alveo_stats = []
        for sw in self._switches.values():
            for alveo in sw.alveos.values():
                a_info = {}
                a_info["serial"] = alveo.serial_no
                a_info["hardware"] = alveo.hardware
                a_info["tango_dev"] = alveo.tango_dev
                if alveo.has_firmware:
                    a_info["firmware"] = alveo.fw_image
                    a_info["usage"] = alveo.get_usage()
                alveo_stats.append(a_info)
        return alveo_stats

    def get_station_beams(self) -> list:
        """
        Get description of all Station Beams expected from SPS
        One list entry per subarray generating Station Beams
        """
        stn_beams = []
        for subarray_id, descr in self._subarrays.items():
            if len(descr) == 0:
                continue
            subarray_entry = {}
            subarray_entry["subarray_id"] = subarray_id
            # list of [[stn,substn], ] for subarray
            subarray_entry["stns"] = descr["stns"]
            # list of beam_id, freqs, boresight_poly
            subarray_entry["stn_beams"] = descr["stn_beams"]
            stn_beams.append(subarray_entry)
        return stn_beams

    def get_pst_beams(self) -> list:
        """
        Get description of all PST beams being calculated
        One list entry per subarray forming PST beams
        """
        pst_beams = []
        for subarray_id, descr in self._subarrays.items():
            if len(descr) == 0:
                continue
            if len(descr["timing_beams"]) == 0:
                continue
            subarray_entry = {}
            subarray_entry["subarray_id"] = subarray_id
            subarray_entry["beams"] = descr["timing_beams"]["beams"]
            pst_beams.append(subarray_entry)
        return pst_beams

    def alveo_registration(self, registration: dict) -> tuple[bool, str]:
        """
        Register an alveo

        :param registration: dict with serial number and hardware type.
        e.g. {"serial": "A123", "hw": "u55c"}
        :returns: success, status message
        """
        serial_no = registration.get("serial")
        hw = registration.get("hw")
        tango_dev = registration.get("tango")
        if serial_no is None or hw is None:
            msg = f"Invalid registration dictionary: {registration}"
            return (False, msg)

        if serial_no not in self._hw_layout.alv_recs:
            msg = f"Alveo registration FAIL: unknown alveo: {serial_no}"
            return (False, msg)

        admin_mode = registration["status"]

        sw_id, sw_port, alv_id = self._hw_layout.alv_recs[serial_no]
        p4_sw = self._switches[sw_id]
        if serial_no in p4_sw.get_alveo_serial_nums():
            # find alveo and update its admin status
            found = False
            for alv in p4_sw.alveos.values():
                if alv.serial_no == serial_no:
                    alv.admin_mode = admin_mode
                    found = True
                    break
            assert found, "Error didn't find alveo for adminMode update"
            msg = f"Alveo re-registration success: {serial_no}"
            return (True, msg)

        # Register the alveo with this switch
        alv = Alveo(
            alv_id, sw_id, sw_port, serial_no, hw, tango_dev, admin_mode
        )
        p4_sw.add_alveo(alv)

        msg = f"Alveo registration success: {serial_no}"
        return (True, msg)

    def _make_pss_reservations(
        self,
        subarray_id,
        search_alveo_list,
        config: SubarrayConfig,
        subarray_dests: SubarrayDests,
    ):
        """
        Reserve pss Alveo capability for subarray timing beam generation

        Same as PST, expected to change later
        """
        alveo_iter = iter(search_alveo_list)

        # Get first alveo to use for correlation
        try:
            alveo = next(alveo_iter)
        except StopIteration:
            return False
        if not alveo.has_firmware:
            alveo.assign_firmware(
                "pss", self._alveo_limits, config.pss_firmware_name2()
            )
        print(f"attempting to use alveo {alveo} for pss")

        # Find Alveo to beamform ALL freqs of ALL StationBeams in subarray
        for stn_bm_id in config.pss_stn_beams():
            timing_beams = config.pss_beams(stn_bm_id)
            freqs = config.beam_freqs(stn_bm_id)

            freq_idx = 0
            while freq_idx < len(freqs):
                freq_id = freqs[freq_idx]
                success = alveo.pss_reserve(
                    config.stations(),
                    subarray_id,
                    stn_bm_id,
                    freq_id,
                    timing_beams,  # TODO single alveo can beamform all ?? pss beams
                )
                if success:
                    # note this switch needs route to pss server
                    for pss_bm_id in timing_beams:
                        for dest in config.get_pss_host_dests(
                            pss_bm_id, freq_id
                        ):
                            subarray_dests.add_dest_to_sw(
                                alveo.switch_id, dest, "pss"
                            )
                    freq_idx += 1
                else:
                    # Get another alveo
                    try:
                        alveo = next(alveo_iter)
                    except StopIteration:
                        txt = (
                            "Not enough Alveos for pss beamforming: subarray="
                        )
                        txt += f"{subarray_id}, station_beam_id="
                        txt += f"{stn_bm_id}, n_coarse={len(freqs)}"
                        print(txt)
                        return False
                    if not alveo.has_firmware:
                        alveo.assign_firmware(
                            "pss",
                            self._alveo_limits,
                            config.pss_firmware_name2(),
                        )
                    print(f"attempting to use alveo {alveo} for pst")
            txt = f"pss subarray_id={subarray_id}"
            txt += f" station_beam_id={stn_bm_id}, n_coarse={len(freqs)}"
            print(txt)
        txt = f"pss beamforming reserved, subarray_id={subarray_id}"
        txt += ", all station_beams"
        print(txt)
        return True

    def _make_pst_reservations(
        self,
        subarray_id,
        timing_alveo_list,
        config: SubarrayConfig,
        subarray_dests: SubarrayDests,
    ):
        """
        Reserve PST Alveo capability for subarray timing beam generation

        """
        alveo_iter = iter(timing_alveo_list)

        # Get first alveo to use for correlation
        try:
            alveo = next(alveo_iter)
        except StopIteration:
            return False
        if not alveo.has_firmware:
            alveo.assign_firmware(
                "pst", self._alveo_limits, config.pst_firmware_name2()
            )
        print(f"attempting to use alveo {alveo} for pst")

        # Find Alveo to beamform ALL freqs of ALL StationBeams in subarray
        for stn_bm_id in config.pst_stn_beams():
            timing_beams = config.pst_beams(stn_bm_id)
            freqs = config.beam_freqs(stn_bm_id)

            freq_idx = 0
            while freq_idx < len(freqs):
                freq_id = freqs[freq_idx]
                success = alveo.pst_reserve(
                    config.stations(),
                    subarray_id,
                    stn_bm_id,
                    freq_id,
                    timing_beams,  # single alveo can beamform all 16 pst beams
                )
                if success:
                    # note this switch needs route to pst server
                    for pst_bm_id in timing_beams:
                        for dest in config.get_pst_host_dests(
                            pst_bm_id, freq_id
                        ):
                            subarray_dests.add_dest_to_sw(
                                alveo.switch_id, dest, "pst"
                            )
                    freq_idx += 1
                else:
                    # Get another alveo
                    try:
                        alveo = next(alveo_iter)
                    except StopIteration:
                        txt = (
                            "Not enough Alveos for PST beamforming: subarray="
                        )
                        txt += f"{subarray_id}, station_beam_id="
                        txt += f"{stn_bm_id}, n_coarse={len(freqs)}"
                        print(txt)
                        return False
                    if not alveo.has_firmware:
                        alveo.assign_firmware(
                            "pst",
                            self._alveo_limits,
                            config.pst_firmware_name2(),
                        )
                        print(f"attempting to use alveo {alveo} for pst")
            txt = f"PST subarray_id={subarray_id}"
            txt += f" station_beam_id={stn_bm_id}, n_coarse={len(freqs)}"
            print(txt)
        txt = f"PST beamforming reserved, subarray_id={subarray_id}"
        txt += ", all station_beams"
        print(txt)
        return True

    def _make_vis_reservations(
        self,
        subarray_id: int,
        vis_alveos: list,
        config: SubarrayConfig,
        subarray_dests: SubarrayDests,
    ) -> bool:
        """
        Reserve space in correlator alveos for subarray station-beams to be
        correlated and produce visibilities. This is attempting to reserve
        space for ALL station beams of the subarray. It will fail if there
        are not enough correlator alveos with spare capacity to correlate.

        :param subarray_id: subarray ID
        :param vis_alveos: (alveo_id, switch_id) tuples for all alveos that
        can be used for correlation
        :param stn_beam_freqs_by id: dict with key=station_beam_ID,
        value= frequency ID list for the station_beam
        :param subarray_stn_ids: (stn, substation) tuple list for subarray
        :param vis_stn_bm_integ: dict, key=stn_bm_id of milliseconds integration
        :returns: bool indicating if all the station-beam could be correlated
        """
        alveo_iter = iter(vis_alveos)

        # Get first alveo to use for correlation
        try:
            alveo = next(alveo_iter)
        except StopIteration:
            return False
        if not alveo.has_firmware:
            alveo.assign_firmware(
                "vis", self._alveo_limits, config.vis_firmware_name2()
            )
        print(f"attempting to use alveo {alveo}")

        # Find Alveo to correlate ALL freqs of ALL StationBeams in subarray
        for stn_bm_id in config.vis_stn_beams():
            freqs = config.beam_freqs(stn_bm_id)
            freq_idx = 0
            while freq_idx < len(freqs):
                freq_id = freqs[freq_idx]
                success = alveo.vis_reserve(
                    config.stations(),
                    subarray_id,
                    stn_bm_id,
                    freq_id,
                    # long or short integration split at 600msec
                    config.vis_integration_ms(stn_bm_id) > 600,
                )
                if success:
                    for dest in config.get_vis_host_dests(stn_bm_id, freq_id):
                        subarray_dests.add_dest_to_sw(
                            alveo.switch_id, dest, "vis"
                        )
                    freq_idx += 1
                else:
                    # Get another alveo
                    try:
                        alveo = next(alveo_iter)
                    except StopIteration:
                        txt = "Not enough Alveos for correlation: subarray="
                        txt += f"{subarray_id}, station_beam_id="
                        txt += f"{stn_bm_id}, n_coarse={len(freqs)}"
                        return False
                    if not alveo.has_firmware:
                        alveo.assign_firmware(
                            "vis",
                            self._alveo_limits,
                            config.vis_firmware_name2(),
                        )
            txt = f"Fitted correlation: subarray_id={subarray_id}"
            txt += f" station_beam_id={stn_bm_id} (n_coarse={len(freqs)}, "
            txt += f"n_stations={len(config.stations())})"
            print(txt)
        txt = f"Correlation subarray_id={subarray_id}"
        txt += ", all station_beam correlations fitted"
        print(txt)
        return True

    def set_alveo_limits(self, lim_dict) -> None:
        """
        Configure override limits on alveo capabilities

        :param lim_dict: a dictionary, which may be empty, containing
        any applicable limit dictionaries for each alveo personality

        Limits are applied to any alveo when it starts to be used
        ie don't retrospectively affect any alveos that are already in use.
        """
        # set limits to the default maximum  values
        self._alveo_limits = copy.deepcopy(ALVEO_LIMIT_DEFAULTS)
        # copy any override values out of passed argument, replacing default
        for fpga_name in self._alveo_limits:
            if fpga_name not in lim_dict:
                # ignore unknown fpga personalities
                continue
            for limit_name, limit_val in lim_dict[fpga_name].items():
                if limit_name not in self._alveo_limits[fpga_name]:
                    # ignore any unknown limit name
                    continue
                self._alveo_limits[fpga_name][limit_name] = limit_val

    def get_alveo_limits(self):
        """Get current limit override"""
        return self._alveo_limits

    def alveo_ids_for_product(self, firmware_name, admin_mode) -> list[int]:
        """Find alveos that could be used to calcuate output"""
        good_alveos = []
        for sw in self._switches.values():
            for alveo in sw.alveos.values():
                if alveo.admin_mode != admin_mode:
                    continue
                if alveo.fw_image is None or alveo.fw_image == firmware_name:
                    good_alveos.append(alveo)
        return good_alveos

    def reserve_alveos(self, product, sub_id, cfg, dests, admin_mode) -> bool:
        """
        Reserve alveos for generating a product for a subarray
        (TODO add beam argument and make this a subarray-beam)
        :return success:
        """
        alveos = self.alveo_ids_for_product(product.firmware_name, admin_mode)

        if product.product_type == "vis":
            ok = self._make_vis_reservations(sub_id, alveos, cfg, dests)
        elif product.product_type == "pst":
            ok = self._make_pst_reservations(sub_id, alveos, cfg, dests)
        elif product.product_type == "pss":
            ok = self._make_pss_reservations(sub_id, alveos, cfg, dests)
        else:
            ok = False
        return ok

    def delete_reservations(self):
        """Unreserve every alveo in case it had been reserved"""
        for switch in self._switches.values():
            for alveo in switch.alveos.values():
                alveo.delete_reservation()

    def accept_reservations(self):
        """Accept reservations (if any) in every alveo"""
        for switch in self._switches.values():
            for alveo in switch.alveos.values():
                alveo.accept_reservation()

    def cfg_scan2(self, cfg_in) -> tuple[bool, str]:
        """Configure for scan by allocating required compute resources"""
        s_cfg = SubarrayConfig(cfg_in)
        subarray_id = s_cfg.subarray_id()
        # First, remove old configuration if we're re-configuring
        if self.has_cfg(subarray_id):
            self.cfg_end2(subarray_id)

        # What output (vis/pst/pss) does this subarray configuration produce?
        products = get_data_products(s_cfg)
        if len(products) == 0:
            msg = "No output specified: no correlation, no PST or PSS beam"
            print(msg)
            return (False, msg)

        # TODO get list of multi-product stn beams
        # TODO allocate multi-product beams first (and prefer same switch)

        # Try to reserve space in alveos for each product (vis/pst/pss)
        admin_state = s_cfg.admin_state()
        dests = SubarrayDests(subarray_id)
        for product in products:
            # TODO add loop on stn_bm_id and skip if done as multi-product
            ok = self.reserve_alveos(
                product, subarray_id, s_cfg, dests, admin_state
            )
            if not ok:
                self.delete_reservations()
                msg = "Not enough alveos"
                return (False, msg)
        self.accept_reservations()

        # Temp debug: show what destination routes are required at each switch
        # for visibilities
        print("DEBUG: vis dests", dests.get_alveo_dests("vis"))
        print("DEBUG: pst dests", dests.get_alveo_dests("pst"))
        print("DEBUG: pss dests", dests.get_alveo_dests("pss"))

        # Temporary to hold subarray info while trying to satisfy ConfigureScan
        sa_info = {}
        # stations is a list of tuples: (stn, sub-stn)
        sa_info["stns"] = s_cfg.stations()
        for stn, _ in sa_info["stns"]:
            if stn < 1 or stn > self._max_stations:
                msg = f"Station number out of range 1-{self._max_stations}"
                print(msg)
                return (False, msg)

        # stn_beams is dict, keys "beam_id", "freq_id"[], "boresight_dly_poly"
        sa_info["stn_beams"] = cfg_in["stations"]["stn_beams"]
        sa_info["scanning"] = False
        sa_info["scan_id"] = 0
        if s_cfg.has_visibilities:
            # VIS info for Tango "internal_subarray" attribute
            sa_info["vis"] = {}
            sa_info["vis"]["stn_beams"] = s_cfg.vis_raw_beam_cfg()
        if s_cfg.has_timing_beams:
            # PST info for Tango "internal_subarray" attribute
            sa_info["timing_beams"] = {}
            sa_info["timing_beams"]["beams"] = s_cfg.pst_raw_beam_cfg()
        if s_cfg.has_search_beams:
            # PST info for Tango "internal_subarray" attribute
            sa_info["search_beams"] = {}
            sa_info["search_beams"]["beams"] = s_cfg.pss_raw_beam_cfg()

        # save info about the subarray if Config succeeded and reached here
        self._subarrays[subarray_id] = sa_info
        self._subarray_traf_dests[subarray_id] = dests

        msg = f"Successful ConfigureScan for subarray {subarray_id}"
        return (True, msg)

    def cfg_end2(self, subarray_id) -> None:
        """Relese Alveo that were configured (acquired) for a scan"""
        for sw in self._switches.values():
            for alveo in sw.alveos.values():
                alveo.clear_subarray(subarray_id)
        # remove no-longer-configured subarrays from records
        if subarray_id not in self._subarrays:
            print(f"deconfigure: subarray {subarray_id} wasn't configured")
            return
        _ = self._subarrays.pop(subarray_id)
        _ = self._subarray_traf_dests.pop(subarray_id)

    def enable_scan(self, subarray_id, should_scan, scan_id):
        """Record if subarray should be scanning"""
        if (not should_scan) and (subarray_id not in self._subarrays):
            print(f"disable scan: subarray {subarray_id} wasn't present")
            return
        assert (
            "scanning" in self._subarrays[subarray_id]
        ), f"impossible: no scanning entry for subarray_{subarray_id}"
        self._subarrays[subarray_id]["scanning"] = should_scan
        self._subarrays[subarray_id]["scan_id"] = scan_id

    def has_cfg(self, subarray_id) -> bool:
        """Determine whether subarray has a configuration"""
        if (
            subarray_id not in self._subarrays
            or self._subarrays[subarray_id] is None
        ):
            return False
        return True

    def get_internal_repr(self) -> dict:
        """
        Get internal state of all in-use Alveos (no entry if no fw)
        :return: dict with:
            keys   = alveo serial number
            values = dict with keys 'fw', 'regs'
        """
        intl_repr = {}
        for sw in self._switches.values():
            for alveo in sw.alveos.values():
                if not alveo.is_in_use():
                    continue
                serial_no = alveo.serial_no
                firmware = alveo.fw_image
                intl_repr[serial_no] = {
                    "fw": firmware,
                    "regs": alveo.get_internal_regs(),
                }
        return intl_repr

    def get_stn_routes(self):
        """Get P4 route table entries for station->alveo traffic"""
        all_routes = {}  # Key will be switch_id
        for switch_id in self._switches:
            all_routes[switch_id] = {}  # key will be route tuple
        # programmed Alveos are attached to P4s in the firmware_table
        for dst_sw_id, sw in self._switches.items():
            # temporary for link traffic balance on this switch
            flow_cntr = {}
            # add destination routes to the alveo's switch
            for alveo in sw.alveos.values():
                alveo_port = alveo.switch_port
                for route_info in alveo.get_sps_routing_info():
                    (sbary, beam, freq, stn, _) = route_info
                    # add route to dest switch (where alveo connects)
                    # no need to check, access ensured by access to alveo
                    if (sbary, beam, freq) not in all_routes[dst_sw_id]:
                        all_routes[dst_sw_id][(sbary, beam, freq)] = set()
                    all_routes[dst_sw_id][(sbary, beam, freq)].add(alveo_port)
                    if stn not in self._hw_layout.io_locn["stn"]:
                        print(
                            f"Warning: May not route from unknown station {stn}"
                        )
                        continue
                    # if dest switch == source switch, no more to do
                    (src_sw_id, _) = self._hw_layout.io_locn["stn"][stn]
                    if src_sw_id == dst_sw_id:
                        continue

                    # add route to source switch (where stn connects)
                    link_ports = self._switches[src_sw_id].port_to(dst_sw_id)
                    if link_ports is None:
                        print(f"No link {src_sw_id} to {dst_sw_id}!")
                        continue

                    # balance traffic if multiple links btw switches by
                    # sending each successive stream down a different link
                    link_idx = 0
                    if len(link_ports) > 1:
                        switch_pair = f"{src_sw_id}{dst_sw_id}"
                        if switch_pair not in flow_cntr:
                            flow_cntr[switch_pair] = 0
                        link_idx = flow_cntr[switch_pair] % len(link_ports)
                        flow_cntr[switch_pair] += 1
                    (chosen_link_port, _) = link_ports[link_idx]

                    # but for now just use first port in the list
                    if (sbary, beam, freq) not in all_routes[src_sw_id]:
                        all_routes[src_sw_id][(sbary, beam, freq)] = set()
                    all_routes[src_sw_id][(sbary, beam, freq)].add(
                        chosen_link_port
                    )
        # Present routes as a list for each P4
        combined_routes = {}
        for p4_id, routing in all_routes.items():
            combined_routes[p4_id] = []
            for route_tuple, port_set in routing.items():
                combined_routes[p4_id].append((route_tuple, list(port_set)))

        return combined_routes

    def get_subarray_repr(self) -> dict:
        """
        Get state of all configured subarrays
        :return: dict with subarray_id as key
        """
        subarray_repr = {}
        for subarray_id, descr in self._subarrays.items():
            if len(descr) == 0:
                continue

            entry = {
                "scanning": descr["scanning"],
                "scan_id": descr["scan_id"],
                "stns": descr["stns"],
                "stn_beams": descr["stn_beams"],
            }
            optional_items = ("vis", "timing_beams", "search_beams")
            for itm in optional_items:
                if itm in descr:
                    entry[itm] = descr[itm]

            subarray_repr[subarray_id] = entry
        return subarray_repr

    def get_pst_repr(self) -> dict:
        """
        Get description of all PST beams
        :return: dict with pst_beam_id as key
        """
        pst_repr = {}
        for subarray_id, descr in self._subarrays.items():
            if len(descr) == 0:
                continue
            if "timing_beams" not in descr:
                continue

            for pst_beam_descr in descr["timing_beams"]["beams"]:
                beam_info = copy.deepcopy(pst_beam_descr)
                pst_beam_id = beam_info["pst_beam_id"]
                beam_info.pop("pst_beam_id")
                beam_info["subarray_id"] = subarray_id
                pst_repr[pst_beam_id] = beam_info

        return pst_repr

    def get_sdp_routes(self, sdp_arp_replies: ArpReplies, logger=None) -> dict:
        """
        Get list of SDP routes for each switch and list of SDP hosts to ARP for
        This info is published to connector via sdp_routes attribute

        :return: dictionary of routes & hosts (interface to Connector)
        """
        # 1. SDP hosts needing ARP request
        rte_dict = {"arp_rq": self._get_sdp_hosts()}  # all hosts in configure
        paths = self._calc_sdp_routes(
            sdp_arp_replies, logger
        )  # src of host traffic
        # print(f"paths={paths}")
        # 2. For each switch: ports connecting to SDP and route info
        for switch_id, switch in self._switches.items():
            rte_lst = []
            if switch_id in paths:
                for port, ip_list in paths[switch_id].items():
                    for ip_addr in ip_list:
                        rte_lst.append([ip_addr, port])
            # TODO make this less fragile:
            # Text 'sdp' must match allocator chart values file at present
            sdp_ports = switch.port_to_named_links("sdp")
            rte_dict[switch_id] = {
                "sdp_ports": sdp_ports,
                "routes": rte_lst,
            }
        return rte_dict

    def _calc_sdp_routes(self, sdp_arp_replies: ArpReplies, logger) -> dict:
        """
        for each SDP destination required at a source switch find a path to
        the destination switch & port that connects to the SDP host

        :return: dictionary keyed by switch ID containing list of route tuples
            for each switch
        """
        routes = {}
        # Note we are recalculating routes for all subarrays in this outer loop
        # There isn't anything here to prevent routes flapping. TODO!
        for rte_cfg in self._subarray_traf_dests.values():
            subarray_dests = rte_cfg.get_alveo_dests("vis")
            for src_sw, iplist in subarray_dests.items():
                for ip_addr in iplist:
                    if logger:
                        logger.info(
                            "calc SDP route from %s to %s", src_sw, ip_addr
                        )
                    dest_loc = sdp_arp_replies.host_info(ip_addr)
                    if dest_loc is None:
                        # no arp reply for ip_addr (yet)
                        if logger:
                            logger.info("no arp reply for %s", ip_addr)
                        continue
                    # Now know switch and port that connect to the SDP host
                    dest_sw = dest_loc["switch"]
                    dest_sw_port = dest_loc["port"]
                    if logger:
                        logger.info(
                            " %s is at %s %s", ip_addr, dest_sw, dest_sw_port
                        )
                    # SDP host is on same switch as alveos connect to (AA0.5-2)
                    if src_sw == dest_sw:
                        # just one switch needs a route
                        if src_sw not in routes:
                            routes[src_sw] = {}
                        if dest_sw_port not in routes[src_sw]:
                            routes[src_sw][dest_sw_port] = set()
                        routes[src_sw][dest_sw_port].add(ip_addr)
                        continue
                    # SDP host on different switch to Alveos (AA3)
                    # Need to find link(s) btw switches and add routes to both
                    cnx_ports = self._hw_layout.intersw_links.get_ports(
                        src_sw, dest_sw
                    )
                    if len(cnx_ports) == 0:
                        if logger:
                            logger.info(
                                "No route for %s from %s to %s",
                                ip_addr,
                                src_sw,
                                dest_sw,
                            )
                        continue
                    src_sw_port = cnx_ports[0]  # TODO: load balancing

                    # route for source switch
                    if src_sw not in routes:
                        routes[src_sw] = {}
                    if src_sw_port not in routes[src_sw]:
                        routes[src_sw][src_sw_port] = set()
                    routes[src_sw][src_sw_port].add(ip_addr)
                    # route for destination switch
                    if dest_sw not in routes:
                        routes[dest_sw] = {}
                    if dest_sw_port not in routes[dest_sw]:
                        routes[dest_sw][dest_sw_port] = set()
                    routes[dest_sw][dest_sw_port].add(ip_addr)

        return routes

    def _get_sdp_hosts(self) -> list:
        """
        get list of SDP hosts that are destinations for visibilities

        Done by scanning all subarray configurations for any host definitions
        :return: list of dotted-ip address strings (all unique)
        """
        hosts = set()
        for sub_info in self._subarrays.values():
            vis_info = sub_info.get("vis")
            if vis_info is None:
                continue
            # Visibility info is for all station beams
            for beam_info in vis_info["stn_beams"]:
                # vibility data for a beam may be split to several SDP servers
                for host in beam_info["host"]:
                    # second element in tuple for host contains the host IP
                    hosts.add(host[1])
        return list(hosts)

    def get_subarray_alveos(self, subarray_id) -> list:
        """
        Get list of alveo card serial nos used by a subarray

        :return: list of alveo serial number strings
        """
        serials = []
        for sw in self._switches.values():
            for alveo in sw.alveos.values():
                sub_ids = alveo.get_subarray_clients()
                if subarray_id in sub_ids:
                    serials.append(alveo.serial_no)
        return serials
