# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
Maintain register of ARP reples from switches
"""
# Ignore the unused arguments in the scan commands for now
# pylint: disable=unused-argument
import json

from schema import Or, Schema  # ,  Use,Optional, Or, And, Forbidden

arp_reply_schema = Schema(
    {
        str: {"mac": str, "port": Or(int, str)},
    }
)


class ArpReplies:
    """
    Maintain list of arp replies
    """

    def __init__(self, logger=None):
        self._replies = {}
        self._logger = logger

    def arp_update(self, sw_id: str, json_update: str) -> None | tuple:
        """
        :param json_update: json string with ARP-reply info
        """
        update = json.loads(json_update)  # errors caught by Tango
        if not arp_reply_schema.is_valid(update):
            if self._logger:
                self._logger.error("Invalid ARP info: %s", update)
            return None

        hosts_added = set()
        hosts_removed = set()
        for host, value in update.items():
            if value["mac"] == "00:00:00:00:00:00":
                if host in self._replies:
                    del self._replies[host]
                    hosts_removed.add(host)
                continue
            value["switch"] = sw_id
            self._replies[host] = value
            hosts_added.add(host)
        return list(hosts_removed), list(hosts_added)

    def num_hosts(self):
        """
        Get number of hosts that have sent ARP replies
        :return: number of hosts
        """
        return len(self._replies)

    def host_info(self, host_ip_str: str) -> None | dict:
        """
        Get
        :return: None or
            dict {"mac": mac_string, "port": sw_port_string, "switch":sw_id}
        """
        return self._replies.get(host_ip_str)
