# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2025 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=invalid-name,protected-access,too-few-public-methods

"""
'alveo_est_tool' function from this module calculates the number of alveos
that will be required for the subarray confguration given as function argument
"""

import argparse
import copy
import json

from ska_low_cbf.allocator.resources import ALVEO_LIMIT_DEFAULTS, Resources


def make_cnx_list(n_stns: int, n_alveo: int) -> list[str]:
    """
    Create a list of connections for the resource manager initialisation

    """
    cnx = []
    sw_port = 1

    # add alveos into connections
    for alv in range(0, n_alveo):
        itm = {
            "switch": "p4_01",
            "port": f"{sw_port}/0",
            "speed": 100,
            "alveo": f"alv_{alv+1}",
        }
        cnx.append(itm)
        sw_port += 1

    # add stations into connections
    for stn in range(0, n_stns):
        itm = {
            "switch": "p4_01",
            "port": f"{sw_port}/0",
            "speed": 100,
            "link": f"stn_{stn:03d}",
        }
        cnx.append(itm)
        sw_port += 1

    # add a SDP link
    itm = {
        "switch": "p4_01",
        "port": f"{sw_port}/0",
        "speed": 100,
        "link": "sdp_001",
    }
    cnx.append(itm)
    sw_port += 1

    # add a PST link
    itm = {
        "switch": "p4_01",
        "port": f"{sw_port}/0",
        "speed": 100,
        "link": "pst_001",
    }
    cnx.append(itm)
    sw_port += 1

    # add a PSS link
    itm = {
        "switch": "p4_01",
        "port": f"{sw_port}/0",
        "speed": 100,
        "link": "pss_001",
    }
    cnx.append(itm)
    sw_port += 1

    return cnx


def alveo_est_tool(cfg, alveo_limits={}) -> int:
    """
    Tool to estimate number of alveo needed for subarray configuration

    :param cfg: subarray configuration dictionary
    :param alveo_limits: optional dictionary of alveo limitations (if any)
    :return: number of alveo needed, or -1 for bad configuration
    """
    array_cfg = {"stations": 512, "channels": 324}  # Low parameters
    n_alveo = 500  # maximum Low.CBF hardware alveo cards
    cnx_data = make_cnx_list(array_cfg["stations"], n_alveo)
    # Create a resource manager object (same as allocator)
    res = Resources(array_cfg, cnx_data)

    # Register all the alveos for use by the resource manager
    for itm in cnx_data:
        alv_serial = itm.get("alveo")
        if alv_serial is None:
            continue
        signup = {
            "serial": alv_serial,
            "hw": "u55c",
            "tango": "n/a",
            "status": 1,  # offline (any value works for this tool)
        }
        res.alveo_registration(signup)

    # Update alveo limits used by resource manager
    if len(alveo_limits) == 0:
        alveo_limits = copy.deepcopy(ALVEO_LIMIT_DEFAULTS)
    res.set_alveo_limits(alveo_limits)

    # Configure the subarray
    sub_id = 1
    cfg["subarray_id"] = sub_id
    ok, msg = res.cfg_scan2(cfg)

    if not ok:
        # bad configuration request
        print(f"\n\tERROR: {msg}")
        return -1

    # Get list of alveos used (or partly used) by the subarray configuration
    alveo_states = res.get_subarray_alveos(sub_id)
    return len(alveo_states)


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        prog="Alveo Estimation Tool",
        description="Estimate Alveo used bu a subarray configuration",
        epilog="",
    )
    parser.add_argument(
        "-a",
        "--alveo_limits",
        type=str,
        default="{}",
        help="""limit dict JSON e.g. '{"pst": {"vch": 900, "sps_ch": 128}}'""",
    )
    parser.add_argument(
        "-c", "--cfg", type=str, default="{}", help="JSON Subarray config dict"
    )
    return parser.parse_args()


if __name__ == "__main__":
    """
    Script to allow tool to be run from command line if required
    """
    args = parse_args()
    cfg = json.loads(args.cfg)
    alveo_limits = json.loads(args.alveo_limits)

    n_alv = alveo_est_tool(cfg, alveo_limits)

    print(f"{n_alv} alveos required for subarray configuration")
