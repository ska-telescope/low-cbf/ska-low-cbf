# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023, CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=invalid-name,protected-access,too-few-public-methods

"""
This class records the host destinations for traffic that the alveos
connected to each switch will generate. One of these per subarray

"""


class SubarrayDests:
    """
    What routing destinations a subarray has on each switch
    """

    def __init__(self, subarray_id):
        self._id = subarray_id
        # route destinations from alveos at each switch
        self._sw_outbound_dests = {}
        # routes at each switch that reach the destinations
        self._sw_outbound_routes = {}

    def remove_dest_type(self, host_type: str):
        """Wipe destination host_type from class instance data"""
        for sw, set_of_tuples in self._sw_outbound_dests.items():
            dests_remaining = {d for d in set_of_tuples if d[1] != host_type}
            self._sw_outbound_dests[sw] = dests_remaining

        for sw, set_of_tuples in self._sw_outbound_routes.items():
            routes_remaining = {r for r in set_of_tuples if r[1] != host_type}
            self._sw_outbound_routes[sw] = routes_remaining

    def add_dest_to_sw(self, sw_id: str, host_ip: str, host_type: str) -> None:
        """
        Add the IP address of a host IP that needs a route

        :param host_ip: dotted IP address string
        :param host_type: eg "sdp" or "pst"
        :param sw_id: switch ID at which an alveo is sending traffic to host
        """
        if sw_id not in self._sw_outbound_dests:
            self._sw_outbound_dests[sw_id] = set()
        self._sw_outbound_dests[sw_id].add((host_ip, host_type))

    def _get_dests_at_sw(self, sw_id, host_type) -> list:
        """
        Get list of particular type of SDP hosts that Alveos
           connected to a switch need a route to
        """
        if sw_id not in self._sw_outbound_dests:
            return []
        return [
            d[0] for d in self._sw_outbound_dests[sw_id] if d[1] == host_type
        ]

    def get_alveo_dests(self, host_type: str) -> dict[str, list]:
        """Get list of all host destinations of a particular type that Alveos
         connected to each switch need a route to

        :param host_type: string, eg "sdp", "pst"
        :return: dict [sw_id] = [host_ip, ]
        """
        return {
            sw_id: self._get_dests_at_sw(sw_id, host_type)
            for sw_id in self._sw_outbound_dests
        }
