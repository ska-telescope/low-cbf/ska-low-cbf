"""
This file contains Classes and methods representing the properties of
Correlator FPGAs for the purpose of assigning visibility computation resources
to subarrays
"""

import copy
import math
from dataclasses import dataclass


@dataclass(frozen=False)
class VisFreqBlk:
    """
    Representing a block of contiguous frequency channels in a station_beam
    """

    first_coarse: int  # sky_freq = first_coarse * 781.25 KHz
    num_coarse: int  # first & last coarse may only be partially used
    first_fine: int  # first_fine within first coarse channel, as per FPGA
    num_fine: int  # total number of fine chans = 3456*coarse (standard vis)
    integ_times: int  # 192->849msec, 64->283msec
    integ_chans: int  # 24 fine channels integrated (standard visibilities)
    mxc_num: int  # which matrix correlator to use (or MXC_UNDECIDED)


FINE_PER_COARSE = 3456
LONG_INTEG_SAMPLE = 192
SHORT_INTEG_SAMPLE = 64

# FPGA constants
CT2_BYTES_PER_STN_CHNL = (3456 * 6 * 512) // 4  # Mem for Filterbank output
CT2_BYTES_PER_STN_FINE_CHAN = (6 * 512) // 4
VCHAN_MAX_IN_FPGA = 1024
NUM_MX_CORRELATORS = 2
CT2_MX_MEM_BYTES = 3 * 512 * 1024 * 1024
MAX_MXC_BASELINES = 1 * round(512 * 513 / 2)  # 512 stns = 131328 baselines
MAX_SBT_ENTRIES = 128  # slots in subarray-beam table for each MxC

MXC_UNDECIDED = NUM_MX_CORRELATORS  # marker: MxC choice not yet made


def std_vis_long_integ_one_coarse(freq_id: int) -> VisFreqBlk:
    """
    :return: Object to model an entry in the correlator 'subarray_beams' table
    requesting a standard 890ms correlation of one coarse channel
    """
    return VisFreqBlk(
        freq_id, 1, 0, FINE_PER_COARSE, LONG_INTEG_SAMPLE, 24, MXC_UNDECIDED
    )


def std_vis_short_integ_one_coarse(frq_id: int) -> VisFreqBlk:
    """
    :return: Object to model an entry in the correlator 'subarray_beams' table
    requesting a standard (short) 300ms correlation of one coarse channel
    """
    return VisFreqBlk(
        frq_id, 1, 0, FINE_PER_COARSE, SHORT_INTEG_SAMPLE, 24, MXC_UNDECIDED
    )


class FirmwareVis:
    """Class handles correlator-specific firmware properties"""

    def __init__(self, limits):
        """
        Create Structure that Correlator uses to store the data structure
        describing its internal register settings
        """
        self._state = {}
        # self._state[subarray_id]["stn_beams"][stn_bm_id] contains a list of
        # VisFreqBlk objects each representing a subarray-beam table entry
        # in FPGA registers
        self._rsvd_state = None
        self._vchan_max_in_fpga = limits["vis"]["vch"]  # VCHAN_MAX_IN_FPGA
        self._ct2_max_mem_bytes = (
            limits["vis"]["hbm"] * CT2_BYTES_PER_STN_CHNL
        )  # CT2_MX_MEM_BYTES
        self._max_mxc_baselines = limits["vis"]["bli"]  # MAX_MXC_BASELINES

    def chan_capacity(self, n_stns) -> int:
        """Get num contiguous channels that FPGA could accept for each MxC"""
        # Calculate existing usage contribution toward each limit
        vct_entries = [0] * NUM_MX_CORRELATORS
        baselines = [0] * NUM_MX_CORRELATORS
        sb_entries = [0] * NUM_MX_CORRELATORS
        for sub_desc in self._state.values():  # for each subarray
            vct_stns = math.ceil(len(sub_desc["stations"]) / 4) * 4
            for sbt_blk_list in sub_desc["stn_beams"].values():  # for stn bm
                for blk in sbt_blk_list:  # for blk of contiguous chans
                    mxc_num = blk.mxc_num
                    # how many VCT entries used (and CT2 HBM slots)
                    vct_entries[mxc_num] += blk.num_coarse * vct_stns
                    # workload
                    baselines[mxc_num] += blk.num_coarse * (
                        (vct_stns * (vct_stns + 1)) // 2
                    )
                    sb_entries[mxc_num] += 1

        # what remains between each limit and current loading?
        bli_remain = [self._max_mxc_baselines - n_bli for n_bli in baselines]

        vct_remain = self._vchan_max_in_fpga - sum(vct_entries)
        vct_entries_rqd_per_ch = math.ceil(n_stns / 4) * 4
        vct_chans = vct_remain // vct_entries_rqd_per_ch

        hbm_max_slots = self._ct2_max_mem_bytes // CT2_BYTES_PER_STN_CHNL
        memsl_remain = [hbm_max_slots - n_vct for n_vct in vct_entries]

        sb_remain = [MAX_SBT_ENTRIES - n_sbt for n_sbt in sb_entries]

        # how many chans worth remain for each limit?
        vct_stns = math.ceil(n_stns / 4) * 4
        bli_chans = [
            bli // ((vct_stns * (vct_stns + 1)) // 2) for bli in bli_remain
        ]
        mem_chans = [msl // vct_entries_rqd_per_ch for msl in memsl_remain]

        # number of available chans is the minimum across all of the limits
        chans_free = [0] * NUM_MX_CORRELATORS
        for idx in range(0, len(bli_chans)):
            if sb_remain[idx] <= 0:  # no subarray-beam table entries
                continue
            n_ch = min(bli_chans[idx], mem_chans[idx])
            if n_ch <= 0:
                continue
            chans_free[idx] = n_ch
        # Useful for debugging if this is updated and goes wrong:
        # print(f"bli_chans={bli_chans}")
        # print(f"mem_chans={mem_chans}")
        # print(f"chans_free={chans_free}")
        # print(f"vct_chans={vct_chans}")
        return min(sum(chans_free), vct_chans)

    def vis_reserve(
        self,
        subarray_stn_ids,
        subarray_id: int,
        stn_bm_id: int,
        freq_id: int,
        is_long_integ: bool,
    ) -> bool:
        """
        Reserve space in this FPGA for correlation of one
        coarse frequency channel of one station-beam of a subarray.

        :param subarray_stn_ids: (stn,substn) tuple list for the station beam
        :param subarray_id: subarray ID
        :param stn_bm_id: station beam ID
        :param freq_id: frequency ID (freq_id * 781.25kHz = sky_frequency)
        :param is_long_integ: True for 849ms integration false for 283ms
        :returns: success or failure of the reservation.

        This method may be called repeatedly to add additional frequency
        channels to the correlator. First time it is called a reservation
        will be created, subsequent times the reserved space will be extended
        not re-created or overwritten. In the case of failure (due to FPGA
        full) the failed component of the reservation is abandoned
        """

        if self._rsvd_state is not None:
            # If there is an existing reservation state, attempt to extend
            tmp_state = copy.deepcopy(self._rsvd_state)
        else:
            # Otherwise start from current state
            tmp_state = copy.deepcopy(self._state)

        if subarray_id not in tmp_state:
            # No subarray - Create, add 1st station_beam & 1st coarse channel
            tmp_state[subarray_id] = {}
            # subarray = tmp_state[subarray_id]
            tmp_state[subarray_id]["stations"] = subarray_stn_ids
            tmp_state[subarray_id]["stn_beams"] = {}
            if is_long_integ:
                tmp_state[subarray_id]["stn_beams"][stn_bm_id] = [
                    std_vis_long_integ_one_coarse(freq_id)
                ]
            else:
                tmp_state[subarray_id]["stn_beams"][stn_bm_id] = [
                    std_vis_short_integ_one_coarse(freq_id)
                ]
            does_fit, mxc = self._check_fit_choose_mxc(tmp_state)
            if not does_fit:
                return False
            tmp_state[subarray_id]["stn_beams"][stn_bm_id][0].mxc_num = mxc
            self._rsvd_state = tmp_state
            return True
        else:
            # get subarray that already exists
            subarray = tmp_state[subarray_id]
            # check for program error - subarray stations changed unexpectedly
            for stn in subarray_stn_ids:
                assert (
                    stn in subarray["stations"]
                ), "Error: subarray stations changed"

            if stn_bm_id not in subarray["stn_beams"]:
                # Subarray entry exists but no such station beam:
                # create station beam, add its first channel to subarray
                if is_long_integ:
                    subarray["stn_beams"][stn_bm_id] = [
                        std_vis_long_integ_one_coarse(freq_id)
                    ]
                else:
                    subarray["stn_beams"][stn_bm_id] = [
                        std_vis_short_integ_one_coarse(freq_id)
                    ]
                does_fit, mxc = self._check_fit_choose_mxc(tmp_state)
                if not does_fit:
                    return False
                subarray["stn_beams"][stn_bm_id][0].mxc_num = mxc
                self._rsvd_state = tmp_state
                return True
            else:
                stn_bm_frq_blks = subarray["stn_beams"][stn_bm_id]
                # Add new frequency to existing station-beam (minimises use
                # of the station_channels table in firmware)

                # Note: this won't merge two existing blocks, as would be
                # created if frequencies were given in arbitrary order, but
                # we sort frequencies in configurescan, so no problem

                for frq_blk in stn_bm_frq_blks:
                    f_low = frq_blk.first_coarse
                    f_hi = frq_blk.first_coarse + frq_blk.num_coarse - 1
                    assert (freq_id < f_low) or (
                        freq_id > f_hi
                    ), f"DUPE: freq_id={freq_id} f_low={f_low}, f_hi={f_hi}"

                    # Can we append the channel to end of current block?
                    # NOTE MxC not changed - correlation not moved from MxC
                    if freq_id == (f_hi + 1):
                        frq_blk.num_coarse += 1
                        frq_blk.num_fine += FINE_PER_COARSE
                        does_fit = self._check_fit_in_mxc(
                            tmp_state, frq_blk.mxc_num
                        )
                        if not does_fit:
                            frq_blk.num_coarse -= 1
                            frq_blk.num_fine -= FINE_PER_COARSE
                            break
                        self._rsvd_state = tmp_state
                        return True

                    # Can we prepend the channel to start of current block?
                    # TODO REMOVE?: chans always arrive here in ascendig order
                    if freq_id == (f_low - 1):
                        frq_blk.first_coarse = freq_id
                        frq_blk.num_coarse += 1
                        frq_blk.num_fine += FINE_PER_COARSE
                        frq_blk.first_fine = 0
                        does_fit = self._check_fit_in_mxc(
                            tmp_state, frq_blk.mxc_num
                        )
                        if not does_fit:
                            frq_blk.num_coarse -= 1
                            frq_blk.num_fine -= FINE_PER_COARSE
                            break
                        self._rsvd_state = tmp_state
                        return True

                # failed to append/prepend: have to create separate block
                if is_long_integ:
                    new_ch = std_vis_long_integ_one_coarse(freq_id)
                else:
                    new_ch = std_vis_short_integ_one_coarse(freq_id)
                subarray["stn_beams"][stn_bm_id].append(new_ch)

                does_fit, mxc = self._check_fit_choose_mxc(tmp_state)
                if not does_fit:
                    return False
                subarray["stn_beams"][stn_bm_id][-1].mxc_num = mxc
                self._rsvd_state = tmp_state
                return True

    def delete_reservation(self) -> None:
        """Remove a subarray reservation from correlator"""
        self._rsvd_state = None

    def has_reservation(self) -> bool:
        """Whether there is a reservation in this FPGA"""
        return self._rsvd_state is not None

    def accept_reservation(self) -> bool:
        """
        Accept any current reservation of space for more subarray-beams
        to be correlated that exists within this FPGA

        (Note this may be called for FPGAs that don't have any reservation)
        """
        accepted = False
        if self._rsvd_state is not None:
            # make the reserved state become the actual state
            del self._state
            self._state = self._rsvd_state
            self._rsvd_state = None
            accepted = True
        return accepted

    def _cor_vchans_in_use(self, state) -> int:
        """
        Calculate how many virtual channels are in use in this FPGA
        :return: Number of channels used (includes padding for blocks of 4)
        """
        vchans: int = 0
        for subarray in state.values():
            n_stns = math.ceil(len(subarray["stations"]) / 4) * 4
            for stn_bm in subarray["stn_beams"].values():
                for vis_freq_blk in stn_bm:
                    vchans += vis_freq_blk.num_coarse * n_stns
        return vchans

    def _corr_workload(self, state) -> tuple:
        """
        Calculate how much correlation work (=baselines) each matrix correlator
        is committed to calculating, & how much yet to be assigned to a MxC

        :return: Tuple containing two items:
            1. array of baselines currently allocated to each corelator
            2. baselines that are not currently allocated to either correlator
        """
        existing_mxc_bline = [0 for i in range(0, NUM_MX_CORRELATORS)]
        unallocated_bline = 0
        for subarray in state.values():
            n_stns = math.ceil(len(subarray["stations"]) / 4) * 4
            for stn_bm in subarray["stn_beams"].values():
                for vis_freq_blk in stn_bm:
                    blines = vis_freq_blk.num_coarse * round(
                        n_stns * (n_stns + 1) / 2
                    )
                    if vis_freq_blk.mxc_num < NUM_MX_CORRELATORS:
                        existing_mxc_bline[vis_freq_blk.mxc_num] += blines
                    else:  # mxc_num == MXC_UNDECIDED
                        unallocated_bline += blines
        return (existing_mxc_bline, unallocated_bline)

    def _corr_memload(self, state) -> tuple:
        """
        Calculate how much correlation HBM each matrix correlator is
        committed to using, and how much can be given to either

        :return: Tuple containing two items:
            1. Amount of CT2 memory currently in use by each correlator
            2. Amount of CT2 mem that would be required for the new config
        """
        allocated_mem = [0 for i in range(0, NUM_MX_CORRELATORS)]
        unallocated_mem = 0
        for subarray in state.values():
            n_stns = math.ceil(len(subarray["stations"]) / 4) * 4
            for stn_bm in subarray["stn_beams"].values():
                for vfb in stn_bm:
                    mem = vfb.num_coarse * n_stns
                    if vfb.mxc_num < NUM_MX_CORRELATORS:
                        allocated_mem[vfb.mxc_num] += mem
                    else:
                        unallocated_mem += mem
        return (allocated_mem, unallocated_mem)

    def _check_fit_in_mxc(self, state, mxc_assigned) -> bool:
        """
        Check if workload (baselines) and memory usage fit

        :return: Boolean indicating whether the proposed extra
            correlation work and memory usage would fit within
            the capabilities of the correlator selected for the task
        """
        # will the new allocation fit in the virtual channels?
        vchans = self._cor_vchans_in_use(state)
        if vchans > self._vchan_max_in_fpga:
            return False

        # will the new allocation fit within number of baselines per MxC?
        current_mxc_bline, new_bline = self._corr_workload(state)
        # Try adding the baselines to both MxC to see if it will fit
        proposed_bline = [b + new_bline for b in current_mxc_bline]
        bline_ok = [b <= self._max_mxc_baselines for b in proposed_bline]

        # will the new allocation fit within the CT2 Mem capacity per MxC?
        current_mxc_mem, new_mem = self._corr_memload(state)
        # Try adding the needed mem to both MxC to see if it will fit
        proposed_mem = [m + new_mem for m in current_mxc_mem]
        mem_ok = [
            m * CT2_BYTES_PER_STN_CHNL <= self._ct2_max_mem_bytes
            for m in proposed_mem
        ]

        all_ok = [
            bline_ok[i] and mem_ok[i] for i in range(0, NUM_MX_CORRELATORS)
        ]
        return all_ok[mxc_assigned]

    def _check_fit_choose_mxc(self, state) -> tuple:
        """
        Check if workload (baselines) and memory usage have space for 1 more ch

        :return: Tuple containing two items:
            1. Boolean indicating if the new work and memory will fit in
               any of the matrix correlators
            2. Index into matrix correlators array indicating which MxC
               is best able to handle the extra baselines/memory
        """
        # will the new allocation fit in the virtual channels?
        vchans = self._cor_vchans_in_use(state)
        if vchans > self._vchan_max_in_fpga:
            return (False, 0)

        # will the new allocation fit within number of baselines per MxC?
        current_mxc_bline, new_bline = self._corr_workload(state)
        possible_bline = [b + new_bline for b in current_mxc_bline]
        bline_ok = [b <= self._max_mxc_baselines for b in possible_bline]
        # will the new allocation fit within the CT2 Mem capacity per MxC?
        current_mxc_mem, new_mem = self._corr_memload(state)
        proposed_mem = [m + new_mem for m in current_mxc_mem]
        mem_ok = [
            m * CT2_BYTES_PER_STN_CHNL <= self._ct2_max_mem_bytes
            for m in proposed_mem
        ]
        # is the number of slots used in subarray-beam table still ok?
        sb_vals = [0] * (NUM_MX_CORRELATORS + 1)  # 1 extra for undecided case
        for sub_desc in state.values():
            for sbt_blk_list in sub_desc["stn_beams"].values():
                for blk in sbt_blk_list:
                    mxc_num = blk.mxc_num
                    sb_vals[mxc_num] += 1
        sbt_ok = [sbe <= (MAX_SBT_ENTRIES - sb_vals[-1]) for sbe in sb_vals]

        # Which matrix correlator can accept this request
        all_ok = [
            bline_ok[i] and mem_ok[i] and sbt_ok[i]
            for i in range(0, NUM_MX_CORRELATORS)
        ]

        # get first matrix correlator that everything fits in
        # NOTE other criteria possible if it fits in multiple MxC
        #      -e.g. lowest mem, lowest work
        for i in range(0, NUM_MX_CORRELATORS):
            if all_ok[i]:
                return (True, i)
        # didn't fit in any Matrix correlator
        return (False, 0)

    def get_internal_regs(self) -> list:
        """
        Get info that Processor devices require to configure their internal
        registers

        :return: list, one entry per signal processing instance in the FPGA
        containing parameters sig proc instances need to calculate their regs
        """
        all_subarrays_info = []
        for subarray_id, subarray_detail in self._state.items():
            sa_beam_tbl = []
            sa_info = {
                "sa_id": subarray_id,
                "stns": subarray_detail["stations"],  # Move to subarray?
                "sa_bm": sa_beam_tbl,
            }
            for stn_beam, stn_bm_frq_blks in subarray_detail[
                "stn_beams"
            ].items():
                for frq_blk in stn_bm_frq_blks:
                    subarray_beam_entry = (
                        stn_beam,
                        frq_blk.first_coarse,
                        frq_blk.first_fine,
                        frq_blk.num_fine,
                        frq_blk.integ_times,
                        frq_blk.integ_chans,
                        frq_blk.mxc_num,
                    )
                    sa_beam_tbl.append(subarray_beam_entry)
            all_subarrays_info.append(sa_info)

        return all_subarrays_info

    def get_sps_routing_info(self) -> list:
        """
        Get entries that P4 switch should use for routing SPS packets
        to the appropriate Alveo destination. (To be published via
        "p4_stn_routes" attribute)

        :return: list of (subarray, beam, freq, stn, sub-stn) tuples
        for all SPS packets that should be routed to this alveo
        """
        route_info = []
        for subarray_id, subarray_detail in self._state.items():
            for stn_beam, stn_bm_frq_blks in subarray_detail[
                "stn_beams"
            ].items():
                for frq_blk in stn_bm_frq_blks:
                    for frq in range(
                        frq_blk.first_coarse,
                        frq_blk.first_coarse + frq_blk.num_coarse,
                    ):
                        for stn, substn in subarray_detail["stations"]:
                            entry = (subarray_id, stn_beam, frq, stn, substn)
                            route_info.append(entry)
        return route_info

    def clear_subarray(self, sbarry) -> bool:
        """
        Stop correlating station-beams belonging to subarray
        """
        if sbarry in self._state:
            self._state.pop(sbarry)
        return self.vchans_in_use() > 0

    def vchans_in_use(self) -> int:
        """
        Calculate how many virtual channels are in use or proposed for use
        :return: Number of channels used (includes padding for blocks of 4)
        """
        state = self._state
        # reserved state is more current if not None
        if self._rsvd_state is not None:
            state = self._rsvd_state
        vchans: int = 0
        for subarray in state.values():
            n_stns = math.ceil(len(subarray["stations"]) / 4) * 4
            for stn_bm in subarray["stn_beams"].values():
                for vis_freq_blk in stn_bm:
                    vchans += vis_freq_blk.num_coarse * n_stns
        return vchans

    def subarray_clients(self):
        """Get list of subarrays using this alveo"""
        if len(self._state) is None:
            return []
        cli = list(self._state.keys())
        return cli

    def get_usage(self):
        """Usage by subarray ID"""
        use_list = []
        tot_stn_ch = 0  # virtual channels (station-channels) used
        tot_sbt = 0  # entries in subarray beam table
        tot_ch = 0  # channel memory entries in CT2 HBM
        tot_bli = 0  # baselines computed in MxC
        for sub_id, data in self._state.items():
            real_stns = len(data["stations"])
            n_stns = math.ceil(real_stns / 4) * 4
            n_sbt = 0
            n_ch = 0
            n_bli = 0
            for sbt_itms in data["stn_beams"].values():
                for sbt_entry in sbt_itms:
                    n_sbt += 1
                    n_ch += sbt_entry.num_coarse
                    n_bli += (
                        (n_stns * (n_stns + 1)) * sbt_entry.num_coarse // 2
                    )
            sbt_entry = {
                "sub_id": sub_id,
                "stn_chan": n_ch * n_stns,
                "sbt": n_sbt,
                "mem": n_ch * n_stns,  # will differ for zoom
                "bli": n_bli,
            }
            tot_stn_ch += n_ch * n_stns
            tot_sbt += n_sbt
            tot_ch += n_ch
            tot_bli += n_bli
            use_list.append(sbt_entry)
        use_list.append(
            {
                "sub_id": "*",
                "stn_chan": self._vchan_max_in_fpga - tot_stn_ch,
                "sbt": MAX_SBT_ENTRIES * NUM_MX_CORRELATORS - tot_sbt,
                "mem": (CT2_MX_MEM_BYTES // CT2_BYTES_PER_STN_CHNL)
                * NUM_MX_CORRELATORS
                - tot_ch,
                "bli": MAX_MXC_BASELINES * NUM_MX_CORRELATORS - tot_bli,
            }
        )
        return use_list
