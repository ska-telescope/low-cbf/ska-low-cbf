"""
Classes and methods in this file represent the properties of PST FPGAs
for the purpose of assigning PST beamforming resources to subarrays
"""

import copy
import math
from dataclasses import dataclass


@dataclass
class PstState:
    """Class describing the usage of a PSS signal processing pipeline"""

    subarray_id: int  # which subarray is using the pipeline
    stn_bm_id: int  # which station-beam is using the pipeline
    stns: list  # (stn, substn) belonging to subarray
    freq_ids: list  # SPS channels processed in pipeline
    pst_bm_ids: list  # PST beams being formed from subarray-beam


class FirmwarePst:
    """Class handles pst-specific firmware properties"""

    # PST FPGA constants
    PST_SIGPROC_PIPELINES = 3
    PST_PIPELINE_MAX_BEAMS = 16  # Each pipeline calculates up to 16 beams
    MAX_STATIONS_BEAMFORMED = 512
    MAX_IO_MBPS = 100_000
    PST_MBPS_PER_COARSE = 68.68  # beam MBPS / coarse / pst_beam

    def __init__(self, limits):
        self._state = [None] * FirmwarePst.PST_SIGPROC_PIPELINES
        """
        A list, one entry per PST signal processing pipeline in FPGA,
        containing a tuple describing the station_beam-frequencies that
        are to be beamformed, and the PST beams to be created from them:
        (subarry_id, station_beam_id, stns, frequency_ids, pst_beam_ids)
        """
        self._rsvd_state = [None] * FirmwarePst.PST_SIGPROC_PIPELINES
        self._vchan_max_in_fpga = limits["pst"]["vch"]
        self._sps_ch_max_in_fpga = limits["pst"]["sps_ch"]

    def chan_capacity(self, subarray_id, stn_bm_id, n_stns, n_pst_bms) -> int:
        """
        Get number of channels that the FPGA could accept
        """
        if n_stns > FirmwarePst.MAX_STATIONS_BEAMFORMED:
            return 0
        if n_pst_bms > FirmwarePst.PST_PIPELINE_MAX_BEAMS:
            return 0
        chans = 0
        cur_bm_chns = 0  # Existing output beam-channels
        for pipe in self._state:
            if pipe is None:
                # pipe unused - all capacity available to subarray-beam
                pipe_capacity = min(
                    self._sps_ch_max_in_fpga, self._vchan_max_in_fpga // n_stns
                )
                chans += pipe_capacity
                continue
            cur_bm_chns += len(pipe.pst_bm_ids) * len(pipe.freq_ids)
            if pipe.subarray_id != subarray_id or pipe.stn_bm_id != stn_bm_id:
                # pipe used by another subarray-beam - none available
                continue
            # pipe partly used by this subarray-beam - remaining is available
            pipe_capacity = min(
                self._sps_ch_max_in_fpga - len(pipe.freq_ids),
                (self._vchan_max_in_fpga - len(pipe.stns) * len(pipe.freq_ids))
                // n_stns,
            )
            chans += pipe_capacity

        # Limited by output bandwidth?
        rem_bw = self.MAX_IO_MBPS - self.PST_MBPS_PER_COARSE * cur_bm_chns
        bw_ch_lim = math.floor(rem_bw / (self.PST_MBPS_PER_COARSE * n_pst_bms))
        return min(chans, bw_ch_lim)

    def pst_reserve(
        self,
        stn_ids,
        subarray_id: int,
        stn_bm_id: int,
        freq_id: int,
        pst_bm_ids,
    ) -> bool:
        """
        Attempt to reserve PST beamformer capability for a coarse channel of a
        subarray and station-beam

        :return: boolean indicating if reservation in this FGPA succeeded
        """
        # txt = f"sbarry_{subarray_id}, stn_bm_{stn_bm_id}, frq_id{freq_id}"
        # print(txt)
        if len(stn_ids) > FirmwarePst.MAX_STATIONS_BEAMFORMED:
            return False
        if len(pst_bm_ids) > FirmwarePst.PST_PIPELINE_MAX_BEAMS:
            return False

        for i in range(0, FirmwarePst.PST_SIGPROC_PIPELINES):
            if self._rsvd_state[i] is not None:
                most_recent_state = self._rsvd_state[i]
            else:
                most_recent_state = self._state[i]

            if most_recent_state is not None:
                # Can't extend pipe if subarray_id or stn_beam_id are not same
                if (most_recent_state.subarray_id != subarray_id) or (
                    most_recent_state.stn_bm_id != stn_bm_id
                ):
                    continue

                # Can't extend if we are at SPS channel limit for the pipeline
                if len(most_recent_state.freq_ids) >= self._sps_ch_max_in_fpga:
                    continue

                # compare same length, each item same (always call same order?)
                assert (
                    most_recent_state.stns == stn_ids
                ), "Should never happen: PST station order varied"
                assert (
                    most_recent_state.pst_bm_ids == pst_bm_ids
                ), "Should never happen: PST beam IDs varied"

                # propose pipeline extension by the new frequency
                tmp_state = copy.deepcopy(most_recent_state)
                if freq_id not in tmp_state.freq_ids:  # ignore dup freqs
                    tmp_state.freq_ids.append(freq_id)
                    # print(f"freq_{freq_id} added pipeline{i}")

                # chk extension fits processing capacity of FPGA
                if (
                    len(tmp_state.freq_ids) * len(tmp_state.stns)
                    > self._vchan_max_in_fpga
                ):
                    # print(f"freq_{freq_id} doesn't fit in pipeline{i}")
                    continue
            else:
                # propose use of this empty pipeline
                tmp_state = PstState(
                    subarray_id, stn_bm_id, stn_ids, [freq_id], pst_bm_ids
                )
                # print(f"first freq_{freq_id} in pipeline{i}")

            # Check if output bandwidth still within limits of fiber interface
            tot_bw_mbps = 0.0
            for j in range(0, FirmwarePst.PST_SIGPROC_PIPELINES):
                if j == i:
                    latest_state = tmp_state
                else:
                    if self._rsvd_state[j] is not None:
                        latest_state = self._rsvd_state[j]
                    else:
                        if self._state[j] is None:
                            continue
                        latest_state = self._state[j]
                tot_bw_mbps += (
                    len(latest_state.freq_ids)
                    * len(latest_state.pst_bm_ids)
                    * FirmwarePst.PST_MBPS_PER_COARSE
                )

            if tot_bw_mbps <= FirmwarePst.MAX_IO_MBPS:
                self._rsvd_state[i] = tmp_state
                # print(f"freq_{freq_id} ok in pipeline{i}")
                return True

        # print(f"freq_{freq_id} doesn't fit in any pipeline")
        return False

    def delete_reservation(self) -> None:
        """Remove any subarray resevation from this FPGA"""
        for i in range(0, FirmwarePst.PST_SIGPROC_PIPELINES):
            self._rsvd_state[i] = None

    def has_reservation(self) -> bool:
        """Whether there is any reservation in this fpga"""
        for i in range(0, FirmwarePst.PST_SIGPROC_PIPELINES):
            if self._rsvd_state[i] is not None:
                return True
        return False

    def accept_reservation(self):
        """Accept any reservation that exists in this FPGA"""
        accepted = False
        for i in range(0, FirmwarePst.PST_SIGPROC_PIPELINES):
            if self._rsvd_state[i] is not None:
                self._state[i] = self._rsvd_state[i]
                self._rsvd_state[i] = None
                accepted = True
        return accepted

    def get_internal_regs(self) -> list:
        """
        Get representation of the FPGA internal register state for each
        signal processing pipeline. Representation will be used by
        Processor devices to configure their internal registers
        """
        reg_info_list = []
        for pipestate in self._state:
            pipe_info = {}
            if pipestate is not None:
                pipe_info = {
                    "subarray_id": pipestate.subarray_id,
                    "stn_bm_id": pipestate.stn_bm_id,
                    "stns": pipestate.stns,
                    "freq_ids": pipestate.freq_ids,
                    "pst_bm_ids": pipestate.pst_bm_ids,
                }
            reg_info_list.append(pipe_info)
        return reg_info_list

    def get_sps_routing_info(self) -> list:
        """
        :return: list of (subarray, beam, freq, stn, sub-stn) tuples
        for all SPS packets that should be routed to this alveo
        """
        routes = set()
        for i in range(0, FirmwarePst.PST_SIGPROC_PIPELINES):
            if self._state[i] is None:
                continue
            subarray_id = self._state[i].subarray_id  # int
            stn_bm_id = self._state[i].stn_bm_id  # int
            stns = self._state[i].stns  # list of tuples
            freqs = self._state[i].freq_ids  # list of int
            # pst_bm_ids = descriptor["pst_bm_ids"]  # list of int
            for frq in freqs:
                for stn, substn in stns:
                    entry = (subarray_id, stn_bm_id, frq, stn, substn)
                    routes.add(entry)
        return list(routes)

    def clear_subarray(self, sbarry) -> bool:
        """Remove any entries for particular subarray"""
        for i in range(0, FirmwarePst.PST_SIGPROC_PIPELINES):
            if self._state[i] is not None:
                if self._state[i].subarray_id == sbarry:
                    self._state[i] = None
        return self.pipes_used() > 0

    def pipes_used(self) -> int:
        """
        How many signal processing pipelines are used and available
        """
        in_use = 0
        for i in range(0, FirmwarePst.PST_SIGPROC_PIPELINES):
            if self._state[i] is not None or self._rsvd_state[i] is not None:
                in_use += 1
        return in_use

    def subarray_clients(self):
        """Get list of subarrays using this alveo"""
        cli = set()
        for i in range(0, FirmwarePst.PST_SIGPROC_PIPELINES):
            if self._state[i] is not None:
                cli.add(self._state[i].subarray_id)
        return list(cli)

    def get_usage(self):
        """usage by subarray ID"""
        pipe_usage = {}
        bw_usage = {}
        pipes_free = 0
        for i in range(0, FirmwarePst.PST_SIGPROC_PIPELINES):
            if self._state[i] is None:
                pipes_free += 1
                continue
            sa_id = self._state[i].subarray_id
            if sa_id not in pipe_usage:
                pipe_usage[sa_id] = 0
                bw_usage[sa_id] = 0
            pipe_usage[sa_id] += 1
            bw_usage[sa_id] += (
                len(self._state[i].freq_ids)
                * len(self._state[i].pst_bm_ids)
                * self.PST_MBPS_PER_COARSE
            )
        # convert to self-documenting list of dictionaries
        usage_list = []
        tot_bw = 0
        for sa_id, n_pipe in pipe_usage.items():
            sa_use = {
                "sub_id": sa_id,
                "pipes": n_pipe,
                "mbps": math.ceil(bw_usage[sa_id]),
            }
            tot_bw += bw_usage[sa_id]
            usage_list.append(sa_use)
        usage_list.append(
            {
                "sub_id": "*",
                "pipes": pipes_free,
                "mbps": math.floor(self.MAX_IO_MBPS - tot_bw),
            }
        )
        return usage_list
