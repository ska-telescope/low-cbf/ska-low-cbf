# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023, CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=invalid-name,protected-access,too-few-public-methods

"""
This File exists to localise the names of keys in the subarray.Configure
command to a single place in the code.

SubarrayConfig class is an interface to extract information from subarray
configuration provided during the subarray's "Configure" command.
"""
from dataclasses import dataclass


class SubarrayConfig:
    """
    Holds subarray configuration and facilitates extraction of information
    such as beams, beam channels, stations, when required
    """

    def __init__(self, raw_config: dict):
        self._raw_config = raw_config
        # instance variables cache extracted data
        self._stations = None  # list of station tuples
        self._beam_ids = None  # list of station-beam IDs
        self._beam_freqs = None  # dict of beams, with list of freqs for each

        self._vis_stn_beam_ids = None  # list stn_beams for visibilities
        self._pst_stn_beam_ids = None  # list stn_beams for PST beamforming
        self._pss_stn_beam_ids = None

        self._pst_beam_ids = None  # list of pst_beam_ids in stn_beam
        self._pss_beam_ids = None

        self._vis_host_dests = None  # lists of hosts each beam & coarse chan
        self._pst_host_dests = None  # host list for each beam & coarse_chan
        self._pss_host_dests = None

        self._vis_fw_name = None
        self._pst_fw_name = None
        self._pss_fw_name = None

        self._vis_integ_msec = None

    def subarray_id(self) -> int:
        """Subarray ID for this configuration"""
        return self._raw_config["subarray_id"]

    def admin_state(self) -> int:
        """Get subarrays adminstate value (derived from adminmode)"""
        return self._raw_config["status"]

    @property
    def has_visibilities(self) -> bool:
        """Whether config specifies Visibilties are to be calculated"""
        return "vis" in self._raw_config

    @property
    def has_timing_beams(self):
        """Whether config specifies Timing Beams are to be calculated"""
        return "timing_beams" in self._raw_config

    @property
    def has_search_beams(self):
        """Whether config specifies Search Beams are to be calculated"""
        return "search_beams" in self._raw_config

    def stations(self) -> list[tuple]:
        """Get list of subarray station tuples"""
        if self._stations is None:
            self._stations = [
                [stn[0], stn[1]]
                for stn in self._raw_config["stations"]["stns"]
            ]
        return self._stations

    def stn_beam_ids(self) -> list:
        """Get list of station beam IDs"""
        if self._beam_ids is None:
            self._beam_ids = []
            for beam in self._raw_config["stations"]["stn_beams"]:
                self._beam_ids.append(beam["beam_id"])
        return self._beam_ids

    def beam_freqs(self, beam_id) -> list:
        """Get (ordered) list of frequency_ids for a subarray beam"""
        if self._beam_freqs is None:
            self._beam_freqs = {}
            for beam in self._raw_config["stations"]["stn_beams"]:
                this_beam_id = beam["beam_id"]
                self._beam_freqs[this_beam_id] = sorted(beam["freq_ids"])
        return self._beam_freqs[beam_id]

    def vis_firmware_name2(self) -> str:
        """Get name of correlation firmware"""
        if not self.has_visibilities:
            return ""
        if self._vis_fw_name is None:
            if "firmware" in self._raw_config["vis"]:  # new schema
                fw = self._raw_config["vis"]["firmware"]
            elif "fsp" in self._raw_config["vis"]:  # old schema
                fw = self._raw_config["vis"]["fsp"]["firmware"]
            else:  # otherwise use default firmware name
                fw = "vis:"
            self._vis_fw_name = fw
        return self._vis_fw_name

    def vis_integration_ms(self, beam_id) -> int:
        """Get correlation integration time for station beam"""
        if not self.has_visibilities:
            return 0
        if self._vis_integ_msec is None:
            vis_integ_msec = {}
            for beam in self._raw_config["vis"]["stn_beams"]:
                vis_integ_msec[beam["stn_beam_id"]] = beam["integration_ms"]
            self._vis_integ_msec = vis_integ_msec
        return self._vis_integ_msec[beam_id]

    def vis_stn_beams(self) -> list:
        """Get ordered list of visibility beams"""
        if not self.has_visibilities:
            return []
        if self._vis_stn_beam_ids is None:
            vis_beam_ids = []
            for beam_description in self._raw_config["vis"]["stn_beams"]:
                vis_beam_ids.append(beam_description["stn_beam_id"])
            self._vis_stn_beam_ids = sorted(vis_beam_ids)
        return self._vis_stn_beam_ids

    def vis_raw_beam_cfg(self) -> list:
        """extract visibility beams section verbatim from raw config"""
        if not self.has_visibilities:
            return []
        return self._raw_config["vis"]["stn_beams"]

    def pst_firmware_name2(self) -> str:
        """Get name of pst firmware"""
        if not self.has_timing_beams:
            return ""
        if self._pst_fw_name is None:
            if "firmware" in self._raw_config["timing_beams"]:  # new schema
                fw = self._raw_config["timing_beams"]["firmware"]
            elif "fsp" in self._raw_config["timing_beams"]:  # old schema
                fw = self._raw_config["timing_beams"]["fsp"]["firmware"]
            else:
                fw = "pst:"  # use default firmware name
            self._pst_fw_name = fw
        return self._pst_fw_name

    def pst_stn_beams(self) -> list:
        """Get ordered list of pst beams"""
        if not self.has_timing_beams:
            return []
        if self._pst_stn_beam_ids is None:
            pst_beam_ids = set()
            for beam_description in self._raw_config["timing_beams"]["beams"]:
                pst_beam_ids.add(beam_description["stn_beam_id"])
            self._pst_stn_beam_ids = sorted(list(pst_beam_ids))
        return self._pst_stn_beam_ids

    def pst_beams(self, stn_beam_id: int) -> list:
        """Get list of pst beams derived from station beam"""
        if not self.has_timing_beams:
            return []
        if stn_beam_id not in self.pst_stn_beams():
            return []
        if self._pst_beam_ids is None:
            pst_beam_ids = {}
            for beam_description in self._raw_config["timing_beams"]["beams"]:
                stn_beam = beam_description["stn_beam_id"]
                if stn_beam not in pst_beam_ids:
                    pst_beam_ids[stn_beam] = set()
                pst_beam_ids[stn_beam].add(beam_description["pst_beam_id"])
            sorted_beam_ids_list = {}
            for stn_beam, set_of_pst_beams in pst_beam_ids.items():
                sorted_beam_ids_list[stn_beam] = sorted(list(set_of_pst_beams))
            self._pst_beam_ids = sorted_beam_ids_list
        return self._pst_beam_ids[stn_beam_id]

    def pst_raw_beam_cfg(self) -> list:
        """extract pst beams section verbatim from raw config"""
        if not self.has_timing_beams:
            return []
        return self._raw_config["timing_beams"]["beams"]

    # PSS related functionality

    def pss_firmware_name2(self) -> str:
        """Get name of pss firmware"""
        if not self.has_search_beams:
            return ""
        if self._pss_fw_name is None:
            if "firmware" in self._raw_config["search_beams"]:  # new schema
                fw = self._raw_config["search_beams"]["firmware"]
            elif "fsp" in self._raw_config["search_beams"]:  # old schema
                fw = self._raw_config["search_beams"]["fsp"]["firmware"]
            else:  # default firmware  name
                fw = "pss:"
            self._pss_fw_name = fw
        return self._pss_fw_name

    def pss_stn_beams(self) -> list:
        """Get ordered list of pss beams"""
        if not self.has_search_beams:
            return []
        if self._pss_stn_beam_ids is None:
            pss_beam_ids = set()
            for beam_description in self._raw_config["search_beams"]["beams"]:
                pss_beam_ids.add(beam_description["stn_beam_id"])
            self._pss_stn_beam_ids = sorted(list(pss_beam_ids))
        return self._pss_stn_beam_ids

    def pss_beams(self, stn_beam_id: int) -> list:
        """Get list of pss beams derived from station beam"""
        if not self.has_search_beams:
            return []
        if stn_beam_id not in self.pss_stn_beams():
            return []
        if self._pss_beam_ids is None:
            pss_beam_ids = {}
            for beam_description in self._raw_config["search_beams"]["beams"]:
                stn_beam = beam_description["stn_beam_id"]
                if stn_beam not in pss_beam_ids:
                    pss_beam_ids[stn_beam] = set()
                pss_beam_ids[stn_beam].add(beam_description["pss_beam_id"])
            sorted_beam_ids_list = {}
            for stn_beam, set_of_pss_beams in pss_beam_ids.items():
                sorted_beam_ids_list[stn_beam] = sorted(list(set_of_pss_beams))
            self._pss_beam_ids = sorted_beam_ids_list
        return self._pss_beam_ids[stn_beam_id]

    def pss_raw_beam_cfg(self) -> list:
        """extract pss beams section verbatim from raw config"""
        if not self.has_search_beams:
            return []
        return self._raw_config["search_beams"]["beams"]

    N_COARSE = 384
    # 4094 channel correlator filterbank central 27/32 kept
    # (4096 * 27/32 = 3456) and are integrated in groups of 24
    STDVIS_CH_PER_COARSE = 3456 // 24  # == 144 vis ch per coarse

    def _make_vis_host_dests_cache(self):
        """
        Populate self._vis_host_dests cache with list of IP addresses that
        each vis beam and channel should be sent to
        """
        vis_host_dests = {}
        for beam_idx, beam_id in enumerate(self.vis_stn_beams()):
            vis_host_dests[beam_id] = {}
            hosts = self._raw_config["vis"]["stn_beams"][beam_idx]["host"]
            # list of host IPs
            host_ipaddrs = [hosts[i][1] for i in range(0, len(hosts))]
            # list of vis channels each host accepts
            n_host_ch = [
                hosts[i + 1][0] - hosts[i][0] for i in range(0, len(hosts) - 1)
            ]
            # Final host accepts any remaining channels
            n_host_ch.append(self.N_COARSE * self.STDVIS_CH_PER_COARSE)

            host_idx = 0
            host_ip = host_ipaddrs[host_idx]
            host_ch = n_host_ch[host_idx]
            # make list of channels for each frequency in beam
            for freq_id in self.beam_freqs(beam_id):
                n_ch = self.STDVIS_CH_PER_COARSE
                # print(f"bm{beam_id} ch{freq_id}: {host_ip}")
                vis_host_dests[beam_id][freq_id] = [host_ip]
                while host_ch < n_ch:
                    host_idx += 1
                    host_ch += n_host_ch[host_idx]
                    host_ip = host_ipaddrs[host_idx]
                    # print(f"bm{beam_id} ch{freq_id}: {host_ip}")
                    vis_host_dests[beam_id][freq_id].append(
                        host_ipaddrs[host_idx]
                    )
                host_ch -= n_ch
        self._vis_host_dests = vis_host_dests

    def get_vis_host_dests(self, beam_id, chan_id) -> list:
        """
        Get list of hosts the visibilities for the channel should go to

        The "destinations" section of the configuration specifies hosts
        to which visibility channels are sent. Uses SDP Rx format

        There are 345 standard visibility chans per coarse channel
        """
        if not self.has_visibilities:
            return []
        if beam_id not in self.vis_stn_beams():
            return []
        if chan_id not in self.beam_freqs(beam_id):
            return []
        if self._vis_host_dests is None:
            self._make_vis_host_dests_cache()
        return self._vis_host_dests[beam_id][chan_id]

    # 256-chan pst filterbank, central 27/32 are kept
    PST_CH_PER_COARSE = (256 * 27) // 32  # == 216 pst chan per coarse

    def _make_pst_host_dests_cache(self):
        """
        Populate self._pst_host_dests cache with list of IP addresses that
        each vis beam and channel should be sent to
        """
        pst_host_dests = {}
        pst_beams_list = self._raw_config["timing_beams"]["beams"]
        for pst_info in pst_beams_list:
            pst_beam_id = pst_info["pst_beam_id"]
            stn_beam_id = pst_info["stn_beam_id"]
            # Create all pst_beams & their channels initially with no dests
            if pst_beam_id not in pst_host_dests:
                pst_host_dests[pst_beam_id] = {}
                for freq_id in self.beam_freqs(stn_beam_id):
                    pst_host_dests[pst_beam_id][freq_id] = set()
            # Add in destinations for the appropriate channel ranges
            for dest in pst_info["destinations"]:
                host_ip = dest["data_host"]
                chan_base = dest["start_channel"]
                num_chan = dest["num_channels"]
                first_ch_no = int(chan_base / self.PST_CH_PER_COARSE)
                last_ch_no = int(
                    (chan_base + num_chan - 1) / self.PST_CH_PER_COARSE
                )
                for idx, freq_id in enumerate(self.beam_freqs(stn_beam_id)):
                    if first_ch_no <= idx <= last_ch_no:
                        pst_host_dests[pst_beam_id][freq_id].add(host_ip)

        self._pst_host_dests = pst_host_dests

    def get_pst_host_dests(self, pst_beam_id, chan_id) -> list:
        """Get list of hosts the pst beams for the channel should go to"""
        if not self.has_timing_beams:
            return []
        if self._pst_host_dests is None:
            self._make_pst_host_dests_cache()
        if pst_beam_id not in self._pst_host_dests:
            return []
        if chan_id not in self._pst_host_dests[pst_beam_id]:
            return []
        return list(self._pst_host_dests[pst_beam_id][chan_id])

    # 64-chan pss filterbank, central 27/32 are kept
    PSS_CH_PER_COARSE = (64 * 27) // 32  # == 54 pss chan per coarse

    def _make_pss_host_dests_cache(self):
        """
        Populate self._pss_host_dests cache with list of IP addresses that
        each vis beam and channel should be sent to

        This is just a copy of PST code at present, it is expected to change
        when PSS advises how they want their beams directed to servers
        """
        pss_host_dests = {}
        pss_beams_list = self._raw_config["search_beams"]["beams"]
        for pss_info in pss_beams_list:
            pss_beam_id = pss_info["pss_beam_id"]
            stn_beam_id = pss_info["stn_beam_id"]
            # Create all pss_beams & their channels initially with no dests
            if pss_beam_id not in pss_host_dests:
                pss_host_dests[pss_beam_id] = {}
                for freq_id in self.beam_freqs(stn_beam_id):
                    pss_host_dests[pss_beam_id][freq_id] = set()
            # Add in destinations for the appropriate channel ranges
            for dest in pss_info["destinations"]:
                host_ip = dest["data_host"]
                chan_base = dest["start_channel"]
                num_chan = dest["num_channels"]
                first_ch_no = int(chan_base / self.PST_CH_PER_COARSE)
                last_ch_no = int(
                    (chan_base + num_chan - 1) / self.PST_CH_PER_COARSE
                )
                for idx, freq_id in enumerate(self.beam_freqs(stn_beam_id)):
                    if first_ch_no <= idx <= last_ch_no:
                        pss_host_dests[pss_beam_id][freq_id].add(host_ip)

        self._pss_host_dests = pss_host_dests

    def get_pss_host_dests(self, pss_beam_id, chan_id) -> list:
        """
        Get list of hosts the pss beams for the channel should go to

        This is a copy of PST code at present, it is expected to change
        """
        if not self.has_timing_beams:
            return []
        if self._pss_host_dests is None:
            self._make_pss_host_dests_cache()
        if pss_beam_id not in self._pss_host_dests:
            return []
        if chan_id not in self._pss_host_dests[pss_beam_id]:
            return []
        return list(self._pss_host_dests[pss_beam_id][chan_id])


@dataclass
class SubarrayDataProduct:
    product_type: str  # name of product "vis", "pst", "pss"
    firmware_name: str  # firmware image to use
    station_beams: list[int]  # station-beams that want this product


def get_data_products(s_cfg: SubarrayConfig) -> list[SubarrayDataProduct]:
    """Determine which station beams produce which output (vis/pst/pss)"""
    products = []
    products.append(
        SubarrayDataProduct(
            "vis", s_cfg.vis_firmware_name2(), s_cfg.vis_stn_beams()
        )
    )
    products.append(
        SubarrayDataProduct(
            "pst", s_cfg.pst_firmware_name2(), s_cfg.pst_stn_beams()
        )
    )
    products.append(
        SubarrayDataProduct(
            "pss", s_cfg.pss_firmware_name2(), s_cfg.pss_stn_beams()
        )
    )
    products = [
        product for product in products if len(product.station_beams) != 0
    ]
    return products
