# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=invalid-name,protected-access,too-few-public-methods

"""
SKA Low CBF

Default hardware connections for use in testing testing environments.

This will be loaded if no hardware connections are specified in the Helm chart

It allows test environments to exercise ska-low-cbf without having any FPGA
hardware present
"""

N_ALV = 10
N_SDP = 1
N_PST = 8
N_PSS = 30
N_STN = 512


def default_connection_list() -> list:
    """
    Create a list of strings that mimic the connection table usually
    specified in a helm chart associated with a site
    """

    next_port = 1
    connections = []

    # Note the code currently assumes a single switch (i.e. AA1 or before)

    # each alveo connects to its own switch port
    for i in range(0, N_ALV):
        sw = f"switch=p4_01  port={next_port}/0  speed=100"
        alv = f" alveo=alv{i+1:03d}"
        connections.append(sw + alv)
        next_port += 1

    # sdp links use a switch port
    for i in range(0, N_SDP):
        sw = f"switch=p4_01  port={next_port}/0  speed=100"
        sdp = f" link=sdp_{i+1:03d}"
        connections.append(sw + sdp)
        next_port += 1

    # There are 4x25G pst links per switch port (using 4x25G lanes per port)
    lane = 0
    for i in range(0, N_PST):
        sw = f"switch=p4_01  port={next_port}/{lane}  speed=25"
        pst = f" link=pst_{i+1:03d}"
        connections.append(sw + pst)
        lane += 1
        if lane >= 4:
            lane = 0
            next_port += 1
    if lane != 0:
        next_port += 1

    # Expect 3 pss servers on each 25G switch port (using 4x25G lanes per port)
    lane = 0
    server = 0
    for i in range(0, N_PSS):
        sw = f"switch=p4_01  port={next_port}/{lane}  speed=25"
        pss = f" link=pss_{i+1:03d}"
        connections.append(sw + pss)
        server += 1
        if server >= 3:
            server = 0
            lane += 1
            if lane >= 4:
                lane = 0
                next_port += 1
    if lane != 0:
        next_port += 1

    # we give each station its own switch port for now
    for i in range(0, N_STN):
        sw = f"switch=p4_01  port={next_port}/0  speed=100"
        stn = f"  link=stn_{i+1:03d}"
        connections.append(sw + stn)
        next_port += 1

    return connections
