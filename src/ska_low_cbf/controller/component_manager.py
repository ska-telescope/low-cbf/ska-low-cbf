# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2022 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
SKA Low CBF
Controller Component Manager
"""
import logging

from ska_tango_base.base import BaseComponentManager
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import CommunicationStatus, PowerState

from ska_low_cbf.controller.controller import Controller


class LowCbfControllerComponentManager(BaseComponentManager):
    """
    Component Manager for LowCBF controller
    At present only allows for LowCBF controller to be turned on
    """

    def __init__(
        self,
        logger: logging.Logger,
        communication_state_callback,  #: Callable[[CommunicationStatus]],
        component_state_callback,  #: Callable,
    ):
        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            power=PowerState.UNKNOWN,
            fault=None,
        )
        self.controller = Controller()
        self._communication_state_cb = communication_state_callback
        self._component_state_cb = component_state_callback

    # inherited from BaseComponentManager
    def start_communicating(self):
        self._communication_state_cb(CommunicationStatus.ESTABLISHED)
        self._component_state_cb(power=PowerState.ON, fault=False)

    def stop_communicating(self):
        self._communication_state_cb(CommunicationStatus.DISABLED)

    def off(self, _):  # 2nd arg is "task_callback: Callable"
        message = "Ignored OFF, controller does not have hardware"
        self.logger.error(message)
        return (ResultCode.REJECTED, message)

    def standby(self, _):  # 2nd arg is "task_callback: Callable"
        message = "Ignored STANDBY, controller not does have hardware"
        self.logger.error(message)
        return (ResultCode.REJECTED, message)

    def on(self, _):  # 2nd arg is "task_callback: Callable"
        message = "Ignored ON, controller does not have hardware"
        self.logger.error(message)
        return (ResultCode.REJECTED, message)

    def reset(self, _):  # 2nd arg is "task_callback: Callable"
        message = "Ignored RESET, controller does not have hardware"
        self.logger.error(message)
        return (ResultCode.REJECTED, message)
