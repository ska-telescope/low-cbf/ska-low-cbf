# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

""" Low CBf Controller

This file implements the Controller's base functionality.
See also controller_device, which handles the TANGO layer.
"""

import enum
import warnings
from collections import defaultdict
from typing import Dict, List

from ska_tango_base.control_model import HealthState, ObsMode


class SearchBeamBandwidthMode(enum.IntEnum):
    """Python enumerated type for SearchBeamBandwidthMode attribute."""

    SINGLE = 0
    DOUBLE = 1


class Controller:
    """
    Sub-element controller for Low CBf
    """

    def __init__(self):
        """create a new Controller"""
        self._health_state = HealthState.UNKNOWN
        self._device_health = {}
        """Health of downstream devices. key: device fqdn, value: healthState"""
        self._device_type = {}
        """Type of downstream devices. key: device fqdn, value: device type"""

        self._subarrays = (
            {}
        )  # dict of subarray numbers, each a dict of attributes
        self._search_beam_bandwidth_mode = SearchBeamBandwidthMode.SINGLE

    @property
    def search_beam_bandwidth_mode(self):
        """Get the current search beam bandwidth mode"""
        return self._search_beam_bandwidth_mode

    @search_beam_bandwidth_mode.setter
    def search_beam_bandwidth_mode(self, mode):
        """Set the search beam bandwidth mode"""
        try:
            SearchBeamBandwidthMode(mode)
        except ValueError:
            # requested mode doesn't exist in enum
            return

        # check all subarrays are idle
        print(self._subarrays)
        if all(
            subarray["obsMode"] == ObsMode.IDLE
            for subarray in self._subarrays.values()
        ):
            self._search_beam_bandwidth_mode = mode
        # TODO - how to forward this setting to all subarrays?
        # if we had a collection of subarray device proxies we could do it...
        # or should we defer to the Controller tango device?
        return

    @property
    def health_state(self):
        """Calculate overall health state from input device health states"""
        # for now, we take the worst case
        if self._device_health:
            # Exclude any invalid (None) entries in the table
            self._health_state = max(
                (
                    health
                    for health in self._device_health.values()
                    if health is not None
                )
            )
        else:
            self._health_state = HealthState.UNKNOWN
        return self._health_state

    @property
    def device_health_states(self) -> Dict[str, HealthState]:
        """All device FQDNs & Health States that contribute to ``health_state``."""
        return self._device_health

    def update_health_state(
        self, fqdn: str, health_state: HealthState, valid: bool = True
    ) -> None:
        """
        Update an entry in our Health State table.

        Called on each health update event received.

        :param fqdn: Address of device that we are updating
        :param health_state: Current health state of device
        :param valid: Is the update valid? (e.g. Quality indicator)
          Invalid values are ignored when evaluating overall health.
        """
        if fqdn not in self._device_health:
            warnings.warn(
                f"Health update received for unexpected device {fqdn}"
            )
        if valid:
            self._device_health[fqdn] = health_state
        else:
            self._device_health[fqdn] = None

    def add_health_device(self, device_type: str, fqdn: str) -> None:
        """
        Add a device for health monitoring.

        :param device_type: Type (Class) of device, e.g. "LowCbfConnector"
        :param fqdn: Tango address of device, e.g. "low-cbf/connector/3"
        """
        self._device_type[fqdn] = device_type
        self._device_health[fqdn] = None

    @property
    def health_by_type(self) -> Dict[str, List[HealthState]]:
        """Health of monitored devices, by device type."""
        health_by_type = defaultdict(list)
        for fqdn, device_type in self._device_type.items():
            health_by_type[device_type].append(self._device_health[fqdn])
        return health_by_type
