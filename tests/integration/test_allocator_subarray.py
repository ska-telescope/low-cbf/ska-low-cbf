# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
test LowCbfSubarray using DeviceTestContext
"""
import json
from time import sleep

import pytest
from ska_tango_base.control_model import AdminMode
from tango import DevFailed, DeviceProxy
from tango._tango import DevState


@pytest.fixture()
def subarray04():
    """Proxy to Low CBF Subarray 4"""
    subarray = DeviceProxy("low-cbf/subarray/04")

    # Wait until the subarray device completes initialisation before using it
    tries = 0
    while tries < 30:
        tries += 1
        try:
            dev_initialised = subarray.State() != DevState.INIT
        except DevFailed:
            dev_initialised = False
        if dev_initialised:
            break
        print(f"wait for subarray init to complete [{tries}]")
        sleep(1)

    # ensure that subarray is on before using
    subarray.adminMode = AdminMode.ONLINE
    timeout_count = 0
    while (subarray.State() != DevState.ON) and (timeout_count < 60):
        sleep(1)
        timeout_count += 1
    print(f"{timeout_count} seconds to subarray ON")

    yield subarray


# pylint: disable=no-self-use,redefined-outer-name
@pytest.mark.post_deployment
@pytest.mark.usefixtures("subarray04")
class TestSubarrayResourceAssignment:
    """
    Tests behaviour of the Subarray device ResourceAssignment command,
    which interacts with the Allocator device
    """

    def test_configure_scan(self, subarray04):
        """Test Configure Scan Command"""
        # AssignResources doesn't do anything but next level up might call it?
        subarray04.AssignResources("{}")

        stn_substn_list = [(stn, 0) for stn in range(1, 1 + 307)]
        scan_config = {
            "subarray_id": 4,
            "stations": {
                "stns": stn_substn_list,
                "stn_beams": [
                    {
                        "beam_id": 11,
                        "freq_ids": [65],
                        "delay_poly": "some_url",
                    },
                ],
            },
            "vis": {
                "fsp": {"firmware": "vis", "fsp_ids": [1]},
                "stn_beams": [
                    {
                        "stn_beam_id": 11,
                        "host": [[0, "192.168.0.1"]],
                        "port": [[0, 9000, 1]],
                        "integration_ms": 849,
                    },
                ],
            },
        }
        subarray04.Configure(json.dumps(scan_config))
