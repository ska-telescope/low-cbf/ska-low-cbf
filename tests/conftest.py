"""
Common definitions for Low CBF testing
"""

import logging
import socket

import pytest
import tango
from tango import Database
from tango.test_context import MultiDeviceTestContext, get_host_ip


# pylint: disable=unused-argument
def pytest_sessionstart(session):
    """
    Pytest hook; prints info about tango version.
    :param session: a pytest Session object
    :type session: :py:class:`pytest.Session`
    """
    print(tango.utils.info())


def pytest_addoption(parser):
    """
    Pytest hook; implemented to add the `--true-context` option, used to
    indicate that a true Tango subsystem is available, so there is no
    need for a :py:class:`tango.test_context.MultiDeviceTestContext`.
    :param parser: the command line options parser
    :type parser: :py:class:`argparse.ArgumentParser`
    """
    parser.addoption(
        "--true-context",
        action="store_true",
        default=False,
        help=(
            "Tell pytest that you have a true Tango context and don't "
            "need to spin up a Tango test context"
        ),
    )


# DevFactory copied from ska-tango-examples
class DevFactory:
    # pylint: disable=line-too-long
    """
    This class is an easy attempt to develop the concept developed by MCCS team
    in the following confluence page:
    https://confluence.skatelescope.org/display/SE/Running+BDD+tests+in+multiple+harnesses

    It is a factory class which provide the ability to create an object of
    type DeviceProxy.

    When testing the static variable _test_context is an instance of
    the TANGO class MultiDeviceTestContext.

    More information on tango testing can be found at the following link:
    https://pytango.readthedocs.io/en/stable/testing.html

    """

    _test_context = None

    def __init__(self, green_mode=tango.GreenMode.Synchronous):
        self.dev_proxys = {}
        self.logger = logging.getLogger(__name__)
        self.default_green_mode = green_mode

    def get_device(self, dev_name, green_mode=None):
        """
        Create (if not done before) a DeviceProxy for the Device fqnm

        :param dev_name: Device name
        :param green_mode: tango.GreenMode (synchronous by default)

        :return: DeviceProxy
        """
        if green_mode is None:
            green_mode = self.default_green_mode

        if DevFactory._test_context is None:
            if dev_name not in self.dev_proxys:
                self.logger.info("Creating Proxy for %s", dev_name)
                self.dev_proxys[dev_name] = tango.DeviceProxy(
                    dev_name, green_mode=green_mode
                )
            return self.dev_proxys[dev_name]
        return DevFactory._test_context.get_device(dev_name)


@pytest.fixture
def tango_context(devices_to_load, request):
    """
    Get the tango test context (if appropriate)
    :returns None or MultiDeviceTestContext
    """
    true_context = request.config.getoption("--true-context")
    logging.info("true context: %s", true_context)
    if not true_context:
        with MultiDeviceTestContext(devices_to_load, process=False) as context:
            DevFactory._test_context = context
            yield context
    else:
        yield None


@pytest.fixture(scope="module")
def devices_to_test(request):
    """Get the module's devices_to_test"""
    yield getattr(request.module, "devices_to_test")


@pytest.fixture(scope="function")
def multi_device_tango_context(
    mocker, devices_to_test  # pylint: disable=redefined-outer-name
):
    """
    Creates and returns a TANGO MultiDeviceTestContext object, with
    tango.DeviceProxy patched to work around a name-resolving issue.
    """

    def _get_open_port():
        skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        skt.bind(("", 0))
        skt.listen(1)
        port = skt.getsockname()[1]
        skt.close()
        return port

    host = get_host_ip()
    port = _get_open_port()
    device_proxy = tango.DeviceProxy
    mocker.patch(
        "tango.DeviceProxy",
        wraps=lambda fqdn, *args, **kwargs: device_proxy(
            f"tango://{host}:{port}/{fqdn}#dbase=no", *args, **kwargs
        ),
    )
    with MultiDeviceTestContext(
        devices_to_test, host=host, port=port, process=True
    ) as context:
        yield context


def collect_all_attr(dev_class, attr):
    """
    Collect an attribute (attr) value from all instances of a device class
    (dev_class).
    """
    retval = []
    for device in Database().get_device_exported_for_class(dev_class):
        device_proxy = tango.DeviceProxy(device)
        retval.append(getattr(device_proxy, attr))
    return retval


def collect_all_cmd(dev_class, cmd, arg=None):
    """
    Collect a command (cmd) return value from all instances of a device class
    (dev_class).
    Optionally specify command arguments with 'arg'.
    """
    retval = []
    for device in Database().get_device_exported_for_class(dev_class):
        device_proxy = tango.DeviceProxy(device)
        retval.append(device_proxy.command_inout(cmd, arg))
    return retval


@pytest.fixture()
def json_subarray_assign_pst():
    """JSON text for PST assignment to subarray"""
    return """{
        "lowcbf": {
            "stations": [
                {"station_id": 1, "sub_station_id": 1},
                {"station_id": 3, "sub_station_id": 1},
                {"station_id": 3, "sub_station_id": 2}
            ],
            "station_beams": [
                {
                    "station_beam_id": 1,
                    "channels": [1, 2, 3, 4, 5, 6, 7, 8],
                    "pst_beams": [
                        {"pst_beam_id": 1},
                        {"pst_beam_id": 2}
                    ]
                },
                {
                    "station_beam_id": 2,
                    "channels": [9, 10, 11, 12, 13, 14, 15],
                    "pst_beams": [
                        {"pst_beam_id": 3}
                    ]
                }
            ]
        }
    }"""


@pytest.fixture()
def fsp_assign():
    """Parameters for assignment of FSP to subarray"""
    return {
        "common": {"subarrayID": 91},
        "lowcbf": {
            "resources": [
                {
                    "device": "fsp_01",
                    "shared": True,
                    "fw_image": "xyz.bit",
                    "fw_mode": "pst",
                }
            ]
        },
    }


@pytest.fixture()
def fsp_assign_json():
    """Parameters for assignment of FSP to subarray"""
    # 2023-05-11 subarray assign now takes empty argument
    return """{}"""
