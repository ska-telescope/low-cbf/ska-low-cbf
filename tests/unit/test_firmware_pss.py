# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import pytest

from ska_low_cbf.allocator.firmware_pss import FirmwarePss

# Tests assume FPGA capabilities are not limited by FPGA implementation issues:
ALVEO_LIMITS_IDEAL = {
    "pst": {
        "vch": 1024,  # if beamformer clock frequency fixed
        "sps_ch": 1024,  # if packetiser first chans was 32k
    },
    "pss": {
        "vch": 1024,  # if FPGA failures near 1020 vch fixed
        "sps_ch": 1024,  # Max SPS chans per pipeline (VCT limit)
    },
    "vis": {
        "vch": 1024,  # Max Virtual Channels into FPGA
        "hbm": 606,  # Max VChans in one matrix correlator's (MxC) 2Bbyte HBM
        "bli": (512 * 513) // 2,  # Max baselines in one MxC
    },
}


NUM_PIPELINES = 2

MBPS_PER_COARSE_PSS_BM = 26.808
MAX_MBPS = 100_000


@pytest.mark.parametrize(
    "num_subarrays, num_stn_beams, num_pss_bms, num_chans, num_stns, xrslt",
    [
        (2, 1, 5, 2, 512, True),
        (2, 2, 1, 2, 512, False),
        (4, 1, 1, 1, 4, False),  # 4x absolutely minimal sized subarrays
        (1, 4, 1, 1, 4, False),  # 1 subarray 4stn_bms minimal stns, chans
        (4, 1, 5, 2, 512, False),  # 4x maximal subarrays that fit a pipeline
    ],
)
def test_pss_beamformer_max_2_sbarry_bm(
    num_subarrays, num_stn_beams, num_pss_bms, num_chans, num_stns, xrslt
):
    """
    Check class representing PSS beamformer FPGA handles up to 2 pss-beams

    This particular test is checking that a PSS signal processing pipeline only
    allows one subarray-beam per pipeline. The FPGA has no support for multiple
    subarrays in a single pipeline. So if the first two numbers multiply to
    more than two (Two signal processing pipelines in PSS FPGAs) then it should
    fail and otherwise suceed.

    Other tests check numbers of pss beams, channels and stations, but
    for this test:
    - Up to 512 PST beams can be formed from a single subarray-beam so anything
      up to 512 is ok
    - Stations and channels don't matter provided the numbers multiply to
      something that fits in the 1024 VCT entries so the numbers are chosen
      small for this test
    """

    fw_repr = FirmwarePss(ALVEO_LIMITS_IDEAL)
    stn_ids = [(i, 0) for i in range(0, num_stns)]
    tot_stn_bms = 0
    for suba in range(0, num_subarrays):
        sbarry_id = suba + 1
        for stbm in range(0, num_stn_beams):
            stn_bm_id = stbm + 1
            tot_stn_bms += 1
            pss_bm_ids = [_ + 1 for _ in range(0, num_pss_bms)]
            for freq_id in range(0, num_chans):
                rslt = fw_repr.pss_reserve(
                    stn_ids, sbarry_id, stn_bm_id, freq_id, pss_bm_ids
                )
    assert rslt == xrslt


@pytest.mark.parametrize(
    "num_pss_bms, num_chans, num_stns, xrslt",
    [
        # Test a bit beyond what would fit
        (1, 4, 512, True),  # 4chan x 512stn fits in 2 pipeline, but not 5 chan
        (
            1,
            5,
            512,
            False,
        ),  # 4chan x 512stn fits in 2 pipeline, but not 5 chan
        (1, 512, 4, True),  # 512ch * 4stn fits in 2 pipelines, not 513 ch
        (1, 513, 4, False),  # 512ch * 4stn fits in 2 pipelines, not 513 ch
    ],
)
def test_pss_beamformer_max_chans(num_pss_bms, num_chans, num_stns, xrslt):
    """
    Check class representing PSS beamformer handles chans and stns up to
    stns * chans < 1024 for each of the 2 pipelines
    """

    fw_repr = FirmwarePss(ALVEO_LIMITS_IDEAL)

    stn_ids = [(i, 0) for i in range(0, num_stns)]
    pss_bm_ids = [i for i in range(0, num_pss_bms)]

    for freq_id in range(0, num_chans):
        rslt = fw_repr.pss_reserve(stn_ids, 1, 1, freq_id, pss_bm_ids)

    assert rslt == xrslt


@pytest.mark.parametrize(
    "num_pss_bms, num_chans, num_stns, xrslt",
    [
        # 373beams * 10ch would almost generate 100Gbps
        (373, 10, 4, True),
        (374, 10, 4, False),
    ],
)
def test_pss_beamformer_max_bw(num_pss_bms, num_chans, num_stns, xrslt):
    """
    Check class representing PSS beamformer correctly passes/fails
    when generating many pss beam channels provided output bandwidth
    is within the 100Gbps of the network interface
    """

    fw_repr = FirmwarePss(ALVEO_LIMITS_IDEAL)

    stn_ids = [(i, i) for i in range(0, num_stns)]
    pss_bm_ids = list(range(0, num_pss_bms))

    tot_chans = 0
    for freq_id in range(0, num_chans):
        _ = fw_repr.pss_reserve(stn_ids, 1, 1, freq_id, pss_bm_ids)
        tot_chans += 1
        bw_mbps = tot_chans * num_pss_bms * MBPS_PER_COARSE_PSS_BM

    bw_rslt = bw_mbps <= MAX_MBPS
    assert bw_rslt == xrslt


ALVEO_LIMITS_LOW = {
    "pss": {
        "vch": 100,  # if beamformer clock frequency fixed
        "sps_ch": 20,  # if packetiser first chans was 32k
    },
}


@pytest.mark.parametrize(
    "num_chans, num_stns, expected_result",
    [
        # chans, stns, expected alloc result (2 pipelines in PSS)
        (NUM_PIPELINES * 4, 26, False),  # 4*26=104vch > vch limit
        (NUM_PIPELINES * 4, 25, True),
        (NUM_PIPELINES * 21, 4, False),  # 21 > ch limit
        (NUM_PIPELINES * 20, 4, True),
    ],
)
def test_pss_lowered_limit(num_chans, num_stns, expected_result):
    """Test resource limiting mechanism"""
    fw_repr = FirmwarePss(ALVEO_LIMITS_LOW)
    stn_ids = [(i, i) for i in range(0, num_stns)]
    pss_bm_ids = list(range(0, 1))  # only one beam needed for limit test

    for freq_id in range(0, num_chans):
        rslt = fw_repr.pss_reserve(stn_ids, 1, 1, freq_id, pss_bm_ids)

    assert rslt == expected_result


@pytest.mark.parametrize(
    "sa_id, stn_bm_id, n_stns, n_pss",
    [
        # subarray_id, stn_beam_id, n_stations, n_pss_beam
        (1, 9, 8, 16),
        (2, 10, 2, 16),
        (3, 11, 256, 16),
        (4, 14, 512, 16),
        (5, 15, 600, 16),
    ],
)
def test_capacity_estimator(sa_id, stn_bm_id, n_stns, n_pss):
    """Test fpga channel capacity estimation matches fit"""
    fw_repr = FirmwarePss(ALVEO_LIMITS_IDEAL)

    stns = [(x + 1, x + 1) for x in range(0, n_stns)]
    pst_bm_ids = [x + 1 for x in range(0, n_pss)]
    frq_id = 100

    # add in some initial used capacity to make it more difficult
    fw_repr.pss_reserve(stns, sa_id + 1, 3, frq_id, pst_bm_ids)
    fw_repr.accept_reservation()

    # How many channels should fit?
    n_chan_expected = fw_repr.chan_capacity(sa_id, stn_bm_id, n_stns, n_pss)

    stns = [(x + 1, x + 1) for x in range(0, n_stns)]
    pss_bm_ids = [x + 1 for x in range(0, n_pss)]
    # all the expected channels should fit
    chans = [64 + i for i in range(0, n_chan_expected)]
    for ch in chans:
        rslt = fw_repr.pss_reserve(stns, sa_id, stn_bm_id, ch, pss_bm_ids)
        assert rslt is True, f"chan {ch-64+1} of {n_chan_expected} didn't fit"

    # One more channel should fail to fit
    extra_ch = 64 + n_chan_expected
    rslt = fw_repr.pss_reserve(stns, sa_id, stn_bm_id, extra_ch, pss_bm_ids)
    assert rslt is False, "unexpected extra channel fitted!"
