# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
This tests checks that stations in a subarray.configure request are sorted
correctly into ascending order by station and sub-station


"""

import copy

import pytest

from ska_low_cbf.subarray.subarray import sort_stns_in_subarray_cfg

configure_requests = [
    # 1. Subarray with stations already in correct ordering
    {
        "stations": {
            "stns": [
                [1, 1],
                [3, 1],
                [4, 1],
                [22, 3],
                [22, 2],
            ],
        },
    },
    # 2. Subarray that is not in correct order
    {
        "stations": {
            "stns": [
                [1, 1],
                [22, 2],
                [22, 1],
                [3, 1],
                [4, 1],
            ],
        },
    },
    # 3. subarray with timing beams and search beams
    {
        "stations": {
            "stns": [
                [1, 1],
                [22, 2],
                [22, 1],
                [3, 1],
                [4, 1],
            ],
        },
        "timing_beams": {
            "beams": [
                {"stn_weights": [1.0, 2.0, 3.0, 4.0, 5.0]},
                {"stn_weights": [5.0, 4.0, 3.0, 2.0, 1.0]},
                {"stn_weights": [5.0, 4.0, 3.0, 2.0, 1.0, 6.0]},
                {"stn_weights": [1.0, 2.0, 3.0, 4.0]},
            ],
        },
        "search_beams": {
            "beams": [
                {"stn_weights": [1.0]},
                {"stn_weights": [1.0, 2.0, 3.0]},
            ],
        },
    },
]
"""
Subarray.configure() test cases
(Cut-down to just the parts that matter for the sorting)
"""


@pytest.mark.parametrize(
    "config",
    configure_requests,
)
def test_stn_ordering(config):
    """
    Check that stations are ordered in ascending order regardless of
    which order they were provided in
    """

    lcl_cfg = copy.deepcopy(config)  # don't mutate the original config

    sort_stns_in_subarray_cfg(lcl_cfg)  # sort the config

    prev_stn = None
    for stn in lcl_cfg["stations"]["stns"]:
        if prev_stn:
            # stn number has to increment, or if same stn, sub-stn increments
            assert (stn[0] > prev_stn[0]) or (
                (stn[0] == prev_stn[0]) and (stn[1] > prev_stn[1])
            ), "Station sorting error"
        prev_stn = stn


@pytest.mark.parametrize(
    "config",
    configure_requests,
)
def test_pst_stn_wts_ordering(config):
    """
    Check that timing beam weights still match stations after sorting
    """
    if "timing_beams" not in config:
        return
    lcl_cfg = copy.deepcopy(config)  # don't mutate the original config
    orig_stns = copy.deepcopy(lcl_cfg["stations"]["stns"])

    sort_stns_in_subarray_cfg(lcl_cfg)  # sort the lcl_cfg

    for cnt, beam in enumerate(lcl_cfg["timing_beams"]["beams"]):
        orig_wts = copy.deepcopy(
            config["timing_beams"]["beams"][cnt]["stn_weights"]
        )
        # abbreviated weights description to full length
        if len(orig_wts) < len(orig_stns):
            orig_wts.extend([orig_wts[-1]] * (len(orig_stns) - len(orig_wts)))

        # verify each station-weight for every station in the beam
        for idx, stn in enumerate(lcl_cfg["stations"]["stns"]):
            wt = lcl_cfg["timing_beams"]["beams"][cnt]["stn_weights"][idx]
            assert (
                wt == orig_wts[orig_stns.index(stn)]
            ), "timing beam station-weights sorting error"


@pytest.mark.parametrize(
    "config",
    configure_requests,
)
def test_pss_stn_wts_ordering(config):
    """
    Check that search beam weights still match stations after sorting
    (currently almost same as timing-beams, but may change soon)
    """
    if "search_beams" not in config:
        return
    lcl_cfg = copy.deepcopy(config)  # don't mutate the original config
    orig_stns = copy.deepcopy(lcl_cfg["stations"]["stns"])

    sort_stns_in_subarray_cfg(lcl_cfg)  # sort the lcl_cfg

    for cnt, beam in enumerate(lcl_cfg["search_beams"]["beams"]):
        orig_wts = copy.deepcopy(
            config["search_beams"]["beams"][cnt]["stn_weights"]
        )
        # abbreviated weights description to full length
        if len(orig_wts) < len(orig_stns):
            orig_wts.extend([orig_wts[-1]] * (len(orig_stns) - len(orig_wts)))

        # verify each station-weight for every station in the beam
        for idx, stn in enumerate(lcl_cfg["stations"]["stns"]):
            wt = lcl_cfg["search_beams"]["beams"][cnt]["stn_weights"][idx]
            assert (
                wt == orig_wts[orig_stns.index(stn)]
            ), "search beam station-weights sorting error"
