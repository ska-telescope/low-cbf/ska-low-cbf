# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import logging

import pytest
from sw_cnx import aa3, create_cnx_list, marsfield_psi

from ska_low_cbf.allocator.arp_replies import ArpReplies
from ska_low_cbf.allocator.resources import Resources


@pytest.fixture()
def arp_db():  # def arp_db(logger):
    """Return an arp database for test"""
    return ArpReplies()


# Array config for these tests
array_config_small = {
    "stations": 8,
    "channels": 382,
    "subarrays": 16,
    "alveos_per_fsp": 1,
    "pss_beams": 22,
    "pst_beams": 4,
}


def test_sdp_arp_and_routing(arp_db):
    """
    Test that ARP reply database successfully stores ARP replies
    And that an ARP reply for a SDP host allows routes to be
    calculated to the host
    """

    # create sample connections list
    cnx, _ = create_cnx_list(marsfield_psi)  # don't need summary _
    n_alveo = 8

    res = Resources(array_config_small, cnx)
    # Register the available alveos similar to FSP registration
    for i in range(0, n_alveo):
        # serial#_N format used in create_cnx_list for fake serial numbers
        success, msg = res.alveo_registration(
            {
                "serial": f"serial#_{i+1}",
                "hw": "u55c",
                "tango_dev": None,
                "status": 1,
            }
        )
        assert success, msg

    # subarray 4 stns x 1 channel with 1 timing beam should fit
    stn_substn_list = [(stn, 0) for stn in range(1, 1 + 4)]  # all AA3 stations
    frq_list = [65 + i for i in range(0, 1)]  # No. coarse channels
    suba_id = 13
    scan_config = {
        "subarray_id": suba_id,
        "status": 1,
        "stations": {
            "stns": stn_substn_list,
            "stn_beams": [
                {
                    "beam_id": 11,
                    "freq_ids": frq_list,
                    "boresight_dly_poly": "some_url",
                },
            ],
        },
        "vis": {
            # "fsp": {"firmware": "vis", "fsp_ids": [1, 2, 3, 4]},
            "firmware": "vis",
            "stn_beams": [
                {
                    "stn_beam_id": 11,
                    "host": [[0, "192.168.0.1"]],
                    "port": [[0, 9000, 1]],
                    "integration_ms": 849,
                },
            ],
        },
    }
    (rslt, _) = res.cfg_scan2(scan_config)
    assert (
        rslt is True
    ), "config should succeed using 120/144 PST beamformer chans"

    # add arp reply for host we don't want, and check it's really present
    arp_db.arp_update(
        "p4_01",
        """{"192.168.0.2": {"mac": "c:42:a1:9c:a2:1b", "port": "38/0"}}""",
    )
    assert arp_db.num_hosts() == 1, "expected only one host in arp replies"

    # See if arps can satisfy the routing required by the configured subarray
    # (shouldn't be able to since host is not one configuration wants)
    rslt_dict = res.get_sdp_routes(arp_db)
    assert isinstance(rslt_dict, dict)
    assert "p4_01" in rslt_dict, "switch should be in dict"
    rtes = rslt_dict["p4_01"]["routes"]
    assert len(rtes) == 0, f"oops no routes expected, got routes={rtes}"

    # add arp reply for host we do want, check both are present now
    arp_db.arp_update(
        "p4_01",
        """{"192.168.0.1": {"mac": "c:42:a1:9c:a2:1b", "port": "38/0"}}""",
    )
    assert arp_db.num_hosts() == 2, "expected two hosts in arp replies"

    # See if arp replies can satisfy the routing required by subarray
    # (should work since arp database has reply from host subarray wants)
    rslt_dict = res.get_sdp_routes(arp_db)
    assert isinstance(rslt_dict, dict)
    assert "p4_01" in rslt_dict, "switch should be in dict"
    rtes2 = rslt_dict["p4_01"]["routes"]
    assert len(rtes2) == 1, f"oops one route expected, got {rslt_dict}"

    # Clear the configuration
    res.cfg_end2(suba_id)  # clear scan cfg


# Array config for these tests
array_config_aa3 = {
    "stations": 307,
    "channels": 382,
    "subarrays": 16,
    "alveos_per_fsp": 8,
    "pss_beams": 44,
    "pst_beams": 4,
}


def test_sdp_two_layer_routing(arp_db):
    """
    Test that SDP routes can be calculated for a two-layer switch
    topology with alveo at layer 2, SDP at layer 1
    """

    # create sample connections list
    cnx, _ = create_cnx_list(aa3)  # don't need summary _
    n_alveo = 8  # enough alveos for one FSP

    res = Resources(array_config_small, cnx)
    # Register the available alveos similar to FSP registration
    for i in range(0, n_alveo):
        # serial#_N format used in create_cnx_list for fake serial numbers
        success, msg = res.alveo_registration(
            {
                "serial": f"serial#_{i+1}",
                "hw": "u55c",
                "tango_dev": None,
                "status": 1,
            }
        )
        assert success, msg

    # subarray 4 stns x 1 channel with 1 timing beam should fit
    stn_substn_list = [(stn, 0) for stn in range(1, 1 + 4)]  # all AA3 stations
    frq_list = [65 + i for i in range(0, 1)]  # No. coarse channels
    suba_id = 13
    scan_config = {
        "subarray_id": suba_id,
        "status": 1,
        "stations": {
            "stns": stn_substn_list,
            "stn_beams": [
                {
                    "beam_id": 11,
                    "freq_ids": frq_list,
                    "boresight_dly_poly": "some_url",
                },
            ],
        },
        "vis": {
            # "fsp": {"firmware": "vis", "fsp_ids": [1, 2, 3, 4]},
            "firmware": "vis",
            "stn_beams": [
                {
                    "stn_beam_id": 11,
                    "host": [[0, "192.168.0.1"]],
                    "port": [[0, 9000, 1]],
                    "integration_ms": 849,
                },
            ],
        },
    }
    (rslt, _) = res.cfg_scan2(scan_config)
    assert rslt is True, "config should succeed  beamformer chans"

    # add arp reply for host we don't want, and check it's really present
    arp_db.arp_update(
        "p4_06",
        """{"192.168.0.2": {"mac": "c:42:a1:9c:a2:1b", "port": "33/0"}}""",
    )
    assert arp_db.num_hosts() == 1, "expected only one host in arp replies"

    # See if arps can satisfy the routing required by the configured subarray
    # (shouldn't be able to since host is not one configuration wants)
    rslt_dict = res.get_sdp_routes(arp_db)
    assert isinstance(rslt_dict, dict)
    assert "p4_06" in rslt_dict, "switch should be in dict"
    rtes = rslt_dict["p4_01"]["routes"]
    assert len(rtes) == 0, f"oops no routes expected, got routes={rtes}"

    # add arp reply for host we do want, check both are present now
    arp_db.arp_update(
        "p4_06",
        """{"192.168.0.1": {"mac": "c:42:a1:9c:a2:1b", "port": "33/0"}}""",
    )
    assert arp_db.num_hosts() == 2, "expected two hosts in arp replies"

    # See if arp replies can satisfy the routing required by subarray
    # (should work since arp database has reply from host subarray wants)
    # Since alveo is on p4_01 and sdp is on p4_06, both of those switches
    # should have a route each
    rslt_dict = res.get_sdp_routes(arp_db)
    assert isinstance(rslt_dict, dict)
    assert "p4_06" in rslt_dict, "switch should be in dict"
    # expect one route from alveo connected to p4_01 (to p4_06)
    rtes2 = rslt_dict["p4_01"]["routes"]
    assert len(rtes2) == 1, f"oops one route expected, got {rtes2}"
    # expect another route from p4_06 to SDP host (arp-ed on 33/0)
    rtes6 = rslt_dict["p4_06"]["routes"]
    assert len(rtes6) == 1, f"oops one route expected, got {rtes6}"

    # Clear the configuration
    res.cfg_end2(suba_id)  # clear scan cfg
