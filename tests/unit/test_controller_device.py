# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""
test Controller using DeviceTestContext (or DeviceProxy)
"""
# pylint: disable=redefined-outer-name
import time

import pytest
import tango
from ska_tango_base.control_model import AdminMode
from tango import DevState
from tango.test_utils import DeviceTestContext

from ska_low_cbf.controller.controller_device import (
    LowCbfController,
    SearchBeamBandwidthMode,
)


@pytest.fixture
def controller(request):
    """
    Return a device test context or proxy to a Controller, depending on the
    --true-context command line argument.
    """
    true_context = request.config.getoption("--true-context")
    if not true_context:
        with DeviceTestContext(LowCbfController) as proxy:
            yield proxy
    else:
        proxy = tango.DeviceProxy("low-cbf/control/0")
        yield proxy


@pytest.mark.post_deployment
@pytest.mark.usefixtures("controller")
class TestController:
    """Tests for the Controller Device"""

    def test_init(self, controller):
        """Init Command"""
        controller.Init()

    # Test commented out until there is something more to test than
    # base-clase state transitions (which are tested in base classes)
    # def test_on(self, controller):
    #     """On Command"""
    #     controller.adminMode = AdminMode.ONLINE
    #     controller.On()
    #     assert controller.state() == DevState.ON

    # Test commented out until there is something more to test than
    # base-clase state transitions (which are tested in base classes)
    # def test_off(self, controller):
    #     """Off Command"""
    #     controller.On()
    #     controller.Off()
    #     assert controller.state() == DevState.OFF

    @pytest.mark.parametrize(
        "mode",
        [SearchBeamBandwidthMode.SINGLE, SearchBeamBandwidthMode.DOUBLE],
    )
    def test_sbbwm(self, controller, mode):
        """Search Beam Bandwidth Mode attribute"""
        controller.searchBeamBandwidthMode = mode
        assert controller.searchBeamBandwidthMode == mode

    # Test commented out until there is something more to test than
    # base-clase state transitions (which are tested in base classes)
    # def test_operating_state_mode(self, controller):
    #     """Apply different AdminMode values and check it reflects
    #     in device's operating state (DevState)
    #     updates
    #     """
    #     # A dictionary with AdminMode as a key ("represents user intent")
    #     # and the state the "Control System" should report as value
    #     # see https://developer.skao.int/projects/ska-control-model/en/latest/admin_mode.html
    #     mode_state_dict = {
    #         AdminMode.ONLINE: DevState.ON,
    #         AdminMode.MAINTENANCE: DevState.ON,
    #         AdminMode.OFFLINE: DevState.DISABLE,
    #         AdminMode.NOT_FITTED: DevState.DISABLE,
    #         AdminMode.RESERVED: DevState.DISABLE,
    #     }

    #     for adm_mode, state in mode_state_dict.items():
    #         controller.adminMode = adm_mode
    #         time.sleep(1)  # takes a bit of time to switch states
    #         assert controller.adminMode == adm_mode
    #         assert controller.state() == state
