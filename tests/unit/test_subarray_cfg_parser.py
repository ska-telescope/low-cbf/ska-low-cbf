# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import json

import pytest

from ska_low_cbf.allocator.subarray_config import SubarrayConfig

# first beam with a set of frequencies
bm_id_a = 1
freqs_a = [
    100,
    101,
    102,
]
# second beam with a different set of frequencies
bm_id_b = 3
freqs_b = [
    123,
    124,
    125,
]
#
stns = [[1, 1], [2, 3], [3, 7], [4, 13]]
two_beam_config = {
    "stations": {
        "stns": stns,
        "stn_beams": [
            {
                "beam_id": bm_id_a,
                "freq_ids": freqs_a,
                "boresight_dly_poly": "uri_a",
            },
            {
                "beam_id": bm_id_b,
                "freq_ids": freqs_b,
                "boresight_dly_poly": "uri_b",
            },
        ],
    },
}


@pytest.mark.parametrize(
    "config, frq_a, frq_b, bm_a, bm_b",
    [(two_beam_config, freqs_a, freqs_b, bm_id_a, bm_id_b)],
)
def test_cfg_two_beam_frequencies(config, frq_a, frq_b, bm_a, bm_b):
    """
    Check subarray configuration parser
    is able to parse station beams with different freqencies,
    correctly distinguishing the different sets of beam frequencies
    """

    subarray_cfg = SubarrayConfig(config)
    for f_id in subarray_cfg.beam_freqs(bm_a):
        assert f_id in frq_a, print(f"expected frequency {f_id} not in beam")
    for f_id in subarray_cfg.beam_freqs(bm_b):
        assert f_id in frq_b, print(f"expected frequency {f_id} not in beam")


@pytest.mark.parametrize(
    "config, stns",
    [(two_beam_config, stns)],
)
def test_cfg_station_ids(config, stns):
    """
    Check subarray config parser finds stations in the configuration
    """
    subarray_cfg = SubarrayConfig(config)
    cfg_stns = subarray_cfg.stations()
    assert len(cfg_stns) == len(stns), print("differing number of stations")
    for stn in cfg_stns:
        assert stn in stns, print(f"stn {stn} not in stations {cfg_stns}")


@pytest.mark.parametrize(
    "config, beam_ids",
    [(two_beam_config, (bm_id_a, bm_id_b))],
)
def test_cfg_beam_ids(config, beam_ids):
    """
    Check subarray config parser finds beamIDs in the configuration
    """
    subarray_cfg = SubarrayConfig(config)
    cfg_bmids = subarray_cfg.stn_beam_ids()
    assert len(cfg_bmids) == len(beam_ids), print(
        "differing number of beam_ids"
    )
    for beamid in cfg_bmids:
        assert beamid in beam_ids, print(f"beam {beamid} not in {cfg_bmids}")
