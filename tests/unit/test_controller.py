# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""
test Controller (core logic, not Tango device)
"""
# pylint: disable=no-self-use,redefined-outer-name,protected-access
import pytest
from ska_tango_base.control_model import HealthState, ObsMode

from ska_low_cbf.controller.controller import (
    Controller,
    SearchBeamBandwidthMode,
)


@pytest.fixture(scope="class")
def controller():
    """A Controller object with defaults useful for testing"""
    controller = Controller()
    controller._subarrays = {
        1: {"obsMode": ObsMode.IDLE},
        4: {"obsMode": ObsMode.IDLE},
        16: {"obsMode": ObsMode.IDLE},
    }
    controller._device_health = {
        "low-cbf/dummy/1": HealthState.OK,
        "low-cbf/dummy/2": HealthState.OK,
        "low-cbf/dummy/3": HealthState.OK,
        "low-cbf/dummy/4": HealthState.OK,
    }
    return controller


@pytest.mark.usefixtures("controller")
class TestSearchBeamBandwidthMode:
    """Tests relating to Search Beam Bandwidth Mode"""

    def test_initial_value(self, controller):
        """Check initial value is correct"""
        assert (
            controller.search_beam_bandwidth_mode
            == SearchBeamBandwidthMode.SINGLE
        )

    @pytest.mark.parametrize(
        "mode",
        [SearchBeamBandwidthMode.SINGLE, SearchBeamBandwidthMode.DOUBLE],
    )
    def test_change(self, controller, mode):
        """Check that changed mode propagates"""
        controller.search_beam_bandwidth_mode = mode
        assert controller.search_beam_bandwidth_mode == mode

    @pytest.mark.parametrize(
        "initial_mode, req_mode",
        [
            (SearchBeamBandwidthMode.SINGLE, SearchBeamBandwidthMode.DOUBLE),
            (SearchBeamBandwidthMode.DOUBLE, SearchBeamBandwidthMode.SINGLE),
        ],
    )
    def test_not_idle(self, controller, initial_mode, req_mode):
        """If any subarray is not idle, change of mode should be rejected"""
        controller.search_beam_bandwidth_mode = initial_mode
        controller._subarrays[4]["obsMode"] = ObsMode.IMAGING
        controller.search_beam_bandwidth_mode = req_mode
        assert controller.search_beam_bandwidth_mode == initial_mode

    def test_invalid_value(self, controller):
        """Attempt to write invalid value should be rejected"""
        invalid = max(SearchBeamBandwidthMode) + 1

        initial = controller.search_beam_bandwidth_mode
        controller.search_beam_bandwidth_mode = invalid
        assert controller.search_beam_bandwidth_mode == initial


@pytest.mark.usefixtures("controller")
class TestHealthState:
    """Tests relating to HealthState"""

    def test_initial_value(self, controller):
        """
        simulates a controller that hasn't received any device health attribute
        updates
        """
        controller._device_health = {}
        assert controller.health_state == HealthState.UNKNOWN

    @pytest.mark.parametrize(
        "device_health_mod, result",
        [
            ({}, HealthState.OK),
            ({"low-cbf/dummy/5": HealthState.DEGRADED}, HealthState.DEGRADED),
            ({"low-cbf/dummy/6": HealthState.FAILED}, HealthState.FAILED),
            ({"low-cbf/dummy/7": HealthState.UNKNOWN}, HealthState.UNKNOWN),
        ],
    )
    def test_health_states(self, controller, device_health_mod, result):
        """
        Modify device health dictionary with test parameter, then check for
        expected result. Note that _device_health has an initial value from the
        controller fixture.
        """
        controller._device_health.update(device_health_mod)
        assert controller.health_state == result
