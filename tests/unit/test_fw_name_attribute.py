# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import json

import pytest
from sw_cnx import aa3, create_cnx_list, marsfield_psi

from ska_low_cbf.allocator.resources import Resources

# Array config for these tests
array_config_psi = {
    "stations": 6,
    "channels": 96,
    "subarrays": 16,
    "alveos_per_fsp": 1,
    "pss_beams": 44,
    "pst_beams": 4,
}


def test_configure():
    """Test Allocator's firmware_name attribute functionality"""
    # CFG 1: create sample connections list
    cnx, _ = create_cnx_list(marsfield_psi)  # don't need summary _
    n_sw_psi = 1
    n_alv_psi = 8

    # CFG 2: Create a resources object under test
    res = Resources(array_config_psi, cnx)
    # Register the available alveos similar to FSP registration
    for i in range(0, n_alv_psi):
        # serial#_N format used in create_cnx_list for fake serial numbers
        success, msg = res.alveo_registration(
            {
                "serial": f"serial#_{i+1}",
                "hw": "u55c",
                "tango_dev": None,
                "status": 1,
            }
        )
        assert success, msg

    # 1. check number of switches found
    assert len(res.switch_ids) == n_sw_psi, "expected {} P4s, got {}".format(
        n_sw_psi, len(res.switch_ids)
    )

    # CFG 3: check total number of alveos found (only l2 sw will have any,
    # but overall sum should be ok)
    total_alveo = 0
    for id in res.switch_ids:
        alv = res.get_alveo_ids(id)
        total_alveo += len(alv)
    assert (
        total_alveo == n_alv_psi
    ), f"expected {n_alv_psi} alveos, got {total_alveo}"

    # CFG 4: All subarrays resourced with shared access to all P4 switches
    # removed 2023-05-11 P4 switches now always shared

    # CFG 5: All subarrays resourced with shared access to 2 PST B/F FSPs
    #      providing only 96 PST coarse channels
    # CFG 6: All subarrays resourced with shared access to 2 Correlator FSPs
    #      providing only 96 PST coarse channels
    # removed 2023-05-11 FSPs now allocated during ConfigureScan

    suba_id = 13
    scan_config = {
        "subarray_id": suba_id,
        "status": 1,
        "stations": {
            "stns": [(1, 0), (2, 0), (3, 0), (4, 0)],
            "stn_beams": [
                {
                    "beam_id": 11,
                    "freq_ids": [65],
                    "delay_poly": "some_url",
                },
            ],
        },
        "vis": {
            # "fsp": {"firmware": "vis", "fsp_ids": vis_fsps},
            "firmware": "vis",
            "stn_beams": [
                {
                    "stn_beam_id": 11,
                    "host": [[0, 192, 168, 0.1]],
                    "port": [[0, 9000, 1]],
                    "integration_ms": 849,
                },
            ],
        },
        "timing_beams": {
            # "fsp": {"firmware": "pst", "fsp_ids": pst_fsps},
            "firmware": "pst",
            "beams": [
                {
                    "pst_beam_id": 1,
                    "stn_beam_id": 11,
                    "delay_poly": "url",
                    "jones": "url",
                    "destinations": [
                        {
                            "data_host": "10.0.3.2",
                            "data_port": 9000,
                            "start_channel": 0,
                            "num_channels": 16,
                        },
                        {
                            "data_host": "10.0.3.3",
                            "data_port": 9000,
                            "start_channel": 16,
                            "num_channels": 200,
                        },
                    ],
                },
            ],
        },
    }
    (rslt, msg) = res.cfg_scan2(scan_config)
    assert (
        rslt is True
    ), "config should succeed with PST+Vis because there are enough alveo"
    # End of test setup, now test fw_names functionality implemented
    # in the allocator

    # Test 1. Get firmwarenames and check personalitys all present and correct
    fw_names = res.fw_names_with_test_overrides
    # a. All FSPs have an entry
    assert (
        len(fw_names) == n_alv_psi
    ), f"expected {n_alv_psi} fw names, one per Alveo, got {len(fw_names)}"
    # b. Correct number of Correlator FSPs
    n_vis_found = sum([1 for _ in fw_names.values() if _ == "vis"])
    assert (
        n_vis_found > 0
    ), f"Expected at least one vis personality, got {n_vis_found}"
    # c. Correct number of PST FSPs
    n_pst_found = sum([1 for _ in fw_names.values() if _ == "pst"])
    assert (
        n_pst_found > 0
    ), f"Expected at least one vis personality, got {n_pst_found}"
