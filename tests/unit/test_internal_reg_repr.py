# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import json

import pytest
from sw_cnx import create_cnx_list, marsfield_psi

from ska_low_cbf.allocator.resources import Resources

# Array config for these tests
array_config_psi = {
    "stations": 6,
    "channels": 96,
    "subarrays": 16,
    "alveos_per_fsp": 1,
    "pss_beams": 44,
    "pst_beams": 4,
}


def test_routing_entries_present():
    """
    Test Allocator's ability to generate correct number of SPS route entries
    (for p4 switches) when configured to calculate visibilities and PST beams
    """
    # CFG 1: create sample connections list
    cnx, _ = create_cnx_list(marsfield_psi)  # don't need summary _
    n_sw_psi = 1
    n_alv_psi = 8

    with open("build/psi_cnx.txt", "w", encoding="ascii") as f:
        json.dump(cnx, f, indent=2)

    # CFG 2: Create a resources object under test
    res = Resources(array_config_psi, cnx)
    # Register the available alveos similar to FSP registration
    for i in range(0, n_alv_psi):
        # serial#_N format used in create_cnx_list for fake serial numbers
        success, msg = res.alveo_registration(
            {
                "serial": f"serial#_{i+1}",
                "hw": "u55c",
                "tango_dev": None,
                "status": 1,
            }
        )
        assert success, msg

    # 1. check number of switches found
    assert (
        len(res.switch_ids) == n_sw_psi
    ), f"expected {n_sw_psi} P4s, got {len(res.switch_ids)}"

    # CFG 3: check total number of alveos found (only l2 sw will have any,
    # but overall sum should be ok)
    total_alveo = 0
    for alveo_id in res.switch_ids:
        alv = res.get_alveo_ids(alveo_id)
        total_alveo += len(alv)
    assert (
        total_alveo == n_alv_psi
    ), f"expected {n_alv_psi} alveos, got {total_alveo}"

    # CFG 4: All subarrays resourced with shared access to all P4 switches
    # for subarray in range(1, 1 + 16):
    # removed 2023-05-11 switches always shared

    # CFG 5: All subarrays resourced with shared access to 2 PST B/F FSPs
    #      providing only 96 PST coarse channels
    # removed 2023-05-11 PST FSP assignment now done in ConfigureScan

    # CFG 6: All subarrays resourced with shared access to 2 Correlator FSPs
    #      providing only 96 PST coarse channels
    # removed 2023-05-11 VIS FSP assignment now done in ConfigureScan

    # CFG 8: Configure subarray up 6stn*96chans with
    #  1 timing beam + VIS should fit
    my_stns = 6  # between 1..6 stations for Marsfield PSI
    my_chans = 96  # between 1..96 coarse channels for Marsfield PSI
    stn_substn_list = [(stn, 0) for stn in range(1, 1 + my_stns)]
    frq_list = [65 + i for i in range(0, my_chans)]  # No. coarse channels
    suba_id = 13
    scan_config = {
        "subarray_id": suba_id,
        "status": 1,
        "stations": {
            "stns": stn_substn_list,
            "stn_beams": [
                {
                    "beam_id": 11,
                    "freq_ids": frq_list,
                    "boresight_dly_poly": "some_url",
                },
            ],
        },
        "vis": {
            # "fsp": {"firmware": "vis", "fsp_ids": [3, 4]},
            "firmware": "vis",
            "stn_beams": [
                {
                    "stn_beam_id": 11,
                    "host": [[0, 192, 168, 0.1]],
                    "port": [[0, 9000, 1]],
                    "integration_ms": 849,
                },
            ],
        },
        "timing_beams": {
            # "fsp": {"firmware": "pst", "fsp_ids": [1, 2]},
            "firmware": "pst",
            "beams": [
                {
                    "pst_beam_id": 1,
                    "stn_beam_id": 11,
                    "delay_poly": "url",
                    "jones": "url",
                    "destinations": [
                        {
                            "data_host": "10.0.3.2",
                            "data_port": 9000,
                            "start_channel": 0,
                            "num_channels": 16,
                        },
                        {
                            "data_host": "10.0.3.3",
                            "data_port": 9000,
                            "start_channel": 16,
                            "num_channels": 200,
                        },
                    ],
                },
            ],
        },
    }
    # End of test setup, now test ConfigureScan functionality implemented
    # in the allocator

    # Test 1. Configuration should succeed with the assigned resources
    (rslt, _) = res.cfg_scan2(scan_config)
    assert (
        rslt is True
    ), "config should succeed with PST+Vis because there are enough alveo"

    # Test 2. Should have 2 alveo in the internal register representation
    # (With this config, one used for VIS one for PST, two unused and not reported)
    internal_alveo_regs_repr = res.get_internal_repr()
    assert len(internal_alveo_regs_repr) == 2, "expected one reg-set per alveo"
