# Create list of connections for various array releases

single_alveo = {
    # Abbreviations used for each kind of link connection (A for alveo, etc)
    "dest_names": {
        "A": "alveo",
        "L": "stn",  # L for LFAA
        "S": "pss_beam",
        "T": "pst_beam",
        "V": "sdp",
    },
    # How each kind of port is split (links, gbps, endpoints per link)
    "port_param": {
        "A": (1, 100, 1),  # port is 1x100G with 1 endpoint
        "L": (1, 100, 8),  # port is 1x100G with 8 endpoints
        "S": (4, 25, 3),  # port is 4x25G with 3 endpoint each
        "T": (4, 25, 1),  # port is 4x25G with 1 endpoint each
        "V": (1, 100, 1),  # port is 1x100G with 1 endpoint
    },
    # How ports of the 64-port P4 switch are allocated to alveo, pss, pst, etc
    "p4": [
        # switch 1
        "A-------------LT"  # ports 1,3,5,7 .. 31
        "-------------VST"  # ports 2,4,6,8 .. 32
        "----------------"  # ports 33,35,37,39, .. 63
        "----------------",  # ports 34,36,38,40, .. 64
    ],
    # Total of each kind of endpoint at switch
    # 1 Alveo, 6 Stations, 9PSS+8PST+1VIS servers
    "n_ep": [
        {"A": 1, "L": 6, "S": 9, "T": 8, "V": 1},
    ],
}

# Recipe to create interconnections list for Marsfield PSI
marsfield_psi = {
    # Abbreviations used for each kind of link connection (A for alveo, etc)
    "dest_names": {
        "A": "alveo",
        "L": "stn",  # L for LFAA
        "S": "pss_beam",
        "T": "pst_beam",
        "V": "sdp",
    },
    # How each kind of port is split (links, gbps, endpoints per link)
    "port_param": {
        "A": (1, 100, 1),  # port is 1x100G with 1 endpoint
        "L": (1, 100, 8),  # port is 1x100G with 8 endpoints
        "S": (4, 25, 3),  # port is 4x25G with 3 endpoint each
        "T": (4, 25, 1),  # port is 4x25G with 1 endpoint each
        "V": (1, 100, 1),  # port is 1x100G with 1 endpoint
    },
    # How ports of the 64-port P4 switch are allocated to alveo, pss, pst, etc
    "p4": [
        # switch 1
        "AAAAAAAA------LT"  # ports 1,3,5,7 .. 31
        "-------------VST"  # ports 2,4,6,8 .. 32
        "----------------"  # ports 33,35,37,39, .. 63
        "----------------",  # ports 34,36,38,40, .. 64
    ],
    # Total of each kind of endpoint at switch
    # 8 Alveo, 6 Stations, 9PSS+8PST+1VIS servers
    "n_ep": [
        {"A": 8, "L": 6, "S": 9, "T": 8, "V": 1},
    ],
}

# Recipe to create interconnections list for AA3
aa3 = {
    # Abbreviations used for each kind of link connection (A for alveo, etc)
    "dest_names": {
        "A": "alveo",
        "L": "stn",
        "S": "pss",
        "T": "pst",
        "V": "sdp",
    },
    # How each kind of port is split (links, gbps, endpoints per link)
    "port_param": {
        "A": (1, 100, 1),  # port is 1x100G with 1 endpoint
        "L": (1, 100, 8),  # port is 1x100G with 8 endpoints
        "S": (4, 25, 3),  # port is 4x25G with 3 endpoint each
        "T": (4, 25, 1),  # port is 4x25G with 1 endpoint each
        "V": (1, 100, 1),  # port is 1x100G with 1 endpoint
    },
    # How ports of the 64-port P4 switch are allocated to alveo, pss, pst, etc
    "p4": [
        # Switch 1
        "AAAAAAAAAAAAAAAA"  # ports 1,3,5,7 .. 31
        "------AAAA---III"  # ports 2,4,6,8 .. 32
        "------AAAA---III"  # ports 33,35,37,39, .. 63
        "AAAAAAAAAAAAAAAA",  # ports 34,36,38,40, .. 64
        # Switch 2
        "AAAAAAAAAAAAAAAA"  # ports 1,3,5,7 .. 31
        "------AAAA---III"  # ports 2,4,6,8 .. 32
        "------AAAA---III"  # ports 33,35,37,39, .. 63
        "AAAAAAAAAAAAAAAA",  # ports 34,36,38,40, .. 64
        # Switch 3
        "AAAAAAAAAAAAAAAA"  # ports 1,3,5,7 .. 31
        "------AAAA---III"  # ports 2,4,6,8 .. 32
        "------AAAA---III"  # ports 33,35,37,39, .. 63
        "AAAAAAAAAAAAAAAA",  # ports 34,36,38,40, .. 64
        # switch 4
        "AAAAAAAAAAAAAAAA"  # ports 1,3,5,7 .. 31
        "------AAAA---III"  # ports 2,4,6,8 .. 32
        "------AAAA---III"  # ports 33,35,37,39, .. 63
        "AAAAAAAAAAAAAAAA",  # ports 34,36,38,40, .. 64
        # switch 5
        "AAAAAAAAAAAAAAAA"  # ports 1,3,5,7 .. 31
        "------AAAA---III"  # ports 2,4,6,8 .. 32
        "------AAAA---III"  # ports 33,35,37,39, .. 63
        "AAAAAAAAAAAAAAAA",  # ports 34,36,38,40, .. 64
        # switch 6
        "LLLLLLLLLLLLL---"  # ports 1,3,5,7 .. 31
        "TT---------IIIII"  # ports 2,4,6,8 .. 32
        "SSSSSSSSS--IIIII"  # ports 33,35,37,39, .. 63
        "VVVVVVVVV-------",  # ports 34,36,38,40, .. 64
        # switch 7
        "LLLLLLLLLLLLL---"  # ports 1,3,5,7 .. 31
        "TT---------IIIII"  # ports 2,4,6,8 .. 32
        "SSSSSSSSS--IIIII"  # ports 33,35,37,39, .. 63
        "VVVVVVVVV-------",  # ports 34,36,38,40, .. 64
        # switch 8
        "LLLLLLLLLLLLL---"  # ports 1,3,5,7 .. 31
        "T----------IIIII"  # ports 2,4,6,8 .. 32
        "SSSSSSSSS--IIIII"  # ports 33,35,37,39, .. 63
        "VVVVVVVV--------",  # ports 34,36,38,40, .. 64
    ],
    # Total of each kind of endpoint at each switch
    "n_ep": [
        {"A": 40, "L": 0, "S": 0, "T": 0, "V": 0},
        {"A": 40, "L": 0, "S": 0, "T": 0, "V": 0},
        {"A": 40, "L": 0, "S": 0, "T": 0, "V": 0},
        {"A": 40, "L": 0, "S": 0, "T": 0, "V": 0},
        {"A": 40, "L": 0, "S": 0, "T": 0, "V": 0},
        {"A": 0, "L": 102, "S": 83, "T": 6, "V": 9},
        {"A": 0, "L": 102, "S": 83, "T": 6, "V": 9},
        {"A": 0, "L": 103, "S": 83, "T": 4, "V": 8},
    ],
    # Two links between L1 and L2 switches at 100G
    "inter_layer_links": [2, 100],
}
# Translation between index of chars in "p4" string above and P4 port no.
str_idx_2_port_num = [i + 1 for i in range(0, 32, 2)]
str_idx_2_port_num.extend([i + 2 for i in range(0, 32, 2)])
str_idx_2_port_num.extend([i + 33 for i in range(0, 32, 2)])
str_idx_2_port_num.extend([i + 34 for i in range(0, 32, 2)])


def create_cnx_list(cfg):
    """Create list of connections from recipe passed as argument"""

    assert len(cfg["p4"]) == len(cfg["n_ep"]), (
        "number of switches"
        " in recipe should match number of endpoint descriptions"
    )

    all_cnx = []
    summary = []
    n_sw = len(cfg["p4"])
    next_serial_no = 1

    # Create lists of Layer-1 (L1) and Layer-2 (L2) switches.
    # L1 switches, are defined here as thos switches (if any) that
    # have no alveos attached, all other switches are L2
    l1_sw = []
    l2_sw = []
    for i in range(0, n_sw):
        if cfg["n_ep"][i]["A"] == 0:
            l1_sw.append(i)
        else:
            l2_sw.append(i)

    # if we have L1 & L2, pull out lists of each switches interconnection ports
    if len(l1_sw) > 0:
        # create list of interconnection ports
        sw_iport = [[] for i in range(0, n_sw)]
        for i in range(0, n_sw):
            sw_txt = cfg["p4"][i]
            for idx, ch in enumerate(sw_txt):
                if ch == "I":
                    sw_iport[i].append(str_idx_2_port_num[idx])
        # check there are enough inter-switch link ports
        for idx, iport_list in enumerate(sw_iport):
            n_iport = len(iport_list)
            if idx in l1_sw:
                required = len(l2_sw) * cfg["inter_layer_links"][0]
            else:
                required = len(l1_sw) * cfg["inter_layer_links"][0]

            assert n_iport >= required, "not enough inter-switch link ports"

    pst_hi = True
    tl_dest = {"A": 0, "L": 0, "S": 0, "T": 0, "V": 0}
    for i in range(0, n_sw):
        sw_txt = cfg["p4"][i]
        n_ep = {"A": 0, "L": 0, "S": 0, "T": 0, "V": 0}
        link_ep = {"A": 0, "L": 0, "S": 0, "T": 0, "V": 0}
        lane_idx = {"A": 0, "L": 0, "S": 0, "T": 0, "V": 0}
        for idx, ch in enumerate(sw_txt):
            sw_port = str_idx_2_port_num[idx]
            if ch in "ALSTV":
                while n_ep[ch] < cfg["n_ep"][i][ch]:
                    switch = "p4_{:02d}".format(i + 1)
                    port = "{}/{}".format(sw_port, lane_idx[ch])
                    if ch == "T":
                        if pst_hi:
                            hilo_suffix = "hi"
                        else:
                            hilo_suffix = "lo"
                        pst_hi = not pst_hi
                        dest = "{}_{:03}_{}".format(
                            cfg["dest_names"][ch],
                            tl_dest[ch] // 2 + 1,
                            hilo_suffix,
                        )
                    else:
                        dest = "{}_{:03}".format(
                            cfg["dest_names"][ch], tl_dest[ch] + 1
                        )
                    speed = "{}".format(cfg["port_param"][ch][1])
                    summary.append(
                        "{} {} <-> {} ({}G)".format(switch, port, dest, speed)
                    )

                    c = {
                        "switch": switch,
                        "port": port,
                        "speed": speed,
                    }
                    if ch == "A":
                        c["alveo"] = f"serial#_{next_serial_no}"
                        next_serial_no += 1
                    else:
                        c["link"] = dest
                    all_cnx.append(c)

                    tl_dest[ch] += 1
                    n_ep[ch] += 1
                    link_ep[ch] += 1
                    if link_ep[ch] >= cfg["port_param"][ch][2]:
                        link_ep[ch] = 0
                        lane_idx[ch] += 1
                        if lane_idx[ch] >= cfg["port_param"][ch][0]:
                            # used all the lanes available for port
                            lane_idx[ch] = 0
                            break

            pass  # TODO other cases??

        # Check that we achieved all required alveo, pst, pss, vis, cnx
        for key in cfg["n_ep"][i]:
            if n_ep[key] != cfg["n_ep"][i][key]:
                print(
                    "WARNING: Only {} of {} {} endpoints possible"
                    " for switch p4_{:02}".format(
                        n_ep[key],
                        cfg["n_ep"][i][key],
                        cfg["dest_names"][key],
                        i + 1,
                    )
                )

    # if we have a two-layer structure creat links between L1 & L2 switches
    if len(l1_sw) != 0:
        sw_iport_idx = [0 for _ in sw_iport]
        for sw1 in l1_sw:
            for sw2 in l2_sw:
                for lnk in range(0, cfg["inter_layer_links"][0]):
                    switch1 = "p4_{:02d}".format(sw1 + 1)
                    port1 = "{:02d}/0".format(sw_iport[sw1][sw_iport_idx[sw1]])
                    sw_iport_idx[sw1] += 1
                    switch2 = "p4_{:02d}".format(sw2 + 1)
                    port2 = "{:02d}/0".format(sw_iport[sw2][sw_iport_idx[sw2]])
                    sw_iport_idx[sw2] += 1
                    summary.append(
                        "{} {} <-> {} {} ({}G)".format(
                            switch1,
                            port1,
                            switch2,
                            port2,
                            cfg["inter_layer_links"][1],
                        )
                    )

                    c = {
                        "switch": switch1,
                        "port": port1,
                        "speed": cfg["inter_layer_links"][1],
                        "switch2": switch2,
                        "port2": port2,
                    }
                    all_cnx.append(c)
    return all_cnx, summary


if __name__ == "__main__":
    cnx, summary = create_cnx_list(aa3)
    for line in summary:
        print(line)
