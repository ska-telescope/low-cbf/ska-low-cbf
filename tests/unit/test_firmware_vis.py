# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import pytest

from ska_low_cbf.allocator.firmware_vis import FirmwareVis
from ska_low_cbf.allocator.resources import ALVEO_LIMIT_DEFAULTS


@pytest.mark.parametrize(
    "num_chans, num_stns, chan_spacing",
    [
        # Cases with few coarse channels, many stations to correlate
        (1, 512, 1),
        (2, 512, 1),
        (3, 300, 1),
        (5, 200, 1),
        (6, 168, 1),
        (7, 144, 1),
        # Cases with few stations to correlate, but many coarse channels
        (256, 2, 1),
        (256, 3, 1),
        (256, 4, 1),
        (128, 5, 1),
        (128, 6, 1),
        # Cases with non-contiguous channels
        (256, 2, 2),
        (128, 6, 2),
    ],
)
def test_correlator_good_sizes(num_chans, num_stns, chan_spacing):
    """
    Check class representing Correlator successfully allocates largest
    known-good subarray sizes (for a bunch of equal sized subarrays)
    """

    fw_repr = FirmwareVis(ALVEO_LIMIT_DEFAULTS)
    stn_ids = [(i, 0) for i in range(0, num_stns)]
    for idx in range(0, num_chans):
        freq_id = idx * chan_spacing
        rslt = fw_repr.vis_reserve(stn_ids, 0, 0, freq_id, True)
        if idx == (num_chans - 1):
            assert (
                rslt is True
            ), f"{num_chans} coarse {num_stns} stns should be OK"


@pytest.mark.parametrize(
    "num_chans, num_stns",
    [
        (256, 2),
    ],
)
def test_good_but_reversed_freqs(num_chans, num_stns):
    """
    Check class representing Correlator properly fails to allocate smallest
    known-bad subarray sizes (for a bunch of equal sized subarrays)
    """

    fw_repr = FirmwareVis(ALVEO_LIMIT_DEFAULTS)
    stn_ids = [(i, 0) for i in range(0, num_stns)]
    for freq_id in reversed(range(0, num_chans)):
        rslt = fw_repr.vis_reserve(stn_ids, 0, 0, freq_id, True)
        if freq_id == (num_chans - 1):
            assert rslt is True, "reversed freq order should also be OK"


@pytest.mark.parametrize(
    "num_chans, num_stns, chan_spacing",
    [
        # Cases with few coarse channels, many stations to correlate
        (1, 513, 1),  # Too much workload for any MatrixCorrelator
        (2, 513, 1),  # Too many VirtualChannels
        (3, 301, 1),  # Two chans to one MxC need too much CT2 mem
        (5, 201, 1),  # Too much CT2 mem used
        (6, 169, 1),  # Too many vchans
        (7, 145, 1),  # Too many vchans
        # Cases with few stations to correlate, but many coarse channels
        (257, 2, 1),  # 2 stns becomes a group of 4 stns (see case below)
        (257, 3, 1),  # 3 stns becomes a group of 4 stns (see case below)
        (257, 4, 1),  # 4stns x 257channels is too many VirtualChannels
        (129, 5, 1),  # 5 stations is grouped as 8 stns, Too many VirtChannels
        (129, 6, 1),  # 6 stations becomes 8 grouped. Too many virtualchannels
        # Cases with few stations, but many non-contiguous channels
        (257, 2, 2),  # 2 stns becomes a group of 4 stns (see case below)
        (129, 6, 2),  # 6 stations becomes 8 grouped. Too many virtualchannels
    ],
)
def test_correlator_bad_sizes(num_chans, num_stns, chan_spacing):
    """
    Check class representing Correlator properly fails to allocate smallest
    known-bad subarray sizes (for a bunch of equal sized subarrays)
    """

    fw_repr = FirmwareVis(ALVEO_LIMIT_DEFAULTS)
    stn_ids = [(i, 0) for i in range(0, num_stns)]
    for idx in range(0, num_chans):
        freq_id = idx * chan_spacing
        rslt = fw_repr.vis_reserve(stn_ids, 0, 0, freq_id, True)
        if idx == (num_chans - 1):
            assert (
                rslt is False
            ), f"{num_chans} coarse {num_stns} stns should not be OK"


@pytest.mark.parametrize(
    "channel_list, expected_blks",
    [
        ([100, 101, 102], 1),  # 3 contiguous chans -> 1 block
        ([100, 192], 2),  # two discontiguous chans ->  2 blocks
        ([100, 101, 200, 201], 2),  # two lots of 2 contig chans -> 2 blocks
        ([100, 102, 115], 3),  # 3 discontiguous chans -> 3 blocks
    ],
)
def test_discontiguous_channels(channel_list, expected_blks):
    """
    Check that frequency lists with discontiguous channels are handled properly
    """
    fw_repr = FirmwareVis(ALVEO_LIMIT_DEFAULTS)
    stn_ids = [1, 2, 3, 4, 5, 6]
    for freq_id in channel_list:
        rslt = fw_repr.vis_reserve(stn_ids, 0, 0, freq_id, True)
        assert rslt is True, "Expected all channels to fit"

    # reservations should exist, then be accepted and no longer exist
    assert fw_repr.has_reservation() is True
    fw_repr.accept_reservation()
    assert fw_repr.has_reservation() is False

    # extract data that includes channel blocks, check number of blocks
    internal_alveo = fw_repr.get_internal_regs()
    num_chanl_blks = len(internal_alveo[0]["sa_bm"])
    assert (
        num_chanl_blks == expected_blks
    ), f"expected {expected_blks} channel blocks, got {num_chanl_blks}"


ALVEO_LIMITS_LOW_VCH = {
    "vis": {
        "vch": 16,  # Lowered Max Virtual Channels into FPGA
        "hbm": 606,  # Max VChans in one matrix correlator's (MxC) 2Bbyte HBM
        "bli": (512 * 513) // 2,  # Max baselines in one MxC
    },
}


@pytest.mark.parametrize(
    "num_chans, num_stns, expected_result",
    [
        # chans, stns, expected alloc result (stns in groups of 4)
        (5, 4, False),  # 5*4=20vch is beyond vch limit
        (4, 4, True),
    ],
)
def test_lowered_vch_limit(num_chans, num_stns, expected_result):
    """Test resource limiting mechanism"""
    fw_repr = FirmwareVis(ALVEO_LIMITS_LOW_VCH)
    stn_ids = [(i, i) for i in range(0, num_stns)]

    for freq_id in range(0, num_chans):
        rslt = fw_repr.vis_reserve(stn_ids, 1, 1, freq_id, True)

    assert rslt == expected_result, "Failed to lower VCH limit"


ALVEO_LIMITS_LOW_HBM = {
    "vis": {
        "vch": 1024,  # Lowered Max Virtual Channels into FPGA
        "hbm": 16,  # Max VChans in one matrix correlator's (MxC) 2Bbyte HBM
        "bli": (512 * 513) // 2,  # Max baselines in one MxC
    },
}


@pytest.mark.parametrize(
    "num_chans, num_stns, expected_result",
    [
        # chans, stns, expected alloc result (stns in groups of 4)
        (2 * 5, 4, False),  # 5*4=20vch is beyond hbm limit (2 MxCs)
        (2 * 4, 4, True),
    ],
)
def test_lowered_hbm_limit(num_chans, num_stns, expected_result):
    """Test resource limiting mechanism"""
    fw_repr = FirmwareVis(ALVEO_LIMITS_LOW_HBM)
    stn_ids = [(i, i) for i in range(0, num_stns)]

    for freq_id in range(0, num_chans):
        rslt = fw_repr.vis_reserve(stn_ids, 1, 1, freq_id, True)

    assert rslt == expected_result, "Failed to lower HBM limit"


ALVEO_LIMITS_LOW_BLI = {
    "vis": {
        "vch": 1024,  # Lowered Max Virtual Channels into FPGA
        "hbm": 606,  # Max VChans in one matrix correlator's (MxC) 2Bbyte HBM
        "bli": (8 * 9) // 2,  # Max baselines in one MxC
    },
}


@pytest.mark.parametrize(
    "num_chans, num_stns, expected_result",
    [
        # chans, stns, expected alloc result (stns in groups of 4)
        (1, 9, False),  # 9stns = 9*10/2 bli is beyond hbm limit
        (1, 8, True),
    ],
)
def test_lowered_bli_limit(num_chans, num_stns, expected_result):
    """Test resource limiting mechanism"""
    fw_repr = FirmwareVis(ALVEO_LIMITS_LOW_BLI)
    stn_ids = [(i, i) for i in range(0, num_stns)]

    for freq_id in range(0, num_chans):
        rslt = fw_repr.vis_reserve(stn_ids, 1, 1, freq_id, True)

    assert rslt == expected_result, "Failed to lower baseline limit"


@pytest.mark.parametrize(
    "sa_id, stn_bm_id, n_stns",
    [
        # subarray_id, stn_beam_id, n_stations
        (1, 9, 8),
        (2, 10, 2),
        (3, 11, 256),
        (4, 14, 512),
        (5, 15, 600),
    ],
)
def test_capacity_estimator(sa_id, stn_bm_id, n_stns):
    """Test fpga channel capacity estimation matches fit"""
    fw_repr = FirmwareVis(ALVEO_LIMIT_DEFAULTS)
    stns = [(i, i) for i in range(0, n_stns)]

    # add in some initial capacity usage to make it more difficult
    fw_repr.vis_reserve(stns, sa_id + 1, 3, 100, False)
    fw_repr.accept_reservation()

    # How many channels of that size should fit?
    n_chan_expected = fw_repr.chan_capacity(n_stns)

    # The expected number of channels should succeed in fitting
    chans = [64 + i for i in range(0, n_chan_expected)]
    for ch in chans:
        rslt = fw_repr.vis_reserve(stns, sa_id, stn_bm_id, ch, False)
        assert rslt is True, f"chan {ch-64} of {n_chan_expected} didn't fit"

    # One more channel should fail to fit
    extra_ch = 64 + n_chan_expected
    rslt = fw_repr.vis_reserve(stns, sa_id, stn_bm_id, extra_ch, False)
    assert rslt is False, "unexpected extra channel fitted!"
