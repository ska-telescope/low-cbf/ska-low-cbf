# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""
Test LowCbfAllocator using DeviceTestContext
"""
# pylint: disable=no-self-use,redefined-outer-name
import json
from unittest.mock import patch

import pytest
import tango
from ska_tango_base.control_model import HealthState
from tango import DevState
from tango.test_utils import DeviceTestContext

from ska_low_cbf.allocator.allocator_device import LowCbfAllocator


@pytest.fixture
def allocator(request):
    """
    Get either a DeviceTestContext or a DeviceProxy, depending on the state of
    the true-context command-line argument.
    """
    true_context = request.config.getoption("--true-context")
    if not true_context:
        with DeviceTestContext(LowCbfAllocator) as proxy:
            yield proxy
    else:
        proxy = tango.DeviceProxy("low-cbf/allocator/0")
        yield proxy


@pytest.mark.post_deployment
@pytest.mark.usefixtures("allocator")
class TestAllocatorBasics:
    """Test the basic Tango interfaces"""

    def test_init(self, allocator):
        """Init command"""
        allocator.Init()

    #     assert allocator.state() == DevState.DISABLE
    #
    # def test_on(self, allocator):
    #     """On command"""
    #     allocator.On()
    #     assert allocator.state() == DevState.ON
    #
    # def test_off(self, allocator):
    #     """Off command"""
    #     allocator.On()
    #     allocator.Off()
    #     assert allocator.state() == DevState.OFF
    #
    # def test_health(self, allocator):
    #     """Health State"""
    #     assert allocator.healthState == HealthState.OK

    # 2023-05-11 allocation command checks removed since
    # Low.CBF subarrays no longer have AssignResources argument

    @pytest.mark.parametrize(
        "attribute",
        [
            "allocationVersionCounter",
            "connectorIDs",
            "connectorUpdate",
            "processorIDs",
            "processorUpdate",
        ],
    )
    def test_attributes_exist(self, allocator, attribute):
        """Expected attributes exist"""
        assert attribute in allocator.get_attribute_list()
