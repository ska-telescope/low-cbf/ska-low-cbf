# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2025 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

# import pytest

from ska_low_cbf.allocator.estimation_tool import alveo_est_tool


def make_stn_beam_cfg(beam_chans):
    beams = []
    for bm_id, n_chan in beam_chans.items():
        bm_spec = {
            "beam_id": bm_id,
            "freq_ids": [x + 64 for x in range(0, n_chan)],
            "delay_poly": "n/a",
        }
        beams.append(bm_spec)
    return beams


def make_vis_stn_beams_cfg(beam_chans):
    beams = []
    for bm_id, n_chan in beam_chans.items():
        bm_spec = {
            "stn_beam_id": bm_id,
            "host": [[0, f"vis_{bm_id}.x.x.x"]],
            "port": [0, 0, 0],
            "integration_ms": 850,
        }
        beams.append(bm_spec)
    return beams


def make_pst_bms_cfg(pst_bms):
    beams = []
    for stn_bm_id, pst_bm_list in pst_bms.items():
        for pst_bm_id in pst_bm_list:
            cfg = {
                "pst_beam_id": pst_bm_id,
                "stn_beam_id": stn_bm_id,
                "delay_poly": "pst_poly_sub n/a",
                "jones": "jones_sub n/a",
                "destinations": [
                    {
                        "data_host": f"pst_{pst_bm_id}.x.x.x",
                        "data_port": 10000,
                        "start_channel": 0,
                        "num_channels": 216,
                    }
                ],
                "stn_wts": [1.0],
            }
            beams.append(cfg)
    return beams


def make_pss_bms_cfg(pss_bms):
    beams = []
    for stn_bm_id, pss_bm_list in pss_bms.items():
        for pss_bm_id in pss_bm_list:
            cfg = {
                "pss_beam_id": pss_bm_id,
                "stn_beam_id": stn_bm_id,
                "delay_poly": "pst_poly_sub n/a",
                "jones": "jones_sub n/a",
                "destinations": [
                    {
                        "data_host": f"pss_{pss_bm_id}.x.x.x",
                        "data_port": 10000,
                        "start_channel": 0,
                        "num_channels": 54,
                    }
                ],
                "stn_wts": [1.0],
            }
            beams.append(cfg)
    return beams


def make_subarray_cfg(sub_id, n_stn, stn_bm_chans, pst_bms, pss_bms):
    stns = [[x + 1, 1] for x in range(0, n_stn)]
    subarray_cfg = {
        # Input to subarray from LFAA: stations, station_beams, frequencies
        "subarray_id": sub_id,
        "status": 1,
        "stations": {
            "stns": stns,
            "stn_beams": make_stn_beam_cfg(stn_bm_chans),
        },
        # Output to be calculated: visibilities
        "vis": {
            "firmware": "vis:x.y.z",
            "stn_beams": make_vis_stn_beams_cfg(stn_bm_chans),
        },  # standard Correlation parameter list TBD
    }
    if pst_bms is not None:
        subarray_cfg["timing_beams"] = {
            "firmware": "pst:x.y.z",
            "beams": make_pst_bms_cfg(pst_bms),
        }
    if pss_bms is not None:
        subarray_cfg["search_beams"] = {
            "firmware": "pss:x.y.z",
            "beams": make_pss_bms_cfg(pss_bms),
        }
    return subarray_cfg


def test_tool():
    """
    Test that estimation tool runs without problems
    """

    # Create a subarray configuration string suitable for resourcing test
    s_id = 1
    n_stn = 4
    stn_bm_chans = {1: 4, 2: 5}  # stn_bm_id: no_of_chans
    pst_bm_ids = {1: [1, 2, 3], 2: [4, 5]}  # stn_bm_id: [pst_bm_id, ..]
    pss_bm_ids = {
        1: [100, 200, 300],
        2: [400, 500],
    }  # stn_bm_id: [pss_bm_id, ..]
    subarray_cfg = make_subarray_cfg(
        s_id, n_stn, stn_bm_chans, pst_bm_ids, pss_bm_ids
    )

    # Run estimation tool
    n_alveo = alveo_est_tool(subarray_cfg)

    assert n_alveo != -1, "Bad subarray configuration provided"
    assert n_alveo > 0, "Configuration should require alveos, but 0 ret"
