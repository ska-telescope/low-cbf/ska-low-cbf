#!/usr/bin/python3

import json
import sys

# connection tuples have 3 fixed fields, and a variable number of fields that
# follow:
#   (switch_id, switch_port, link_speed, remainder...)
# remainder can be:
#   switch_id, switch_port
#   station1, station2, ...
#   pst_server_id
#   pss_server_id
#   sdp_server_id
#   alveo_id


def write_cnx_as_csv(filename: str, cnx: list):
    """write a list of connection tuples as a csv file"""
    with open(filename, "w") as f:
        f.write('"Switch", "Port", "Link Speed GBPS", "Connects to"\n')
        for itm in cnx:
            txt = "{}".format(itm[0])
            for i in range(1, len(itm)):
                txt = txt + ", {}".format(itm[i])
            f.write(txt + "\n")


def write_cnx_as_csv_by_switch(filename: str, switch: str, cnx: list):
    """
    write a list of connection tuples as a csv file, filtered by switch ID
    """
    with open(filename, "w") as f:
        f.write('"Switch", "Port", "Link Speed GBPS", "Connects to"\n')
        for itm in cnx:
            if itm[0] == switch:
                txt = "{}".format(itm[0])
                for i in range(1, len(itm)):
                    txt = txt + ", {}".format(itm[i])
                f.write(txt + "\n")
            # re-order output to be "nice" if this is switch-switch connection
            # and the one we want is at the far end
            elif itm[3] == switch:
                txt = "{}, {}, {}, {}, {}".format(
                    itm[3], itm[4], itm[2], itm[0], itm[1]
                )
                f.write(txt + "\n")


def cnx_list_to_dict(cnx: tuple):
    """turn connection tuple into key-value list ready for json output"""
    d = {}
    idx = cnx[0].rfind("_")
    # d["switch_layer"] = cnx[0][0:idx]
    d["switch"] = cnx[0][idx + 1 :]
    idx = cnx[1].rfind("_")
    d["port"] = int(cnx[1][idx + 1 :])
    d["speed"] = cnx[2]
    for i in range(3, len(cnx)):
        idx = cnx[i].rfind("_")
        name = cnx[i][0:idx]
        val = int(cnx[i][idx + 1 :])
        if name == "station":
            if "stations" not in d:
                d["stations"] = []
            d["stations"].append(val)
        elif name == "pst":
            if val % 2 == 1:
                txt = "pst_{:02d}_a".format((val + 1) // 2)
            else:
                txt = "pst_{:02d}_b".format((val + 1) // 2)
            d["pst_server_id"] = txt
        elif name == "pss":
            d["pss_server_id"] = val
        elif name == "sdp":
            d["sdp_server_id"] = val
        elif name == "port":
            d["port2"] = val
        elif name == "alveo":
            d["alveo"] = val
        else:
            # d["switch2_layer"] = name
            d["switch2"] = cnx[i][idx + 1 :]  # want name not int value
    return d


def write_cnx_as_json(filename: str, cnx: list):
    """write a list of connection tuples as a JSON file"""
    cnx_lod = []
    for itm in cnx:
        d = cnx_list_to_dict(itm)
        cnx_lod.append(d)
    a_dict = {}
    a_dict["connections"] = cnx_lod
    with open(filename, "w") as f:
        json.dump(a_dict, f, indent=4)


def create_cnx_list(
    n_l1_sw, n_l2_sw, n_alv, n_alv_per_sw, n_pst, n_pst_per_sw
):
    if n_l2_sw <= 0:
        print("ERROR Must have at least one layer-2 switch")
        return

    # The first free port number to be used for each switch
    l1_next_port = [1 for i in range(0, n_l1_sw)]
    l2_next_port = [1 for i in range(0, n_l2_sw)]

    # names for all the layer one switches
    l1_switch_id = [
        "p4_{:02d}".format(i + 1)  # names will be 1-based ie starting p4_01
        for i in range(0, n_l1_sw)
    ]
    # names for all the layer two switches
    l2_switch_id = [
        "p4_{:02d}".format(i + 1) for i in range(n_l1_sw, n_l1_sw + n_l2_sw)
    ]

    sw_icnx = []
    io_cnx = []
    pst_idx = 0
    # interconnections only exist if there are 1st layer switches
    if n_l1_sw > 0:
        for l1_idx in range(0, n_l1_sw):
            for l2_idx in range(0, n_l2_sw):
                sw1_name = l1_switch_id[l1_idx]
                sw2_name = l2_switch_id[l2_idx]
                # first link between l1 & l2 switch pair
                p1 = l1_next_port[l1_idx]
                p2 = l2_next_port[l2_idx]
                sw_icnx.append(
                    {
                        "switch": sw1_name,
                        "port": "{}/0".format(p1),
                        "speed": 100,
                        "switch2": sw2_name,
                        "port2": "{}/0".format(p2),
                    }
                )
                # second link between l1  & l2 switch pair
                p1 += 1
                p2 += 1
                sw_icnx.append(
                    {
                        "switch": sw1_name,
                        "port": "{}/0".format(p1),
                        "speed": 100,
                        "switch2": sw2_name,
                        "port2": "{}/0".format(p2),
                    }
                )
                l1_next_port[l1_idx] += 2
                l2_next_port[l2_idx] += 2

            # connect PST IO links at 25Gbits/sec 4 per switchport
            remaining_pst = n_pst - pst_idx
            if remaining_pst < n_pst_per_sw:
                n_pst_this_sw = remaining_pst
            else:
                n_pst_this_sw = n_pst_per_sw
            port_lane = 0
            for i in range(0, n_pst_this_sw):
                pst_id = "pst_{:02d}".format(pst_idx + 1)  # use 1-based
                sw_port = "{}/{}".format(l2_next_port[l2_idx], i % 4)
                sw_id = l2_switch_id[l2_idx]
                io_cnx.append(
                    {
                        "switch": sw_id,
                        "port": sw_port,
                        "speed": 25,
                        "link": pst_id,
                    }
                )
                pst_idx += 1
                port_lane += 1
                if port_lane > 3:
                    port_lane = 0
                    l2_next_port[l2_idx] += 1

    # alveos attached to L2 switches
    alv_cnx = []
    alveo_idx = 0

    # if no L1 switches, then PST attach to L2 switches
    n_l2_pst = 0
    if n_l1_sw == 0:
        n_l2_pst = n_pst

    for l2_idx in range(0, n_l2_sw):
        remaining_alv = n_alv - alveo_idx
        if remaining_alv > n_alv_per_sw:
            n_alv_this_sw = n_alv_per_sw
        else:
            n_alv_this_sw = remaining_alv

        # Add connections from L2 switches to Alveos
        for i in range(0, n_alv_this_sw):
            alv_id = "alveo_{:03d}".format(alveo_idx + 1)  # use 1-based
            alv_cnx.append(
                {
                    "switch": l2_switch_id[l2_idx],
                    "port": "{}/0".format(l2_next_port[l2_idx]),
                    "speed": 100,
                    "alveo": alv_id,
                }
            )
            l2_next_port[l2_idx] += 1
            alveo_idx += 1

        # if there were PST IO connected to Layer 2 switches then add them
        remaining_pst = n_l2_pst - pst_idx
        if remaining_pst < n_pst_per_sw:
            n_pst_this_sw = remaining_pst
        else:
            n_pst_this_sw = n_pst_per_sw

        port_lane = 0
        for i in range(0, n_pst_this_sw):
            pst_id = "pst_{:02d}".format(pst_idx + 1)  # use 1-based
            sw_port = "{}/{}".format(l2_next_port[l2_idx], i % 4)
            sw_id = l2_switch_id[l2_idx]
            io_cnx.append(
                {
                    "switch": sw_id,
                    "port": sw_port,
                    "speed": 25,
                    "link": pst_id,
                }
            )
            pst_idx += 1
            port_lane += 1
            if port_lane > 3:
                port_lane = 0
                l2_next_port[l2_idx] += 1

    all_cnx = [cnx for cnx in sw_icnx]
    all_cnx.extend(alv_cnx)
    all_cnx.extend(io_cnx)
    return all_cnx


def create_connections(create_csv, *param_vals):
    # unpack parameters, not expected order
    (
        layer1_p4switches,  # Number of P4 switches in first "I/O" switch layer
        layer2_p4switches,  # Num. of P4 switches in 2nd "Intermediate" layer
        n_alveos,  # Total number of Alveo cards
        alveos_per_switch,  # Alveos connected to 100G ports of each P4
        n_stations,  # Total number of LFAA stations
        stations_per_port,  # LFAA stations per 100G link
        n_psts,  # Number of PST server 25G connections
        pst_per_100G,  # PST connections use 100G port split as 4x 25G
        n_psss,  # Number of PSS server 25G connections
        pss_per_100G,  # PST connections use 100G port split as 4x 25G
        n_sdps,  # Number of SDP server 100G connections
    ) = param_vals

    # The first free port number to be used for each switch
    l1_next_port = [1 for i in range(0, layer1_p4switches)]
    l2_next_port = [1 for i in range(0, layer2_p4switches)]

    # names for all the layer one switches
    start_id = 1
    l1_switch_id = [
        "switch_{:02d}".format(i + start_id)
        for i in range(0, layer1_p4switches)
    ]
    # names for all the layer two switches
    start_id += layer1_p4switches
    l2_switch_id = [
        "switch_{:02d}".format(i + start_id)
        for i in range(0, layer2_p4switches)
    ]

    # if no first layer, layer2 doubles as layer 1,
    # and only 1 switch should exist
    if layer1_p4switches == 0:
        l1_switch_id = l2_switch_id
        l1_next_port = l2_next_port

    # Each xxxx_interconn variable is a list that starts with the same3 entries
    # but the fourth and later vary to describe stations, switches, or servers
    # [switch_name, switch_port, link_speed_GBPS, connects_to...]

    # Two 100G links between each L1 and L2 switch
    # Tuples describing links between the layer one and layer two switches:
    #  - (l1_switch_id, l1_switch_port, speed, l2_switch_id, l2_switch_port)
    switch_interconn = []
    # interconnections only exist if there are 1st layer switches
    if not layer1_p4switches == 0:
        for l1_idx in range(0, len(l1_switch_id)):
            for l2_idx in range(0, len(l2_switch_id)):
                sw1_name = l1_switch_id[l1_idx]
                sw2_name = l2_switch_id[l2_idx]
                p1 = l1_next_port[l1_idx]
                p2 = l2_next_port[l2_idx]

                # The first link between the switches
                p1_name = "port_{:02d}".format(p1)
                p2_name = "port_{:02d}".format(p2)
                t1 = (sw1_name, p1_name, 100, sw2_name, p2_name)
                switch_interconn.append(t1)
                # The second link between the switches
                p1_name = "port_{:02d}".format(p1 + 1)
                p2_name = "port_{:02d}".format(p2 + 1)
                t2 = (sw1_name, p1_name, 100, sw2_name, p2_name)
                switch_interconn.append(t2)

                l1_next_port[l1_idx] += 2
                l2_next_port[l2_idx] += 2

    # LFAA stations spread across L1 switches evenly as possible,
    # multiple stations/link
    #  - tuple: (switch_id, switch_port, speed, stations...)
    l1_sw_idx = 0
    stn = 1
    lfaa_interconn = []
    while stn <= n_stations:
        #  put a group of stations on the next 100G link
        sw_name = l1_switch_id[l1_sw_idx]
        port = l1_next_port[l1_sw_idx]
        port_name = "port_{:02d}".format(port)
        stations = [
            "station_{:03d}".format(i)
            for i in range(stn, stn + stations_per_port)
            if i <= n_stations
        ]
        t = (sw_name, port_name, 100, *stations)
        lfaa_interconn.append(t)

        l1_next_port[l1_sw_idx] += 1
        l1_sw_idx += 1
        if l1_sw_idx >= layer1_p4switches:
            l1_sw_idx = 0
        stn += stations_per_port

    # PST interconnections evenly spread across L1 switches
    #  - tuple: (switch_id, switch_port, speed, pst_server_id)
    l1_sw_idx = 0
    pst_interconn = []
    pst = 1
    while pst <= n_psts:
        sw_name = l1_switch_id[l1_sw_idx]
        port = l1_next_port[l1_sw_idx]

        for i in range(0, pst_per_100G):
            port_name = "port_{:02d}".format(port)
            pst_name = "pst_{:02d}".format(pst)
            t = (sw_name, port_name, 100 // pst_per_100G, pst_name)
            pst_interconn.append(t)
            l1_next_port[l1_sw_idx] += 1
            port = l1_next_port[l1_sw_idx]
            pst += 1
            if pst > n_psts:
                break

        l1_sw_idx += 1
        if l1_sw_idx >= layer1_p4switches:
            l1_sw_idx = 0

    # PSS interconnections evenly spread across L1 switches
    #  - tuple: (switch_id, switch_port, speed, pss_server_id)
    l1_sw_idx = 0
    pss_interconn = []
    pss = 1
    while pss <= n_psss:
        sw_name = l1_switch_id[l1_sw_idx]
        port = l1_next_port[l1_sw_idx]
        for i in range(0, pss_per_100G):
            port_name = "port_{:02d}".format(port)
            pss_name = "pss_{:02d}".format(pss)
            t = (sw_name, port_name, 100 // pss_per_100G, pss_name)
            pss_interconn.append(t)
            l1_next_port[l1_sw_idx] += 1
            port = l1_next_port[l1_sw_idx]
            pss += 1
            if pss > n_psss:
                break

        l1_sw_idx += 1
        if l1_sw_idx >= layer1_p4switches:
            l1_sw_idx = 0

    # SDP interconnections evenly spread across L1 switches
    #  - tuple: (switch_id, switch_port, speed, sdp_server_id)
    l1_sw_idx = 0
    sdp_interconn = []
    sdp = 1
    while sdp <= n_sdps:
        sw_name = l1_switch_id[l1_sw_idx]
        port = l1_next_port[l1_sw_idx]
        port_name = "port_{:02d}".format(port)
        sdp_name = "sdp_{:02d}".format(sdp)
        t = (sw_name, port_name, 100, sdp_name)
        sdp_interconn.append(t)

        l1_next_port[l1_sw_idx] += 1
        sdp += 1
        l1_sw_idx += 1
        if l1_sw_idx >= layer1_p4switches:
            l1_sw_idx = 0

    # alveo connections evenly spread across L2 switches
    #  - tuple: (switch_id, switch_port, speed, alveo_id)
    l2_sw_idx = 0
    alveo_interconn = []
    alveo = 1
    # start alveo assignment from first L2 switch
    while alveo <= n_alveos:
        sw_name = l2_switch_id[l2_sw_idx]
        port = l2_next_port[l2_sw_idx]
        port_name = "port_{:02d}".format(port)

        for i in range(0, alveos_per_switch):
            alveo_name = "alveo_{:03d}".format(alveo)
            t = (sw_name, port_name, 100, alveo_name)
            alveo_interconn.append(t)
            l2_next_port[l2_sw_idx] += 1
            port = l2_next_port[l2_sw_idx]
            port_name = "port_{:02d}".format(port)
            alveo += 1
            if alveo > n_alveos:
                break

        l2_sw_idx += 1
        if l2_sw_idx >= layer2_p4switches:
            l2_sw_idx = 0

    all_cnx = (
        switch_interconn
        + lfaa_interconn
        + pst_interconn
        + pss_interconn
        + sdp_interconn
        + alveo_interconn
    )
    write_cnx_as_json("cnx.json", all_cnx)

    if create_csv:
        # write all connections as csv file
        write_cnx_as_csv("cnx.csv", all_cnx)
        # write csv file for each connection type
        write_cnx_as_csv("cnx_switch.csv", switch_interconn)
        write_cnx_as_csv("cnx_lfaa.csv", lfaa_interconn)
        write_cnx_as_csv("cnx_pst.csv", pst_interconn)
        write_cnx_as_csv("cnx_pss.csv", pss_interconn)
        write_cnx_as_csv("cnx_sdp.csv", sdp_interconn)
        write_cnx_as_csv("cnx_alveo.csv", alveo_interconn)
        # write csv files for each switch's connections
        for sw in range(0, len(l1_switch_id)):
            sw_id = l1_switch_id[sw]
            write_cnx_as_csv_by_switch("{}.csv".format(sw_id), sw_id, all_cnx)

        for sw in range(0, len(l2_switch_id)):
            sw_id = l2_switch_id[sw]
            write_cnx_as_csv_by_switch("{}.csv".format(sw_id), sw_id, all_cnx)

    # Write summary of switch connections to stdout
    print("              **** No. SWITCH PORTS ****     Tot.")
    print(" P4 SWITCH    lfaa pst pss sdp 1-2 alveo     100G")
    print("                   25G 25G")
    # Only summarise layer 1 if there actually are layer 1 switches
    if not layer1_p4switches == 0:
        for sw in range(0, len(l1_switch_id)):
            lfaa_ports = sum(
                1 for x in lfaa_interconn if x[0] == l1_switch_id[sw]
            )
            pst_ports = sum(
                1 for x in pst_interconn if x[0] == l1_switch_id[sw]
            )
            pss_ports = sum(
                1 for x in pss_interconn if x[0] == l1_switch_id[sw]
            )
            sdp_ports = sum(
                1 for x in sdp_interconn if x[0] == l1_switch_id[sw]
            )
            l2_ports = sum(
                1 for x in switch_interconn if x[0] == l1_switch_id[sw]
            )
            alveo_ports = sum(
                1 for x in alveo_interconn if x[0] == l1_switch_id[sw]
            )
            all_ports = (
                lfaa_ports
                + (pst_ports + pst_per_100G - 1) // pst_per_100G
                + (pss_ports + pss_per_100G - 1) // pss_per_100G
                + sdp_ports
                + l2_ports
                + alveo_ports
            )
            print(
                f" {l2_switch_id[sw]}:    {lfaa_ports:2d}  {pst_ports:2d}  "
                f"{pss_ports:2d}  {sdp_ports:2d}   {l2_ports:2d}   "
                f"{alveo_ports:2d}      {all_ports:3d}"
            )

    for sw in range(0, len(l2_switch_id)):
        lfaa_ports = sum(1 for x in lfaa_interconn if x[0] == l2_switch_id[sw])
        pst_ports = sum(1 for x in pst_interconn if x[0] == l2_switch_id[sw])
        pss_ports = sum(1 for x in pss_interconn if x[0] == l2_switch_id[sw])
        sdp_ports = sum(1 for x in sdp_interconn if x[0] == l2_switch_id[sw])
        l1_ports = sum(1 for x in switch_interconn if x[3] == l2_switch_id[sw])
        alveo_ports = sum(
            1 for x in alveo_interconn if x[0] == l2_switch_id[sw]
        )
        all_ports = (
            lfaa_ports
            + (pst_ports + pst_per_100G - 1) // pst_per_100G
            + (pss_ports + pss_per_100G - 1) // pss_per_100G
            + sdp_ports
            + l1_ports
            + alveo_ports
        )
        print(
            f" {l2_switch_id[sw]}:    {lfaa_ports:2d}  {pst_ports:2d}  "
            f"{pss_ports:2d}  {sdp_ports:2d}   {l1_ports:2d}   "
            f"{alveo_ports:2d}      {all_ports:3d}"
        )


def read_params_from_file(filename: str, param_vals, param_names):
    with open(filename, "r") as f:
        lines = f.readlines()
    for line in lines:
        tok = line.split()
        if len(tok) >= 2:
            if tok[0].lower() in param_names:
                i = param_names.index(tok[0])
                param_vals[i] = int(tok[1])


if __name__ == "__main__":
    param_names = [
        "layer1_p4switches",
        "layer2_p4switches",
        "alveos",
        "alveos_per_switch",
        "stations",
        "stations_per_port",
        "pst_links",
        "pst_per_100G",
        "pss_links",
        "pss_per_100G",
        "sdp_links",
    ]
    param_vals = [9, 11, 420, 40, 512, 4, 32, 4, 167, 4, 72]
    create_csv_files = False

    for i in range(1, len(sys.argv)):
        arg = sys.argv[i].lower()
        if arg == "--itf":
            param_vals = [0, 1, 20, 40, 4, 4, 2, 4, 1, 4, 1]
            break
        elif arg == "--aa0.5":
            param_vals = [0, 1, 20, 40, 6, 4, 2, 4, 1, 4, 1]
            break
        elif arg == "--aa1":
            param_vals = [0, 1, 20, 40, 18, 4, 2, 4, 3, 4, 1]
            break
        elif arg == "--aa2":
            param_vals = [0, 1, 60, 40, 64, 4, 4, 4, 5, 4, 1]
            break
        elif arg == "--aa3":
            param_vals = [3, 5, 200, 40, 307, 4, 16, 4, 83, 4, 26]
            break
        elif arg == "--aa4":
            param_vals = [9, 11, 420, 40, 512, 4, 32, 4, 167, 4, 72]
            break
        elif arg == "--file":
            print("Reading parameters from '{}'".format(sys.argv[i + 1]))
            read_params_from_file(sys.argv[i + 1], param_vals, param_names)
            break
        elif arg == "--csv":
            create_csv_files = True
        else:
            pass

    # Summarise the parameters being used
    for idx in range(0, len(param_names)):
        print("{:>5}: {}".format(param_vals[idx], param_names[idx]))

    create_connections(create_csv_files, *param_vals)

    sys.exit(0)
