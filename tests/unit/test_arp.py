# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import json
import logging

import pytest

from ska_low_cbf.allocator.arp_replies import ArpReplies


@pytest.fixture()
def arp_db():
    """Return an arp database for test"""
    return ArpReplies()


@pytest.fixture()
def test_db(arp_db):
    """Check arp database responds correctly to adding/removing arp entries"""

    # some symbols to make tests simpler to understand
    host_1 = "192.168.1.1"
    host_2 = "192.168.1.2"
    no_reply = {"mac": "00:00:00:00:00:00", "port": 0}
    reply = {"mac": "02:03:04:05:06:07", "port": "22/0"}
    reply2 = {"mac": "03:04:05:06:07:08", "port": "3/0"}

    # empty database, invalid input
    arp_db.arp_update("p4_01", 123)
    hosts = arp_db.num_hosts()
    assert hosts == 0, f"expected 0 hosts, got {hosts}"

    # empty database, invalid input
    arp_db.arp_update("p4_01", """invalid""")
    hosts = arp_db.num_hosts()
    assert hosts == 0, f"expected 0 hosts, got {hosts}"

    # empty database, connector sending arp request but no arp reply
    arp_db.arp_update("p4_01", json.dumps({host_1: no_reply}))
    hosts = arp_db.num_hosts()
    assert hosts == 0, f"expected 0 hosts, got {hosts}"

    # first arp reply
    arp_db.arp_update("p4_01", json.dumps({host_1: reply}))
    hosts = arp_db.num_hosts()
    assert hosts == 1, f"expected 1 host, got {hosts}"

    # second arp reply
    arp_db.arp_update("p4_01", json.dumps({host_2: reply}))
    hosts = arp_db.num_hosts()
    assert hosts == 2, f"expected 2 hosts, got {hosts}"

    # first host no longer present
    arp_db.arp_update("p4_01", json.dumps({host_1: no_reply}))
    hosts = arp_db.num_hosts()
    assert hosts == 1, f"expected 1 host, got {hosts}"

    # neither host present
    arp_db.arp_update("p4_01", json.dumps({host_2: no_reply}))
    hosts = arp_db.num_hosts()
    assert hosts == 0, f"expected zero hosts, got {hosts}"

    # new arp reply
    arp_db.arp_update("p4_01", json.dumps({host_1: reply}))
    hosts = arp_db.num_hosts()
    assert hosts == 1, f"expected 1 host, got {hosts}"

    # arp reply with host moved
    arp_db.arp_update("p4_01", json.dumps({host_1: reply2}))
    hosts = arp_db.num_hosts()
    assert hosts == 1, f"expected 1 host, got {hosts}"

    # first host no longer present
    arp_db.arp_update("p4_01", json.dumps({host_1: no_reply}))
    hosts = arp_db.num_hosts()
    assert hosts == 0, f"expected no hosts, got {hosts}"

    # both hosts present in single update
    arp_db.arp_update("p4_01", json.dumps({host_1: reply, host_2: reply}))
    hosts = arp_db.num_hosts()
    assert hosts == 2, f"expected 2 host, got {hosts}"

    # both hosts gone in single update
    arp_db.arp_update(
        "p4_01", json.dumps({host_1: no_reply, host_2: no_reply})
    )
    hosts = arp_db.num_hosts()
    assert hosts == 0, f"expected no hosts, got {hosts}"
