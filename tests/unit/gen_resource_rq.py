#!/usr/bin/python3

from __future__ import annotations  # allow forward references in type hints

import json
import random
import sys


def write_subarray_rq_json(
    idx,
    first_stn,
    first_chn,
    n_stn,
    n_chn,
    n_beam_chans,
    pst_beam_idx,
    n_pst_beams,
):
    """
    Write a subarray request as JSON

    :param idx: the subarray number
    :param first_stn: the lowest station number
    :param first_chn: the lowest channel number
    :param n_stn: number of stations in subarray
    :param n_chn: number of channels in subarray
    :param n_beam_chan: list of number of chans in each beam
    :param pst_beam_idx: ID number for first PST beam in subarray
    """
    # print headings above first item
    if idx == 1:
        print("")
        print("    stations  channels    subarray size   stn_bm  chans/beam")
        print("    --------  --------   ---------------  ------  ----------")
    # show this subarray's range of stations and channels
    txt = "{:2d}:  {:3d}-{:3d}   {:3d}-{:3d}".format(
        idx, first_stn, first_stn + n_stn - 1, first_chn, first_chn + n_chn - 1
    )
    # show size of the subarray
    txt += "   {:3d}stn x {:3d}chn   {:3d}".format(
        n_stn, n_chn, len(n_beam_chans)
    )
    # show channels in each beam of the subbary
    txt += "    {}".format(n_beam_chans[0])
    for i in range(1, len(n_beam_chans)):
        txt += ", {}".format(n_beam_chans[i])
    print(txt)

    # create JSON subarray description
    # list of stations in the subarray
    stn_list = [
        {"station_id": x, "sub_station_id": 0}
        for x in range(first_stn, first_stn + n_stn)
    ]
    # list of station beams for the subarray
    sblist = []
    pst_beam_cnt = 0
    for i in range(0, len(n_beam_chans)):
        pst_svr_nic_a = "pst_{:02d}_a".format(i + pst_beam_idx)
        pst_svr_nic_b = "pst_{:02d}_b".format(i + pst_beam_idx)

        beam_def = {
            "station_beam_id": i + 1,
            "channels": [j + 1 for j in range(0, n_beam_chans[i])],
            "visibilities": {"enabled": True},
            "pss_beams": {"pss_beam_id": pst_beam_idx + i},  # FIXME
        }
        if pst_beam_cnt < n_pst_beams:
            beam_def["pst_beams"] = {
                "pst_beam_id": pst_beam_idx + i,
                "pst_server": [pst_svr_nic_a, pst_svr_nic_b],
            }
            pst_beam_cnt += 1
        sblist.append(beam_def)

    resources = {
        "subarray_id": idx,
        "stations": stn_list,
        "station_beams": sblist,
    }
    a = {"subarray": resources}
    with open("subarray_{:02d}.json".format(idx), "w") as f:
        json.dump(a, f, indent=2)


def split_subarray(
    chblk_1: int, stn_1, n_chblk: int, n_stn: int, levl: int, split_list: list
) -> None:
    """
    Split a subarray from the current subarray [stations x beams] and
    append to a running-total list of splits

    :param chblk1:  lowest numbered channel block (blk of 8 chans)
    :param stn1:   lowest numbered station
    :param n_chblk: number of channel blocks
    :param n_stn:  number of stations
    :param levl:   count of how many recursive splits left to be done
    :param split_list: running total splits done previously
    """

    # Only split if there are at least 2 beams and 4 stations
    min_chblks_to_split = 2
    min_stns_to_split = 4

    if (n_chblk < min_chblks_to_split) and (n_stn < min_stns_to_split):
        return

    # Already split as many times as desired
    # (ie as many subarrays as required)?
    if levl <= 1:
        split_list.append((chblk_1, stn_1, n_chblk, n_stn))
        return

    # randomly choose whether to split the channels or split the stations
    ok = False
    while not ok:
        do_split_beams = random.randint(0, 100) > 30
        if do_split_beams:
            size = n_chblk
            ok = n_chblk >= min_chblks_to_split
        else:
            size = n_stn
            ok = n_stn >= min_stns_to_split

    # find the point at which to split
    big = random.randint(1, size - 1)
    little = size - big
    if big < little:
        (big, little) = (little, big)

    # split the subarray rectangle
    if do_split_beams:
        split_list.append((chblk_1 + big, stn_1, little, n_stn))
        split_subarray(chblk_1, stn_1, big, n_stn, levl - 1, split_list)
    else:
        split_list.append((chblk_1, stn_1 + big, n_chblk, little))
        split_subarray(chblk_1, stn_1, n_chblk, big, levl - 1, split_list)


def gen_resource_rq(
    array_siz: dict, quanta: dict, n_rq: int, n_beams: int
) -> None:
    # max no. of beams summed across all subarrays
    n_total_beams = n_beams
    n_stn = array_siz["stns"] // quanta["stns"]
    n_beam = array_siz["chans"] // quanta["chans"]

    # a limited number of tries to split stations and beams into subarrays
    attempt = 0
    while attempt < 100:
        split_list = []
        split_subarray(0, 0, n_beam, n_stn, n_rq, split_list)
        if len(split_list) == n_rq:
            break
        attempt += 1

    # Was the subarray able to be split as requested
    if len(split_list) != n_rq:
        print(
            "Couldn't split {} stns x {} channels into {} subarrays!".format(
                n_stn * quanta["stns"], n_beam * quanta["chans"], n_rq
            )
        )
        sys.exit(-1)

    # always generate full possible no of beams up to 16
    max_subarray_beams = [itm[2] for itm in split_list]
    # initially assign one station beam for each subarray
    subarray_beams = [1 for i in range(0, len(split_list))]
    n_subarray_beams = len(split_list)

    # how many station beams can we have altogether
    n_stn_beams = sum(max_subarray_beams)
    n_stn_beams = min(n_stn_beams, n_total_beams)

    # go through all subarrays and add extra beams until we are full
    idx = 0
    while n_subarray_beams < n_stn_beams:
        if subarray_beams[idx] < max_subarray_beams[idx]:
            subarray_beams[idx] += 1
            n_subarray_beams += 1
        idx += 1
        if idx >= len(split_list):
            idx = 0
    print("Beams in each subarray: {}".format(subarray_beams))

    # write json out
    pst_beam_start_idx = 1
    cum_tot_pst_beams = 0
    for idx, itm in enumerate(split_list):
        first_stn = itm[1] * quanta["stns"] + 1
        first_chn = itm[0] * quanta["chans"] + 1
        n_stn = itm[3] * quanta["stns"]
        n_chn = itm[2] * quanta["chans"]

        # Compute blocks of channels in each beam by splitting the blocks for
        # the subarray evenly between each beam as far as poss (integer
        # division) then allocate remainder by incrementing blocks in the first
        # few beams
        beam_ch_blk = [
            itm[2] // subarray_beams[idx]
            for i in range(0, subarray_beams[idx])
        ]
        k = 0
        while sum(beam_ch_blk) < itm[2]:
            beam_ch_blk[k] = beam_ch_blk[k] + 1
            k += 1
            if k >= subarray_beams[idx]:
                k = 0
        beam_chans = [quanta["chans"] * blk for blk in beam_ch_blk]

        if cum_tot_pst_beams + subarray_beams[idx] <= n_total_beams:
            n_pst_beams = subarray_beams[idx]
        else:
            n_pst_beams = n_total_beams - cum_tot_pst_beams

        write_subarray_rq_json(
            idx + 1,
            first_stn,
            first_chn,
            n_stn,
            n_chn,
            beam_chans,
            pst_beam_start_idx,
            n_pst_beams,
        )
        pst_beam_start_idx += n_pst_beams
        cum_tot_pst_beams += n_pst_beams


def usage(progname: str) -> None:
    """Show usage help"""
    txt = "USAGE:\n{}".format(progname)
    txt += " n_stations n_chans, n_subarrays [n_pst_beams]"
    print(txt)


# This main used to generate subarray resource requests
if __name__ == "__main__":
    # Require three command line arguments: stations, channels,
    # number of subarrays
    if len(sys.argv) < 4:
        usage(sys.argv[0])
        sys.exit(-1)
    n_stn = int(sys.argv[1])
    n_chan = int(sys.argv[2])
    n_rq = int(sys.argv[3])
    n_beams = 16
    if len(sys.argv) >= 5:
        n_beams = int(sys.argv[4])

    if not ((n_chan % 8) == 0):
        print("Error channels must be a multiple of 8 for SKA")
        usage(sys.argv[0])
        sys.exit(-1)

    print(
        "\nGenerate {} subarrays from {} stations and {} channels".format(
            n_rq, n_stn, n_chan
        )
    )

    array_siz = {"stns": n_stn, "chans": n_chan}
    # channels allocated in groups of 8, but stations allocated individually
    quanta = {"stns": 1, "chans": 8}
    gen_resource_rq(array_siz, quanta, n_rq, n_beams)
