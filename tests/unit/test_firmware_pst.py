# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import pytest

from ska_low_cbf.allocator.firmware_pst import FirmwarePst

# Tests assume FPGA capabilities are not limited by FPGA implementation issues:
ALVEO_LIMITS_IDEAL = {
    "pst": {
        "vch": 1024,  # if beamformer clock frequency fixed
        "sps_ch": 1024,  # if packetiser first chans was 32k
    },
    "pss": {
        "vch": 1024,  # if FPGA failures near 1020 vch fixed
        "sps_ch": 1024,  # Max SPS chans per pipeline (VCT limit)
    },
    "vis": {
        "vch": 1024,  # Max Virtual Channels into FPGA
        "hbm": 606,  # Max VChans in one matrix correlator's (MxC) 2Bbyte HBM
        "bli": (512 * 513) // 2,  # Max baselines in one MxC
    },
}


NUM_PIPELINES = 3

MAX_STN_FREQ = 1024
MBPS_PER_COARSE_PST_BM = 68.68
MAX_MBPS = 100_000


@pytest.mark.parametrize(
    "num_subarrays, num_stn_beams, num_pst_bms, num_chans, num_stns",
    [
        (3, 1, 5, 2, 512),
        (3, 2, 1, 2, 512),
        (4, 1, 1, 1, 4),  # 4x absolutely minimal sized subarrays
        (1, 4, 1, 1, 4),  # 1 subarray 4stn_bms minimal stns, chans
        (4, 1, 5, 2, 512),  # 4x maximal subarrays that fit a pipeline
    ],
)
def test_pst_beamformer_max_3_sbarry_bm(
    num_subarrays, num_stn_beams, num_pst_bms, num_chans, num_stns
):
    """
    Check class representing PST beamformer handles up to 3
    pst-beams, but fails if more are added
    """

    fw_repr = FirmwarePst(ALVEO_LIMITS_IDEAL)
    stn_ids = [(i, 0) for i in range(0, num_stns)]
    tot_stn_bms = 0
    for suba in range(0, num_subarrays):
        sbarry_id = suba + 1
        for stbm in range(0, num_stn_beams):
            stn_bm_id = stbm + 1
            tot_stn_bms += 1
            pst_bm_ids = [_ + 1 for _ in range(0, num_pst_bms)]
            for freq_id in range(0, num_chans):
                rslt = fw_repr.pst_reserve(
                    stn_ids, sbarry_id, stn_bm_id, freq_id, pst_bm_ids
                )
                # Final subarray should always fail
                if tot_stn_bms > NUM_PIPELINES:
                    assert rslt is False
                else:
                    assert rslt is True


@pytest.mark.parametrize(
    "num_pst_bms, num_chans, num_stns",
    [
        # Test a bit beyond what would fit
        (1, 7, 512),  # 6chan x 512stn fits in 3 pipeline, but not 7 chan
        (1, 513, 4),  # 512ch * 4stn fits in 3 pipelines, not 513 ch
    ],
)
def test_pst_beamformer_max_chans(num_pst_bms, num_chans, num_stns):
    """
    Check class representing PST beamformer handles chans and stns up to
    stns * chans < 1024 for each of the 3 pipelines
    """
    max_frqs = (MAX_STN_FREQ // num_stns) * NUM_PIPELINES

    fw_repr = FirmwarePst(ALVEO_LIMITS_IDEAL)

    stn_ids = [(i, 0) for i in range(0, num_stns)]
    pst_bm_ids = [i for i in range(0, num_pst_bms)]

    tot_chans = 0
    for freq_id in range(0, num_chans):
        rslt = fw_repr.pst_reserve(stn_ids, 1, 1, freq_id, pst_bm_ids)
        tot_chans += 1
        # Final subarray should always fail
        if tot_chans <= max_frqs:
            assert rslt is True
        else:
            assert rslt is False


@pytest.mark.parametrize(
    "num_pst_bms, num_chans, num_stns",
    [
        # 16beams * 91ch would generate 100Mbps, test a bit beyond
        (16, 100, 4),
    ],
)
def test_pst_beamformer_max_bw(num_pst_bms, num_chans, num_stns):
    """
    Check class representing PST beamformer correctly passes/fails
    when generating many pst beam channels provided output bandwidth
    is within the 100Gbps of the network interface
    """

    fw_repr = FirmwarePst(ALVEO_LIMITS_IDEAL)

    stn_ids = [(i, i) for i in range(0, num_stns)]
    pst_bm_ids = list(range(0, num_pst_bms))

    tot_chans = 0
    for freq_id in range(0, num_chans):
        rslt = fw_repr.pst_reserve(stn_ids, 1, 1, freq_id, pst_bm_ids)
        tot_chans += 1
        bw_mbps = tot_chans * num_pst_bms * MBPS_PER_COARSE_PST_BM

        if bw_mbps <= MAX_MBPS:
            assert rslt is True
        else:
            assert rslt is False


ALVEO_LIMITS_LOW = {
    "pst": {
        "vch": 100,  # if beamformer clock frequency fixed
        "sps_ch": 20,  # if packetiser first chans was 32k
    },
}


@pytest.mark.parametrize(
    "num_chans, num_stns, expected_result",
    [
        # chans, stns, expected alloc result (3 pipelines in PST)
        (NUM_PIPELINES * 4, 26, False),  # 4x26=104vch > vch limit
        (NUM_PIPELINES * 4, 25, True),
        (NUM_PIPELINES * 21, 4, False),  # 21 chan > chan limit
        (NUM_PIPELINES * 20, 4, True),
    ],
)
def test_pst_lowered_limit(num_chans, num_stns, expected_result):
    """Test resource limiting mechanism"""
    fw_repr = FirmwarePst(ALVEO_LIMITS_LOW)
    stn_ids = [(i, i) for i in range(0, num_stns)]
    pst_bm_ids = list(range(0, 1))  # only one beam needed for limit test

    for freq_id in range(0, num_chans):
        rslt = fw_repr.pst_reserve(stn_ids, 1, 1, freq_id, pst_bm_ids)

    assert rslt == expected_result


@pytest.mark.parametrize(
    "sa_id, stn_bm_id, n_stns, n_pst",
    [
        # subarray_id, stn_beam_id, n_stations, n_pst_beam
        (1, 9, 8, 16),
        (2, 10, 2, 16),
        (3, 11, 256, 16),
        (4, 14, 512, 16),
        (5, 15, 600, 16),
    ],
)
def test_capacity_estimator(sa_id, stn_bm_id, n_stns, n_pst):
    """Test fpga channel capacity estimation matches fit"""
    fw_repr = FirmwarePst(ALVEO_LIMITS_IDEAL)

    stns = [(x + 1, x + 1) for x in range(0, n_stns)]
    pst_bm_ids = [x + 1 for x in range(0, n_pst)]
    frq_id = 100

    # add in some initial capacity usage to make it more difficult
    fw_repr.pst_reserve(stns, sa_id + 1, 3, frq_id, pst_bm_ids)
    fw_repr.accept_reservation()

    # How many channels should fit?
    n_chan_expected = fw_repr.chan_capacity(sa_id, stn_bm_id, n_stns, n_pst)

    # all the expected channels should fit
    chans = [64 + i for i in range(0, n_chan_expected)]
    for ch in chans:
        rslt = fw_repr.pst_reserve(stns, sa_id, stn_bm_id, ch, pst_bm_ids)
        assert rslt is True, f"chan {ch-64+1} of {n_chan_expected} didn't fit"

    # One more channel should fail to fit
    extra_ch = 64 + n_chan_expected
    rslt = fw_repr.pst_reserve(stns, sa_id, stn_bm_id, extra_ch, pst_bm_ids)
    assert rslt is False, "unexpected extra channel fitted!"
