# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""
test LowCbfSubarray using DeviceTestContext
"""
import time

# pylint: disable=no-self-use,redefined-outer-name
import pytest
import tango
from ska_tango_base.control_model import AdminMode, HealthState, ObsState
from tango.test_utils import DeviceTestContext

from ska_low_cbf.subarray.subarray_device import LowCbfSubarray


@pytest.fixture
def subarray(request):
    """Create DeviceTestContext or DeviceProxy for tests"""
    true_context = request.config.getoption("--true-context")
    if not true_context:
        with DeviceTestContext(LowCbfSubarray) as proxy:
            yield proxy
    else:
        database = tango.Database()
        instance_list = database.get_device_exported_for_class(
            "LowCbfSubarray"
        )
        for instance in instance_list.value_string:
            yield tango.DeviceProxy(instance)
            break


@pytest.mark.post_deployment  # subarray needs to talk to Allocator
@pytest.mark.usefixtures("subarray", "fsp_assign_json")
class TestSubarray:
    """Tests of the Subarray Device"""

    def test_init(self, subarray):
        """Init command"""
        subarray.Init()

    # def test_health(self, subarray):
    #     """Health State"""
    #     assert subarray.healthState == HealthState.OK

    def test_jones(self, subarray):
        """Jones source attribute"""
        fqdn = "a/b/c"
        subarray.jonesSource = fqdn
        assert subarray.jonesSource == fqdn

    def test_assign(self, subarray, fsp_assign_json):
        """Assign Resources command"""

        # Wait until the subarray device completes initialisation before using it
        tries = 0
        while tries < 30:
            tries += 1
            try:
                dev_initialised = subarray.State() != tango.DevState.INIT
            except tango.DevFailed:
                dev_initialised = False
            if dev_initialised:
                break
            print(f"wait for subarray init to complete [{tries}]")
            time.sleep(1)

        # bring subarray online
        subarray.adminMode = AdminMode.ONLINE
        subarray.AssignResources(fsp_assign_json)

        # delaying is a dumb way of waiting for async commands to complete
        time.sleep(1)
        assert subarray.adminMode == AdminMode.ONLINE
        assert subarray.obsState == ObsState.IDLE
