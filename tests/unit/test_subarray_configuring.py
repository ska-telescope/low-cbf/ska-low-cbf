# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import logging

import pytest
from sw_cnx import aa3, create_cnx_list, marsfield_psi

from ska_low_cbf.allocator.arp_replies import ArpReplies
from ska_low_cbf.allocator.resources import Resources

# Array config for these tests
array_config_small = {
    "stations": 8,
    "channels": 382,
    "subarrays": 16,
    "alveos_per_fsp": 1,
    "pss_beams": 22,
    "pst_beams": 4,
}


def make_stn_beam_cfg(beam_chans):
    beams = []
    for bm_id, n_chan in beam_chans.items():
        bm_spec = {
            "beam_id": bm_id,
            "freq_ids": [x + 64 for x in range(0, n_chan)],
            "delay_poly": "n/a",
        }
        beams.append(bm_spec)
    return beams


def make_vis_stn_beams_cfg(beam_chans):
    beams = []
    for bm_id, n_chan in beam_chans.items():
        bm_spec = {
            "stn_beam_id": bm_id,
            "host": [[0, f"vis_{bm_id}.x.x.x"]],
            "port": [0, 0, 0],
            "integration_ms": 850,
        }
        beams.append(bm_spec)
    return beams


def make_pst_bms_cfg(pst_bms):
    beams = []
    for stn_bm_id, pst_bm_list in pst_bms.items():
        for pst_bm_id in pst_bm_list:
            cfg = {
                "pst_beam_id": pst_bm_id,
                "stn_beam_id": stn_bm_id,
                "delay_poly": "pst_poly_sub n/a",
                "jones": "jones_sub n/a",
                "destinations": [
                    {
                        "data_host": f"pst_{pst_bm_id}.x.x.x",
                        "data_port": 10000,
                        "start_channel": 0,
                        "num_channels": 216,
                    }
                ],
                "stn_wts": [1.0],
            }
            beams.append(cfg)
    return beams


def make_pss_bms_cfg(pss_bms):
    beams = []
    for stn_bm_id, pss_bm_list in pss_bms.items():
        for pss_bm_id in pss_bm_list:
            cfg = {
                "pss_beam_id": pss_bm_id,
                "stn_beam_id": stn_bm_id,
                "delay_poly": "pst_poly_sub n/a",
                "jones": "jones_sub n/a",
                "destinations": [
                    {
                        "data_host": f"pss_{pss_bm_id}.x.x.x",
                        "data_port": 10000,
                        "start_channel": 0,
                        "num_channels": 54,
                    }
                ],
                "stn_wts": [1.0],
            }
            beams.append(cfg)
    return beams


def make_subarray_cfg(sub_id, n_stn, stn_bm_chans, pst_bms, pss_bms):
    stns = [[x + 1, 1] for x in range(0, n_stn)]
    subarray_cfg = {
        # Input to subarray from LFAA: stations, station_beams, frequencies
        "subarray_id": sub_id,
        "stations": {
            "stns": stns,
            "stn_beams": make_stn_beam_cfg(stn_bm_chans),
        },
        # Output to be calculated: visibilities
        "vis": {
            "firmware": "vis:x.y.z",
            "stn_beams": make_vis_stn_beams_cfg(stn_bm_chans),
        },  # standard Correlation parameter list TBD
        "status": 1,
    }
    if pst_bms is not None:
        subarray_cfg["timing_beams"] = {
            "firmware": "pst:x.y.z",
            "beams": make_pst_bms_cfg(pst_bms),
        }
    if pss_bms is not None:
        subarray_cfg["search_beams"] = {
            "firmware": "pss:x.y.z",
            "beams": make_pss_bms_cfg(pss_bms),
        }
    return subarray_cfg


def test_configure_many_times():
    """
    Test that a subarray can be configured/deconfigured multiple times
    """
    # create sample connections list
    cnx, _ = create_cnx_list(marsfield_psi)  # don't need summary _
    n_alveo = 8
    # Use list to register alveos with allocator
    res = Resources(array_config_small, cnx)
    # Register the available alveos similar to FSP registration
    for i in range(0, n_alveo):
        # serial#_N format used in create_cnx_list for fake serial numbers
        success, msg = res.alveo_registration(
            {"serial": f"serial#_{i+1}", "hw": "u55c", "status": 1}
        )
        assert success, msg

    # Create a subarray configuration string suitable for resourcing test
    s_id = 1
    n_stn = 4
    stn_bm_chans = {1: 4, 2: 5}  # stn_bm_id: no_of_chans
    pst_bm_ids = {1: [1, 2, 3], 2: [4, 5]}  # stn_bm_id: [pst_bm_id, ..]
    pss_bm_ids = {
        1: [100, 200, 300],
        2: [400, 500],
    }  # stn_bm_id: [pss_bm_id, ..]
    subarray_cfg = make_subarray_cfg(
        s_id, n_stn, stn_bm_chans, pst_bm_ids, pss_bm_ids
    )

    # Can configure and deconfigure many times in succession
    for _ in range(0, 100):
        rslt, msg = res.cfg_scan2(subarray_cfg)
        assert rslt is True, "could not configure subarray"
        res.cfg_end2(s_id)


def test_configure_two_subarray():
    """
    Test two different subarrays can be configured
    """
    # create sample connections list
    cnx, _ = create_cnx_list(marsfield_psi)  # don't need summary _
    n_alveo = 8
    # Use list to register alveos with allocator
    res = Resources(array_config_small, cnx)
    # Register the available alveos similar to FSP registration
    for i in range(0, n_alveo):
        # serial#_N format used in create_cnx_list for fake serial numbers
        success, msg = res.alveo_registration(
            {"serial": f"serial#_{i+1}", "hw": "u55c", "status": 1}
        )
        assert success, msg

    # Create a subarray configuration string suitable for resourcing test
    s_id = 1
    n_stn = 4
    stn_bm_chans = {1: 4, 2: 5}  # stn_bm_id: no_of_chans
    pst_bm_ids = {1: [1, 2, 3], 2: [4, 5]}  # stn_bm_id: [pst_bm_id, ..]
    pss_bm_ids = {
        1: [100, 200, 300],
        2: [400, 500],
    }  # stn_bm_id: [pss_bm_id, ..]
    subarray_cfg = make_subarray_cfg(
        s_id, n_stn, stn_bm_chans, pst_bm_ids, pss_bm_ids
    )

    rslt, msg = res.cfg_scan2(subarray_cfg)
    assert rslt is True, "could not configure subarray1"

    # Create a subarray configuration string suitable for resourcing test
    s_id2 = 2
    n_stn2 = 4
    stn_bm_chans2 = {1: 6, 2: 7}  # stn_bm_id: no_of_chans
    pst_bm_ids2 = {1: [6, 7], 2: [8, 9, 10]}  # stn_bm_id: [pst_bm_id, ..]
    pss_bm_ids2 = {
        1: [60, 70],
        2: [80, 90, 100],
    }  # stn_bm_id: [pss_bm_id, ..]
    subarray_cfg2 = make_subarray_cfg(
        s_id2, n_stn2, stn_bm_chans2, pst_bm_ids2, pss_bm_ids2
    )

    rslt, msg = res.cfg_scan2(subarray_cfg2)
    assert rslt is True, "could not configure subarray2"

    print(f"alveo_list= {res.get_alveo_list()}")
    res.cfg_end2(s_id)
    print(f"alveo_list= {res.get_alveo_list()}")
    res.cfg_end2(s_id2)
    # assert False, "Forced fail"


def test_config_access():
    """
    Test methods used to access config don't crash
    """

    # create sample connections list
    cnx, _ = create_cnx_list(marsfield_psi)  # don't need summary _
    n_alveo = 8
    # Use list to register alveos with allocator
    res = Resources(array_config_small, cnx)
    # Register the available alveos similar to FSP registration
    for i in range(0, n_alveo):
        # serial#_N format used in create_cnx_list for fake serial numbers
        success, msg = res.alveo_registration(
            {"serial": f"serial#_{i+1}", "hw": "u55c", "status": 1}
        )
        assert success, msg

    # Check methods work without crash with no configuration
    assert res.get_internal_repr() is not None
    assert res.get_subarray_repr() is not None
    assert res.get_stn_routes() is not None
    assert res.get_subarray_alveos(1) is not None
    assert res.get_pst_repr() is not None
    assert res.get_alveo_list() is not None

    # Create a subarray configuration string suitable for resourcing test
    s_id = 1
    n_stn = 4
    stn_bm_chans = {1: 4, 2: 5}  # stn_bm_id: no_of_chans
    pst_bm_ids = {1: [1, 2, 3], 2: [4, 5]}  # stn_bm_id: [pst_bm_id, ..]
    pss_bm_ids = {
        1: [100, 200, 300],
        2: [400, 500],
    }  # stn_bm_id: [pss_bm_id, ..]
    subarray_cfg = make_subarray_cfg(
        s_id, n_stn, stn_bm_chans, pst_bm_ids, pss_bm_ids
    )

    rslt, msg = res.cfg_scan2(subarray_cfg)
    assert rslt is True, "could not configure subarray1"

    # Check methods work without crash when configured
    assert res.get_internal_repr() is not None
    assert res.get_subarray_repr() is not None
    assert res.get_stn_routes() is not None
    assert res.get_subarray_alveos(s_id) is not None
    assert res.get_pst_repr() is not None
    assert res.get_alveo_list() is not None

    # Check methods work without crash after deconfigure
    res.cfg_end2(s_id)
    assert res.get_internal_repr() is not None
    assert res.get_subarray_repr() is not None
    assert res.get_stn_routes() is not None
    assert res.get_subarray_alveos(s_id) is not None
    assert res.get_pst_repr() is not None
    assert res.get_alveo_list() is not None
