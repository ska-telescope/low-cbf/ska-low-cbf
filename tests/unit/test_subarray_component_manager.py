# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2022 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""
This module contains tests of :py:mod:`ska_low_cbf.subarray.component_manager`.
"""
import json
import logging

import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import CommunicationStatus, PowerState
from ska_tango_base.executor import TaskStatus
from ska_tango_testing.mock import MockCallableGroup

from ska_low_cbf.subarray.component_manager import (
    LowCbfSubarrayComponentManager,
)


@pytest.fixture()
def logger() -> logging.Logger:
    """
    Return a logger for use in testing.

    :return: a logger
    """
    return logging.getLogger()


@pytest.fixture()
def callbacks() -> MockCallableGroup:
    """
    Return a callback group.

    :return: a callback group.
    """
    return MockCallableGroup(
        "communication_state",
        "component_state",
        "task",
    )


@pytest.fixture()
def allocator():
    """Return a dummy allocator for use in testing."""

    class DummyAllocator:
        """Minimal Dummy Allocator device mock"""

        # pylint: disable=invalid-name
        def RequestAllocation(self, request):
            """Minimal Request Allocation implementation"""
            request = json.loads(request)  # ignored, just return one allocated
            resource_table = {
                "fsp_01": {
                    "shared": True,
                    "subarray_access": [True, False, False, False],
                    "fw_image": "image",
                    "fw_mode": "mode",
                },
                "p4_01": {
                    "shared": True,
                    "subarray_access": [False, False, False, False],
                },
            }
            return json.dumps(resource_table)

        # pylint: disable=invalid-name
        def RequestDeallocation(self, request):
            """Minimal Deallocation implementation"""
            request = json.loads(request)  # ignored, just return deallocated
            dealloc_resource_table = {
                "fsp_01": {
                    "shared": True,
                    "subarray_access": [False, False, False, False],
                    "fw_image": "image",
                    "fw_mode": "mode",
                },
                "p4_01": {
                    "shared": True,
                    "subarray_access": [False, False, False, False],
                },
            }
            return json.dumps(dealloc_resource_table)

    return DummyAllocator()


def online_state_cbk():
    """dummy callback returning value indicating adminstate==online"""
    return 0


@pytest.fixture()
def component_manager(allocator, callbacks, logger):
    """Return the component manager under test."""
    subarray_id = 1
    return LowCbfSubarrayComponentManager(
        logger=logger,
        subarray_id=subarray_id,
        communication_state_callback=callbacks["communication_state"],
        component_state_callback=callbacks["component_state"],
        allocator=allocator,
        admin_mode_cbk=online_state_cbk,
        connection_builder=lambda fqdns: {fqdn: None for fqdn in fqdns},
    )


def test_start_and_stop_communicating(
    component_manager,
    callbacks,
) -> None:
    """Test start and stop communicating."""
    assert (
        component_manager.communication_state == CommunicationStatus.DISABLED
    )
    assert component_manager.component_state["power"] == PowerState.UNKNOWN
    assert component_manager.component_state["fault"] is None

    component_manager.start_communicating()

    callbacks.assert_call(
        "communication_state", CommunicationStatus.ESTABLISHED
    )

    callbacks.assert_call(
        "component_state",
        power=PowerState.ON,
        fault=False,
    )

    callbacks.assert_not_called()

    component_manager.stop_communicating()

    callbacks.assert_call(
        "component_state",
        power=PowerState.OFF,
        # TODO: the fact there is no 'fault=FALSE' here is due to base classes??
    )

    callbacks.assert_call("communication_state", CommunicationStatus.DISABLED)

    callbacks.assert_not_called()
