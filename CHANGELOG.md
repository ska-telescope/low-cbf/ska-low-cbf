# Version History
This list is in reverse-chronological order (the latest change is at the top).
Different heading levels are used for Major, Minor, and Patch releases - in Markdown
syntax, that's `#` for Major, `##` for Minor, and `###` for Patch.

## 0.13.0
* `LowCbfController._unavaliable_alveos` didn't corectly update once Alveos were made available; condition variable lock was held too long which lead to deadlock when multiple Alveos got registered with Allocator in a quick succession.
* Suppress "missing subarray id" log messages when processing `internal_alveo` updates
  that contain empty register dictionaries (e.g. PST when only one pipeline configured)
* Prevent unnecessary Allocator & Connector attribute subscription if subscribed already
* `healthState` updated only when `adminMode` is **ONLINE**  or **ENGINEERING** (see also `ENGINEERING_MODE_IGNORE_HEALTH`)
* add `ENGINEERING_MODE_IGNORE_HEALTH` environment variable; when set to 'True' the health state won't propagate to Low CBF Controller
* Add `assigned_processors` Tango attribute to `LowCbfSubarray` device

## 0.12.0
* Controller device exposes two new Tango attributes: `all_alveos` and `available_alveos`; updated Taranta dashboard `Low CBF Controller - Status.wj`
* Remove FSPs from allocator. Deprecate FSPs in subarray configure command
* Update Taranta dashboards that allow setting subarray and processor `adminMode` to ENGINEERING.
* Add tool to calculate Alveo usage by any proposed subarray configuration (see src/allocator/extimation_tool.py)

## 0.11.1
* Add `cbf.telmodel_source_uri` Helm variable, mapped to new `DelayDevice` Tango property
`TelmodelSourceURI`
* Delay-poly simulator changed to publish station-beam delays last. This means PSS/PST test output will begin with both validity flags "valid" for large numbers of stations.
Note: For large numbers of stations, katpoint takes significant time to calculate delays. Expect delay-poly attributes to update slowly.
* ObsState behaviour no longer rejects subarray.Configure() commands when a subarray is already configured (in ObsState.READY)
* Apply known FPGA allocation limits by default.
* Add new FPGA limit for PST number of channels (to work around PST FPGA 2k entry first-frequency table with 16 entries used per channel 2k/16 = 128 SPS chans max)

## 0.11.0
* Avoid Allocator crash when using no Processors, by using a dummy tango device URI when auto-registering processors
* Make Allocator per-personality limits adjustable via tango attribute
* DelayDevice: Add `ypol_offset_ns` attribute
* Update to pytango 9.5.0
* DelayDevice: Polynomial updates enabled/disabled by 'enable_poly_updates' attribute
* DelayDevice: Polynomial validity interval configurable by 'validity_seconds' attribute
* Increase delay emulator CPU & memory limits

## 0.10.0
* add support for PSS personality in the Allocator and Subarray devices
* fix SKB-316: Make subarray configuration wait for processor firmware download
* fix SKB-442: Sort stations in subarray configuration by station/substation ID as per CBF/SDP ICD

## 0.9.0
* new subarray NO_HEALTH_ROLLUP environment variable stops subarray from including external Tango devices in its health roll-up (switches and processors)
* new allocator ALLOW_AUTO_REGISTER_PROCESSORS environment variable - bugfix for undesired auto-registration with large depolyments
* delay-polynomial simulator changed to produce PST beam polynomials that are differences from the polynomials for the corresponding station beam
* delay-polynomial simulator changed to have initial empty scan_id string (matching CspLeafNode change), allowing processor to decide when to discard
* properties related to subscriptions to the connector are configurable via helm chart
* fix PERENTIE-2422 allocator-resubscribe-to-connector-ARP bug that occurs if connector is deleted/respawned
* update delay polynomial validity Grafana panel

### 0.8.1
* fix SKB-317 subarray healthState race condition

## 0.8.0
* Three new Delay polynomial generator commands for PST delay generation: PstOffsetRaDec, PstOffsetAzEl, PstOffsetDelay
* add `delaysValid` attribute to indicate/summarise whether all delay polynomials in this subarray are valid; used by a Grafana dashboard
* Breaking Change: Delay polynomial generator updated to produce delay polynomials using the new Telmodel CSP_LOW_DELAYMODEL_1_0 format (https://gitlab.com/ska-telescope/ska-telmodel/-/blob/master/src/ska_telmodel/csp/examples.py?ref_type=heads#L1878-1914)

## 0.7.0
* **Separate Low CBF Helm charts** - downstream users will need to add
`ska-low-cbf-proc` and `ska-low-cbf-conn` sub-charts as required.
* Added P4 switch interaction to allow for ARP of SDP hosts
* Added calculation of P4 switch routes needed to get visibilities to SDP hosts
* Internal interface to Correlator/Processor changed (not backward compatible)
* Correlator allocation algorithm reworked to use both available Matrix Correlators in Correlator FPGA
* New and updated tests to verify Correlator allocation with 2 Matrix Correlators
* Propagate healthState attribute change: processor -> subarray -> controller
* Fix allocator confusion of station beams when multiple station beams present
* Delay polynomial epoch changed from 1970 to SKA-epoch (midnight, 1 January 2000 TAI)
* Fix LowCbfController where opMode was DISABLED regardless of ONLINE/OFFLINE adminMode ([Jira PERENTIE-2192](https://jira.skatelescope.org/browse/PERENTIE-2192))

### 0.6.1
* bugfixes
  * fixes allocator-proxy-failure in subarray device (now uses mccsdeviceproxy)
  * fixes allocator-crash-when-no-fsps-available (now logs and returns error message)
* updated ska-tango-base to v0.18.1
* updated ska-* charts to latest versions for which events work (at 2023-06-09)
* Added skeleton delay-polynomial simulator device (to enable processor development that uses delay-polynomials)

## 0.6.0
* Added processor registration command and "fsps" attribute listing alveos of registered processors
* Updated subarray AssignResources command:
  * FSP, P4, sharing-mode arguments removed
  * Now takes an empty JSON string argument
* Updated subarray ConfigureScan command:
  *  Now includes FSP selection and FSP firmware selection in ConfigureScan arguments
  *  SDP destination address format updated to match Telmodel
  *  PST destination address format updated to match PST "Channel Blocks" given to LMC
* Fix for Abort-from-scanning-obsstate bug where it was impossible to scan again afterwards
* Add psi-low-test CI job, which uses real harwdare in the Low PSI (currently
only a CNIC-CNIC via P4/Connector test is performed)
* Amend `psi-low-test` CI job with a call to switch's `LoadPorts()` method which reportedly enables associated Alveo card
* Switch to Xilinx platform/shell version 3 (XDMA 3) - `helm` charts and dependant packages (`processor`)
* Use the latest [Taranta versions](https://developer.skao.int/projects/ska-tango-taranta-suite/en/latest/) 1.3.8/1.3.6 (Taranta and TangoGQL respectively).
* Pin the [CI runner version to 9.3.32](https://skao.slack.com/archives/CEMF9HXUZ/p1675900607781289?thread_ts=1675869473.554859&cid=CEMF9HXUZ) until PyTango upgrade to 9.3.6

### 0.5.7
* Use `pyproject.toml` as source of software version number
* Include `ska-low-cbf-conn` and `ska-low-cbf-fpga` sub-charts in our Helm chart
(currently disabled by our `test-parent` chart for running local/CI tests)
* Add `charts/psi-low.values.yaml` for enabling hardware in Low PSI
* Add `VALUES_FILE` variable to Makefile
### 0.5.6
* Allow Subarray On, Off, Standby commands to complete (response is "rejected",
except On which is handled by base classes)
* Background work towards calculating Connector routing table entries &
Processor virtual channel tables as Subarrays are configured
### 0.5.5
* Implement AssignResources command to work with FSP & P4 resources in Low.CBF
* Stubbed implementation of ConfigureScan command
### 0.5.4
* Restructure src directory
* Convert from setup.py to pyproject.toml
* Replace allocation algorithm with allocator/resources.py ("resource tables")
* Update to ska-tango-base v0.13.2
* Various edits to improve lint score
### 0.5.3
* Subarray: Update attribute names used on Processor device
* Allocation Algorithm
  * calculate routes from LFAA to FPGAs and from FPGAs to PST
  * calculate virtual channel tables
### 0.5.2
* Allocation Algorithm can report allocated FPGAs for a subarray
* Allocator device RequestAllocation command return value is derived from
allocation (Processor devices only)
  * A very rough implementation, needs work on mapping ID to FQDN
### 0.5.1
* Add tests for Allocator device
* test-parent chart: add ska-low-cbf-proc chart, for integration tests
## 0.5.0
* Allocator TANGO device created
  * `RequestAllocation` return value is hard-coded
* Subarray passes on information from resource assignment requests to Allocator,
expects to receive details of assigned Processor devices in response
## 0.4.0
* Subarray can assign (hard-coded) Processor resource for 1 PST beam and turn on
PSR test packet generation
### 0.3.4
* Move FPGA-related code to [ska-low-cbf-proc](https://gitlab.com/ska-telescope/ska-low-cbf-proc)
